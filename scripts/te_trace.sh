#!/bin/sh
te_version="2.115.13"

if [ $# -lt 1 ]; then
	echo " "
else
	echo "direct operations not permitted"
fi

export __tango_pid=999999

if [ -n "$TE_DEBUG_DISABLE_COLORS" ]; then
	TE_DEBUG_DISABLE_COLORS=0
else
	TE_DEBUG_DISABLE_COLORS=1
fi

if [ $TE_DEBUG_DISABLE_COLORS -gt 0 ]; then
	RED=''
	NC=''
	GREEN=''
	BLUE=''
	YELLOW=''
else

	RED='\e[0;31m'
	NC='\e[0m'
	GREEN='\e[0;32m'
	BLUE='\e[0;34m'
	YELLOW='\e[1;33m'
fi

__te_api_trace()
{
	__te_level=$1
	if [ $__te_level -gt 0 ]; then
		echo "Enable TE API traces"
		echo 'file stm_te.c +p' > /sys/kernel/debug/dynamic_debug/control
	else
		echo "Disable TE API traces"
		echo 'file stm_te.c -p' > /sys/kernel/debug/dynamic_debug/control
	fi

}

__te_sw_inj_trace()
{
	__te_level=$1
	if [ $__te_level -gt 0 ]; then
		echo "Enable TE SW Injection traces"
		echo 'file te_demux.c +p' > /sys/kernel/debug/dynamic_debug/control
	else
		echo "Disable TE SW Injection traces"
		echo 'file te_demux.c -p' > /sys/kernel/debug/dynamic_debug/control
	fi
}

__te_print_hal_log()
{
	hal_entry=$1; hal_obj=$2; count=$3;
	te_hal_path="/root/te_hal_logs"

	echo -en '\n' >> $te_hal_path/$hal_obj;
	echo ">>>>>>> Count:"$count" >>>>>>> "$hal_entry" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" >> $te_hal_path/$hal_obj;
	echo -en '\n' >> $te_hal_path/$hal_obj;

	cat $hal_entry >> $te_hal_path/$hal_obj;

	echo -en '\n' >> $te_hal_path/$hal_obj;
	echo "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" >> $te_hal_path/$hal_obj;
	echo -en '\n' >> $te_hal_path/$hal_obj;
}

__te_is_tp_dump_active()
{
	if ps -p $__tango_pid > /dev/null
	then
		echo -e "Closing last TANGO dumping request"
		kill $__tango_pid
		echo -e "move tango logs to /root/te_hal_logs_"${__tango_pid}
		mv /root/te_hal_logs /root/te_hal_logs_${__tango_pid}
	fi
}

__te_tango_logs()
{
	echo -e "Dumping TP logs"

	__te_level=$1
	iter=`expr $__te_level \* 5`
	count=0
	rm -rf /root/te_hal_logs
	mkdir -p /root/te_hal_logs

	te_hal_path="/root/te_hal_logs"

	while [ $count -lt $iter ]
	do
		for each in /sys/kernel/debug/stm_te_hal/pDevice.0/*;
		do
			hal_obj=$(echo $each | sed 's/\/sys.*\.0\///')"_"$count;
			hal_obj_1=$(echo $each | sed 's/\/sys.*\.0\///');
			temp=$(echo $each | grep -v 'pDeviceSharedMemory\|CAMData');
			if [ $? -eq 0 ]
			then
				__te_print_hal_log $temp $hal_obj_1 $count
			fi
		done
		for each in /sys/kernel/debug/stm_te_hal/TSInput/*;
		do
			hal_obj=$(echo $each | sed 's/\/sys.*Input\///')"_"$count;
			hal_obj_1=$(echo $each | sed 's/\/sys.*Input\///');
			cat $each | grep ' Uninitialised' > /dev/null;
			if [ $? -eq 1 ]
			then
				__te_print_hal_log $temp $hal_obj_1 $count
			fi
		done
		sleep 1
		count=`expr $count + 1`
	done
}

__te_section_trace()
{
	__te_level=$1
	#echo "[DEBUG] section trace level: "$__te_level
	if [ $__te_level -gt 0 ]; then
		echo "Enable Section traces"
		echo 'file te_section_filter.c +p' > /sys/kernel/debug/dynamic_debug/control
	else
		echo "Disable Section traces"
		echo 'file te_section_filter.c -p' > /sys/kernel/debug/dynamic_debug/control
	fi
}


__te_record_trace()
{
	__te_level=$1
	if [ $__te_level -gt 0 ]; then
		echo "Enable Record traces"
		echo 'file te_output_filter.c +p' > /sys/kernel/debug/dynamic_debug/control
		echo 'file te_ts_filter.c +p' > /sys/kernel/debug/dynamic_debug/control
	else
		echo "Disable Record traces"
		echo 'file te_output_filter.c -p' > /sys/kernel/debug/dynamic_debug/control
		echo 'file te_ts_filter.c -p' > /sys/kernel/debug/dynamic_debug/control
	fi
}

__te_index_trace()
{
	__te_level=$1
	if [ $__te_level -gt 0 ]; then
		echo "Enable TS Index traces"
		echo 'file te_ts_index_filter.c +p' > /sys/kernel/debug/dynamic_debug/control
	else
		echo "Disable TS Index traces"
		echo 'file te_ts_index_filter.c -p' > /sys/kernel/debug/dynamic_debug/control
	fi
}

__te_buffer_overflow_trace()
{
	__te_level=$1
	if [ $__te_level -gt 0 ]; then
		echo "Enable Overflow traces"
		echo 'file te_output_filter.c +p' > /sys/kernel/debug/dynamic_debug/control
	else
		echo "Disable Overflow traces"
		echo 'file te_output_filter.c -p' > /sys/kernel/debug/dynamic_debug/control
	fi
}

#transport engine
process_te_trace()
{
	te_group=$1

	arg=$2;
	#echo -e "[DEBUG] User Level: "$1
	arg=$(echo $arg | tr 'a-z' 'A-Z')
	#echo -e "[DEBUG] Internal Level: "$arg

	case $arg in
		DISABLE) level=0; break ;;
		D) level=0; break ;;
		AUTO) level=2; break ;;
		A) level=2; break ;;
		LIGHT) level=1; break ;;
		L) level=1; break ;;
		VERBOSE) level=3; break ;;
		V) level=3; break ;;
		EXTENSIVE) level=4; break ;;
		E) level=4; break ;;
		*)
			echo -e "${RED}Invalid Level: "$arg"$NC"
			level=-1
			break ;;
	esac

	if [ $level -lt 0 ]; then
		return -1
	fi

	te_level=$level
	#echo -e "[DEBUG] group: "$te_group " level:"$te_level
	if [ $te_group == "api" ]; then
		__te_api_trace $te_level
	elif [ $te_group == "sw-injection" ]; then
		__te_sw_inj_trace $te_level
	elif [ $te_group == "tango" ]; then
		__te_is_tp_dump_active
		 __te_tango_logs ${te_level} &
		 __tango_pid=$!
	elif [ $te_group == "section" ]; then
		__te_section_trace $te_level
	elif [ $te_group == "record" ]; then
		__te_record_trace $te_level
	elif [ $te_group == "index" ]; then
		__te_index_trace $te_level
	elif [ $te_group == "buffer-overflow" ]; then
		__te_buffer_overflow_trace $te_level
	elif [ $te_group == "macro-block" ]; then
		__te_is_tp_dump_active
		__te_tango_logs ${te_level} &
		 __tango_pid=$!
		__te_sw_inj_trace $te_level
		__te_buffer_overflow_trace $te_level
	else
		echo -e "${RED}Invalid TE group: "$te_group"$NC"
	fi
}

process_te_help()
{
	echo -e " Transport-Engine Debug Script $te_version "
	echo -e "\n$BLUE Usage: sdk2-debug --module=te --type=<val> --level=<level_val> $NC \n"
	echo -e " Set Transport Engine Trace Groups (specified with --type) with Level (specified with --level) for Debugging"
	echo -e "\n$BLUE   --type=<val>: $NC"
	echo -e "    Specify the type value to be used. **Mandatory**"
	echo -e "    Possible <val>:"
	echo -e "      api : All API related traces"
	echo -e "      sw-injection : Traces related to software injection"
	echo -e "      tango : Dump a snapshot of TP statistics into /root/te_hal_logs folder on target"
	echo -e "      section : Traces related to section filtering"
	echo -e "      record : Traces related to TS grab"
	echo -e "      index : Traces related to TS indexes"
	echo -e "      buffer-overflow : Traces related to buffer overflow at TE output"
	echo -e "      macro-block : Traces related to macro block in a/v playback"

	echo -e "\n$BLUE  --level=<level_val>: $NC "
	echo -e "    Sets the trace level of the printing information for the specified <type>"
	echo -e "     (d)isable: Disable all traces related to a particular <type>"
	echo -e "     (l)ight: Limited debug traces for the specified <type>"
	echo -e "     (a)uto: TE recommended trace level for a specfied <type>"
	echo -e "     (v)erbose: Detailed traces"
	echo -e "     (e)xtensive: All possible traces for a specified type. Can cause performance issues"

	echo -e "\n$BLUE  --help/-h/-? $NC"
	echo -e "    Prints this information"
	echo -e "    To disable colors from script, set env flag TE_DEBUG_DISABLE_COLORS to 1"

}
