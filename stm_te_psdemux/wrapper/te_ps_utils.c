/******************************************************************************
 * Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.
 *
 * This file is part of the Transport Engine Library.
 *
 * Transport Engine is free software; you can redistribute it and/or
 * modify it
 * under the terms of the GNU General Public License version 2 as
 * published by
 * the Free Software Foundation.
 *
 * Transport Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The Transport Engine Library may alternatively be licensed under a
 * proprietary license from ST.
 *
 * Source file name : te_ps_utils.c
 *
 * Defines PES filter specific operations
* *****************************************************************************/
#include "te_ps_utils.h"
#include "te_internal_cfg.h"

#define PS_OS_INFINITE			((int)-1)
#define PS_OS_IMMEDIATE			((int) 0)

/* The Semaphore functions */
int ps_os_sema_initialize(ps_os_semaphore_t *sema,
			uint32_t	initial_count)
{
	*sema = (ps_os_semaphore_t)kzalloc(
			sizeof(struct semaphore),
			GFP_KERNEL);

	if (*sema != NULL) {
		sema_init(*sema, initial_count);
		return 0;
	} else
		return -ENOMEM;
}

int ps_os_sema_terminate(ps_os_semaphore_t sema)
{
	if (sema)
		kfree((void *)sema);

	return 0;
}

int ps_os_sema_wait(ps_os_semaphore_t sema)
{
	int ret = -1;

	ret = down_interruptible(sema);
	return ret;
}

int ps_os_sema_signal(ps_os_semaphore_t sema)
{
	up(sema);
	return 0;
}
