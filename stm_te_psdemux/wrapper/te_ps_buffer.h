/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_buffer.h

Declares TE PS buffer object type and operators
******************************************************************************/
#ifndef __TE_PS_BUFFER_H
#define __TE_PS_BUFFER_H

#include <linux/kref.h>
#include <linux/mutex.h>
#include "stm_common.h"
#include "te_object.h"

/* Includes --------------------------------------------------------- */

/* Exported Constants ----------------------------------------------------- */
struct te_buffer_obj {
	int   buf_size;
	uint8_t *buf_start;
	uint32_t buf_phy;
	uint32_t buf_index;
	int wr_offs;
	int rd_offs;
	struct kref refcount;
	void (*release)(struct kref *kref);

	uint32_t signalling_upper_threshold;
	/**< The "fullness" at which the host will signal
	 * * (signals when moving from below to above/equal the threshold)*/
	bool discard_input_on_overflow;
	/**< Set to true (normally), if true HOST must used
	 * * ReadOffset above to determin read pointer */
	uint8_t dma_overflow_flag;
	/**cache copy of  dma overflow flag */
	volatile uint32_t signal_threshold;
	/* marker point at which a buffer signal is
	 * * raised (IRQ) (in terms of "fullness")
	 * B31 used for overflow control */
};

/*Exported functions---------------------------------------------------------*/
void te_buffer_obj_inc(struct te_buffer_obj *obj);
int te_buffer_obj_dec(struct te_buffer_obj *obj);
int te_buffer_allocator(struct te_obj *filter,
			struct te_buffer_obj **buffer_obj);

#define PS_CURRENT_READ_OFFSET 0xFFFFFFFF

#define PS_DMA_INFO_NO_SIGNALLING   (0x7FFFFFFF)
#define PS_DMA_INFO_ALLOW_OVERFLOW  (0x80000000)

#define PS_DMA_INFO_MARK_OVERFLOWED_OVERWRITE (0x01)
/* Bit set (and cleared) to indicate the wr ptr has
 * * overtaken the rd ptr, existing data in buffer is overwritten */
#define PS_DMA_INFO_MARK_OVERFLOWED_DISCARD   (0x02)
/* Bit set (and cleared) to indicate buffer is full
 *  * and input data is being discarded */
#define PS_DMA_INFO_MARK_RESET_OVERFLOW       (0x04)
/* Bit set by the host in dma_end to indicate the
 * * rd ptr has been reset, cleared to ack */
#define PSBufferInOverflowOverwrite(x) \
		(x ^ (PS_DMA_INFO_MARK_OVERFLOWED_OVERWRITE & \
		      ~PS_DMA_INFO_MARK_RESET_OVERFLOW))

#endif   /* #ifndef __TE_PS_BUFFER_H */

/* End of te_ps_buffer.h */
