/******************************************************************************
 * Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.
 *
 * This file is part of the Transport Engine Library.
 *
 * Transport Engine is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * Transport Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The Transport Engine Library may alternatively be licensed under a proprietary
 * license from ST.
 *
 * Source file name : te_ps_utils.h
 *
 * Defines PS Utils specific operations
 * ******************************************************************************/
#ifndef _TE_PS_UTILS_H_
#define _TE_PS_UTILS_H_

#include <linux/rtmutex.h>
#include <linux/semaphore.h>

typedef struct semaphore	*ps_os_semaphore_t;

typedef struct {
	uint32_t buf_index;
	void *data_p;
	struct list_head entry;
} te_parser_message_t;

/*************************************************************************/
/*              Message queue functions                                  */
/*************************************************************************/
int ps_os_sema_signal(ps_os_semaphore_t  sema);
int ps_os_sema_wait(ps_os_semaphore_t  sema);

int ps_os_sema_initialize(ps_os_semaphore_t  *sema,
		uint32_t     initial_count);
int ps_os_sema_terminate(ps_os_semaphore_t  sema);

#endif /*_TE_PS_UTILS_H_*/
