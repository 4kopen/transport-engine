/******************************************************************************
 * Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.
 *
 * This file is part of the Transport Engine Library.
 *
 * Transport Engine is free software; you can redistribute it and/or
 * modify it
 * under the terms of the GNU General Public License version 2 as
 * published by
 * the Free Software Foundation.
 *
 * Transport Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The Transport Engine Library may alternatively be licensed under a
 * proprietary license from ST.
 *
 * Source file name : te_ps_buffer.c
 *
 * Defines PS buffer specific operations
* *****************************************************************************/
#if defined(CONFIG_BPA2)
#include <linux/bpa2.h>
#else
#error "define bpa-2 in kernel config"
#endif
#include "te_ps_buffer.h"
#include "te_ps_output_filter.h"
#include "te_internal_cfg.h"
#include "te_psdemux.h"

/* use TE BPA region for allocation */
#define BPA_REGION "te-buffers"

#define space_to_output_ps_packet(dma_write, dma_next_write, dma_read) \
	(((dma_write >= dma_read) && ((dma_next_write > dma_write) ||\
	(dma_read > dma_next_write))) || \
	((dma_next_write > dma_write) && (dma_read > dma_next_write)))

static inline bool ps_output_allowed(struct te_buffer_obj *buffer_obj,
		unsigned int next_write,
		bool *overflow_update)
{
	bool output_allowed = false; /* default is output not allowed */

	unsigned int dma_write = (unsigned int)buffer_obj->wr_offs;
	unsigned int dma_read = (unsigned int)buffer_obj->rd_offs;
	unsigned int dma_overflow_enable =
		(unsigned int)buffer_obj->signal_threshold &
			PS_DMA_INFO_ALLOW_OVERFLOW;

	if (space_to_output_ps_packet(dma_write, next_write, dma_read)) {
		output_allowed = true;
	} else if (dma_overflow_enable) {
		/* Always allow output in this section */
		output_allowed = true;
		*overflow_update = true;
	}
	return output_allowed;
}

/* message notify decision function by parser to buffer work*/
static int check_for_buffer_level(struct te_buffer_obj *buffer_obj,
		uint32_t new_write)
{
	int32_t fullness_pre, fullness_post;

	uint32_t signal_threshold = buffer_obj->signal_threshold &
					(~PS_DMA_INFO_ALLOW_OVERFLOW);
	uint32_t dma_size	= buffer_obj->buf_size;
	uint32_t dma_read = buffer_obj->rd_offs;
	uint32_t dma_write = buffer_obj->wr_offs;

	fullness_pre = dma_write - dma_read;
	if (dma_write < dma_read)
		fullness_pre = dma_size + fullness_pre;

	fullness_post = (uint32_t)new_write - dma_read;

	if ((uint32_t)new_write < dma_read)
		fullness_post = dma_size + fullness_post;

	pr_debug("Pre %d Post %d signal threshold %d\n",
			fullness_pre,
			fullness_post,
			signal_threshold);
	if (fullness_pre < signal_threshold) {
		if (fullness_post >= signal_threshold)
			/* buffer threshold reached */
			return 1;
	}
	return 1; /* TODO check this */
}

static void buf_release_obj(struct kref *ref)
{
	struct te_buffer_obj *obj = container_of(ref, struct te_buffer_obj,
			refcount);
	if (obj) {
		if (obj->buf_start)
			bpa2_free_pages(bpa2_find_part(BPA_REGION),
					obj->buf_phy);
		kfree(obj);
	}
}
/*!
 *\brief increment use count on object
 */
void te_buffer_obj_inc(struct te_buffer_obj *obj)
{
	kref_get(&obj->refcount);
}

/*!
 *\brief decrement use count on object, frees object if now unused.
 */
int te_buffer_obj_dec(struct te_buffer_obj *obj)
{
	if (!obj)
		return 1;
	return kref_put(&obj->refcount, obj->release);
}

int te_buffer_allocator(struct te_obj *filter,
			struct te_buffer_obj **buffer_obj)
{
	int ret = 0;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(filter);
	struct te_buffer_obj *new_obj;
	struct bpa2_part *part;
	uint32_t pages;

	new_obj = kzalloc(sizeof(*new_obj), GFP_KERNEL);
	if (!new_obj) {
		pr_err("couldn't allocate buffer object\n");
		ret = -ENOMEM;
		goto error;
	}

	pr_debug("creating output filter buffer object\n");

	kref_init(&new_obj->refcount);
	new_obj->release = buf_release_obj;

	if (output->buf_size == 0) {
			new_obj->buf_size = te_cfg.pes_buffer_size;
	} else {
		/* use user defined size */
		new_obj->buf_size = output->buf_size;
	}

	pages = new_obj->buf_size / PAGE_SIZE;
	if (new_obj->buf_size % PAGE_SIZE)
		pages++;
	new_obj->buf_size = pages * PAGE_SIZE;

	part = bpa2_find_part(BPA_REGION);
	if (part == NULL) {
		pr_err("bpa2 not found\n");
		ret = -ENOMEM;
		goto error;
	}
	new_obj->buf_phy = bpa2_alloc_pages(part, pages, 0,
				GFP_KERNEL | __GFP_DMA);
	/* check if bpa is in hi-mem and get virt address*/
	if (new_obj->buf_phy) {
		if (bpa2_low_part(part))
			new_obj->buf_start =
				phys_to_virt(new_obj->buf_phy);
		else
			new_obj->buf_start =
				ioremap_cache(new_obj->buf_phy,
					new_obj->buf_size);
	}
	if (new_obj->buf_phy == 0 || new_obj->buf_start == NULL) {
		pr_err("couldn't allocate memory for buffer\n");
		ret = -ENOMEM;
		goto error;
	}

	new_obj->buf_index = (uint32_t)new_obj->buf_start;

	/* Reset pointers for the new buffer */
	new_obj->rd_offs = new_obj->wr_offs = 0;

	new_obj->signalling_upper_threshold = PS_DMA_INFO_NO_SIGNALLING;
	new_obj->discard_input_on_overflow = true;
	new_obj->dma_overflow_flag = 0;
	new_obj->signal_threshold = PS_DMA_INFO_NO_SIGNALLING;

	output->buffer_obj = new_obj;

	*buffer_obj = new_obj;

	return ret;

error:
	kfree(new_obj);
	return ret;
}

int te_buffer_write_bytes(struct te_ps_out_filter *output,
		unsigned char *output_data,
		uint32_t output_data_size,
		uint32_t *bytes_written,
		int *notify)
{
	int res = 1;
	bool overflow_update = false;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		return res;
	}

	buffer_obj = output->buffer_obj;

	pr_debug(" buffer write bytes buffer_obj %p buffer (0x%08x) size %d\n",
			buffer_obj, buffer_obj->buf_index,
			buffer_obj->buf_size);

	res = mutex_lock_interruptible(&output->obj.lock);
	if (res < 0)
		return res;

	if (output_data_size > 0) {
		unsigned int next_write;
		unsigned int wrap;
		unsigned int dma_write, dma_size, dma_base;

		dma_write = buffer_obj->wr_offs;
		dma_base = (unsigned int)buffer_obj->buf_start;
		dma_size = buffer_obj->buf_size;

		pr_debug("Write %d , base 0x%x, size %d, output_data_size %d, buf index 0x%08x\n",
				dma_write, dma_base, dma_size, output_data_size,
				buffer_obj->buf_index);

		/* This is the whole frame including the header */
		/* check on this */
		next_write = dma_write + output_data_size;

		/* check for wrap */
		if (next_write >= dma_size) {
			pr_debug("PS buffer write: Buffer has wrapped");
			wrap = (next_write > dma_size);
			next_write = next_write - dma_size;
		} else
			wrap = 0;

		if (ps_output_allowed(buffer_obj, next_write,
					&overflow_update)) {
			if (wrap) {
				/* the copy is split from write to end and
				* from start to length */
				pr_debug("PS buffer write: Wrap, so doing 2 copy calls %x",
						dma_write);
				/* setup a transfer to the streamer */
				memcpy((void *)(dma_base + dma_write),
						(void *)output_data,
						dma_size - dma_write);

				memcpy((void *)dma_base,
					(void *)((unsigned int)output_data +
						(dma_size - dma_write)),
					output_data_size -
						(dma_size - dma_write));
			} else {
				pr_debug("PS buffer write: No wrap, so doing 1 copy call %x Output data size %d base 0x%x dest 0x%x",
					dma_write, output_data_size, dma_base,
					(dma_base + dma_write));

				/* setup a transfer to the streamer */
				memcpy((void *)(dma_base + dma_write),
					(void *)output_data,
					output_data_size);
			}
		}
		pr_debug("wr offset %d\n", buffer_obj->wr_offs);
		buffer_obj->wr_offs = next_write;
		pr_debug("updated wr offset %d\n", buffer_obj->wr_offs);

		/* check if buffer threashold has reached a requested level */
		*notify = check_for_buffer_level(buffer_obj, next_write);

		/* We need to update the Overflow status */
		if (buffer_obj->dma_overflow_flag &
				PS_DMA_INFO_MARK_RESET_OVERFLOW) {
			buffer_obj->dma_overflow_flag = 0;

		} else if (overflow_update &&
			buffer_obj->dma_overflow_flag == 0) {
			buffer_obj->dma_overflow_flag =
				PS_DMA_INFO_MARK_OVERFLOWED_OVERWRITE;
			overflow_update = false;
		} else {
			/* indicate buffer overflow */
			buffer_obj->dma_overflow_flag =
				PS_DMA_INFO_MARK_OVERFLOWED_DISCARD;
			res = 0;
		}
	}
	mutex_unlock(&output->obj.lock);
	return res;
}

int te_buffer_status(struct te_ps_out_filter *output,
			uint32_t *bytes_in_buffer_p,
			uint32_t *free_space_p,
			bool *overflowed_flag_p)
{
	int res = 1;
	bool overflowed = false;
	uint8_t dma_overflow_flag;
	struct te_buffer_obj *buffer_obj;
	uint32_t read_offset, write_offset, bytes_in_buffer;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		return res;
	}

	buffer_obj = output->buffer_obj;
	res = mutex_lock_interruptible(&output->obj.lock);
	if (res < 0)
		return res;

	/* check the buffer pointer in case an invalid handle was passed in */
	if (buffer_obj != NULL) {
		if ((void *)buffer_obj->buf_index != NULL) {
			write_offset = buffer_obj->wr_offs;
			read_offset = buffer_obj->rd_offs;
			dma_overflow_flag = buffer_obj->dma_overflow_flag;

			if (!buffer_obj->discard_input_on_overflow) {
				if (PSBufferInOverflowOverwrite(
							dma_overflow_flag)) {
					/* host has not set 'reset overflow'
					* flag (done by moving the read offset),
					* so buffer in overflow state */
					overflowed = true;
				}
			}
			if (overflowed) {
				bytes_in_buffer = buffer_obj->buf_size;
			} else if (write_offset >= read_offset) {
				bytes_in_buffer = write_offset - read_offset;
			} else {
				bytes_in_buffer = buffer_obj->buf_size -
					(read_offset - write_offset);
			}

			if (bytes_in_buffer_p != NULL)
				*bytes_in_buffer_p = bytes_in_buffer;

			if (free_space_p != NULL) {
				if (overflowed) {
					*free_space_p = 0;
				} else if (read_offset > write_offset) {
					*free_space_p =
						read_offset - write_offset;
				} else {
					*free_space_p = buffer_obj->buf_size -
						(write_offset - read_offset);
				}
			}

			if (overflowed_flag_p != NULL) {
				if (dma_overflow_flag &
					(PS_DMA_INFO_MARK_OVERFLOWED_OVERWRITE |
					PS_DMA_INFO_MARK_OVERFLOWED_DISCARD)) {
					*overflowed_flag_p = true;
				} else {
					*overflowed_flag_p = false;
				}
			}
			res = 0;
		}
	}
	mutex_unlock(&output->obj.lock);
	return res;
}

/**
 * @brief Signal the buffer if above or equal to the threshold.

This function posts a message on the signal queue if there is enough data
in the buffer.

@param Buffer_p A pointer to the host buffer structure
@param vDevice_p A pointer to the vDevice of the Buffer

@return A standard st error type...
- ST_NO_ERROR
*/
int te_buffer_signal(struct te_ps_out_filter *output)
{
	int res = 1;
	struct te_psdemux *psdemux;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		return res;
	}
	psdemux = te_psdemux_from_obj(output->obj.parent);
	buffer_obj = output->buffer_obj;
	/* Internal signalling decision.
	TP only signals if fullness changes from below the threshold to
	above/equal to it)
	Host needs to (directly) signal if above the threshold
	(done to reduce interrupt load) */

	/* Updating the QWrite AFTER updating the read pointer is essential. */
	if ((void *)buffer_obj->buf_index != NULL) {
		uint32_t buffer_fullness;

		/* Calculate how full the (circular) buffer is */
		if (buffer_obj->wr_offs >= buffer_obj->rd_offs) {
			buffer_fullness = buffer_obj->wr_offs -
				buffer_obj->rd_offs;
		} else {
			buffer_fullness = buffer_obj->buf_size -
				(buffer_obj->rd_offs -
					buffer_obj->wr_offs);
		}
		if (buffer_fullness >= buffer_obj->signalling_upper_threshold) {
			/*TODO check this needed or not */
			res = te_ps_parser_send_message(buffer_obj->buf_index,
							psdemux,
							false);
		}
	}
	return res;
}

/**
@brief Initialise a DMAInfo entry for signal_threshold and overflow control in
the Transport Processor

This function initialises the specified entry in the Transport Processors
DMAInfo table.

@param buffer_obj buffer_obj to buffer to be set.

@return A standard st error type...

 */
int te_buffer_set_threshold_overflow(struct te_ps_out_filter *output)
{
	unsigned int res = 1;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		return res;
	}
	buffer_obj = output->buffer_obj;
	if ((void *)buffer_obj->buf_index != NULL) {
		uint32_t signal_threshold = 0;

		if (!buffer_obj->discard_input_on_overflow) {
			/* the start pointer is modified to turn off
			* overflow management */
			signal_threshold = PS_DMA_INFO_ALLOW_OVERFLOW;
		}

		/* Has a signal been associated with this buffer?) */
		signal_threshold |= buffer_obj->signalling_upper_threshold &
			(~PS_DMA_INFO_ALLOW_OVERFLOW);
		buffer_obj->signal_threshold = signal_threshold;

		te_buffer_signal(output);/* TODO check if needed */
		res = 0;
	}
	return res;
}

/**
@brief Set Overflow Control on and off

This function control whether data is discarded when the buffer is full or
whether unread data is overwritten.

@param BufferHandle	The Handle of the Buffer
@param discard_input_on_overflow true
	if we should discard the incoming data upon overflow (the default)

@return	A standard st error type...
			- ST_NO_ERROR
 */
int te_buffer_set_overflow_control(struct te_ps_out_filter *output,
					bool discard_input_on_overflow)
{
	int res = 1;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_err("There is no allocated buffer_obj 0x%x\n", res);
		res = -ENOSYS;
		return res;
	}
	buffer_obj = output->buffer_obj;

	res = mutex_lock_interruptible(&output->obj.lock);
	if (res < 0)
		return res;

	te_buffer_obj_inc(buffer_obj);

	buffer_obj->discard_input_on_overflow = discard_input_on_overflow;
	te_buffer_set_threshold_overflow(output);

	te_buffer_obj_dec(buffer_obj);

	mutex_unlock(&output->obj.lock);
	return res;
}

/**
@brief Set the threshold for a buffer.

This function set the signalling threshold for the buffer.
It does not enable signalling. This is done by associating a signal.

@param BufferHandle	The Handle of the Buffer
@param UpperThreshold	The threshold at which signalling will occur
			(if above or equal to)
@return			A standard st error type...
			- ST_NO_ERROR
 */

int te_buffer_set_threshold(struct te_ps_out_filter *output,
				uint32_t upperthreshold)
{
	int res = 1;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		return res;
	}

	buffer_obj = output->buffer_obj;

	/* Limit the UpperTheshold to 0x7fffffff,
	* because the upper bit means something else */
	if (upperthreshold > PS_DMA_INFO_NO_SIGNALLING)
		upperthreshold = PS_DMA_INFO_NO_SIGNALLING;

	res = mutex_lock_interruptible(&output->obj.lock);
	if (res < 0)
		return res;

	buffer_obj->signalling_upper_threshold = upperthreshold;

	te_buffer_set_threshold_overflow(output);
	mutex_unlock(&output->obj.lock);
	return res;
}

int te_buffer_set_read_offset(struct te_ps_out_filter *output,
			uint32_t read_offset)
{
	int res = 0;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		res = -ENOSYS;
		return res;
	}
	buffer_obj = output->buffer_obj;

	/* Only adjust the read offset */
	if (read_offset != PS_CURRENT_READ_OFFSET) {
		if (read_offset >= buffer_obj->buf_size) {
			/* We do not allow the user to put the read pointer
			* outside of the buffer. */
			res = -EINVAL;
		} else {
			if ((void *)buffer_obj->buf_index != NULL)
				buffer_obj->rd_offs = read_offset;

			if (!buffer_obj->discard_input_on_overflow) {
				if ((void *)buffer_obj->buf_index != NULL) {
					/* Check if we need to reset the
					* overflow flag -This controls how
					* getbufferfreelen and testfordata
					* behave */
					if (PSBufferInOverflowOverwrite(
						buffer_obj->dma_overflow_flag)) {
					/* First time read offset been moved
					* since overflow in overwrite case.
					Set reset flag, this flag tells host
					read offset has since overflow and
					buffer is not in overflow state.
					TP will clear overflow stat on this
					flag. Host must not write to DMA
					overflow until TP clears overflow
					flag (avoid race condition) */
					buffer_obj->dma_overflow_flag =
						PS_DMA_INFO_MARK_RESET_OVERFLOW;
					}
				} else {
					/* DMA is not allocated just reset
					* overflow flag in host */
					buffer_obj->dma_overflow_flag = 0;
				}
			}
		}
		/* Internal signalling decision.
		TP only signals if fullness changes from below the threshold
		to above/equal to it)
		Host needs to (directly) signal if above the threshold
		(done to reduce interrupt load) */
		/*SignalBufferIfNecessary(Buffer_p, vDevice_p);*/
		/* TODO check if this needed */
	}
	return res;
}

static int ps_copy_from_circ_buffer(struct te_buffer_obj *buffer_obj,
		uint32_t *read_offset_p,
		uint8_t *destbuffer,
		uint32_t dest_bytestocopy,
		int (*copy_function)(void **, const void *, size_t))
{
	int err = 0;

	if (dest_bytestocopy > 0) {
		uint32_t part1, part2 = 0;

		part1 = buffer_obj->buf_size - *read_offset_p;

		pr_debug(" part1 %d buf size %d dest_bytestocopy %d\n",
			part1, buffer_obj->buf_size, dest_bytestocopy);

		if (part1 > dest_bytestocopy)
			part1 = dest_bytestocopy;
			/* part1 = BytesToRead, part2 = 0 */
		else
			part2 = dest_bytestocopy - part1;
			/* part1 = BytesToEndOfBuffer,
			* part2 = BytesToRead-BytesToEndOfBuffer */

		err = (*copy_function)((void **)&destbuffer,
				&buffer_obj->buf_start[*read_offset_p],
				(size_t) part1);
		/* note destbuffer will be advanced by the copy_function */
		if (0 == err) {
			*read_offset_p += part1;
			if (*read_offset_p == buffer_obj->buf_size)
				*read_offset_p = 0;

			if (part2 > 0) {
				err = (*copy_function)((void **)&destbuffer,
						buffer_obj->buf_start,
						(size_t) part2);
				if (0 == err)
					*read_offset_p = part2;
			}
		}
	}
	return err;
}

int te_buffer_read_bytes(struct te_ps_out_filter *output,
		uint32_t *read_offset_p,
		void *destbuffer1_p,
		uint32_t destsize1,
		int (*copy_function)(void **, const void *, size_t),
		uint32_t *bytes_copied)
{
	int res = 0;
	uint32_t bytes_in_buffer = 0, bytes_to_read = destsize1;
	uint32_t read_offset = 0;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL)
		return -1;

	buffer_obj = output->buffer_obj;

	*read_offset_p = buffer_obj->rd_offs;

	if (*read_offset_p != PS_CURRENT_READ_OFFSET) {
		if (*read_offset_p < buffer_obj->buf_size) {
			read_offset = *read_offset_p;
		} else {
			res = -EINVAL;
			return res;
		}
	}
	/* find out bytes in buffer */
	if (!buffer_obj->discard_input_on_overflow &&
		PSBufferInOverflowOverwrite(buffer_obj->dma_overflow_flag)) {
		/* host has not moved the read offset since overflowed */
		bytes_in_buffer = buffer_obj->buf_size;
	} else {
		/* Normal mode (discard input when full)
		* so pointers make sense */
		if (buffer_obj->wr_offs >= buffer_obj->rd_offs) {
			bytes_in_buffer = buffer_obj->wr_offs -
				buffer_obj->rd_offs;
		} else {
			bytes_in_buffer = buffer_obj->buf_size -
					(buffer_obj->rd_offs -
						buffer_obj->wr_offs);
		}
	}

	if (bytes_in_buffer == 0) {
		pr_debug("no data available to read\n");
		res = -EIO;
		return res;
	}
	ps_copy_from_circ_buffer(buffer_obj, &read_offset, destbuffer1_p,
				bytes_to_read,
				copy_function);

	*bytes_copied = bytes_to_read;
	*read_offset_p = read_offset;

	return res;
}
