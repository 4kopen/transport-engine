/******************************************************************************
 * Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.
 *
 * This file is part of the Transport Engine Library.
 *
 * Transport Engine is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * Transport Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The Transport Engine Library may alternatively be licensed under a
 * proprietary license from ST.
 *
 * Source file name : te_ps_internal.h
 *
 * Defines PS Utils specific operations
 * *************************************************************************/
#ifndef _TE_PS_INTERNAL_H_
#define _TE_PS_INTERNAL_H_

#include "te_psdemux.h"
#include "ps_parser.h"

int te_ps_bwq_create(struct te_psdemux *psdmx);

int te_ps_parser_init(ps_parser_data_t **parser_pp);

void te_ps_parser_reset_params(ps_parser_data_t *parser_p);

int te_ps_parse_frame(struct te_psdemux *psdemux,
		uint8_t *buffer,
		uint32_t buffer_len,
		uint32_t *injected_count);

#endif /*_TE_PS_INTERNAL_H_*/
