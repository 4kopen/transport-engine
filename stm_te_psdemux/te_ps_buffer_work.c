/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_buffer_work.c

Defines PS buffer work specific operations
******************************************************************************/
#include <linux/workqueue.h>
#include <linux/radix-tree.h>
#include <linux/mutex.h>
#include <linux/kthread.h>
#include <linux/jiffies.h>

#include "te_ps_buffer_work.h"
#include "te_psdemux.h"

#define PS_BWQ_IDLE_WAIT 30 /* msec */
#define PS_BWQ_MAX_LOOKUP 128

static int thread_psdmx_bufwd[2] = { SCHED_NORMAL, 0 };
module_param_array_named(thread_PSDMX_BufWd, thread_psdmx_bufwd, int, NULL,
		0644);
MODULE_PARM_DESC(thread_PSDMX_BufWd, "PSDMX-BufWd thread:s(Mode), p(Priority)");

struct te_ps_buffer_work {
	struct te_obj *obj;
	struct te_buffer_obj *buf_obj;
	struct work_struct work;
	bool queued;
	TE_PS_BWQ_WORK func;
};

struct te_ps_buffer_work_queue {
	struct workqueue_struct *wq;
	struct radix_tree_root root;
	struct mutex m;
	struct task_struct *thd;
	unsigned long next_idle;
};

/*!
 * \brief monitor signal and despatch work
 */
static int te_ps_bwq_task(void *data)
{
	struct te_psdemux *psdmx = (struct te_psdemux *)data;
	struct te_ps_buffer_work_queue *bwq = psdmx->bwq;
	struct te_ps_buffer_work *bw;
	te_parser_message_t *message;
	int error = 0;

	while (1) {
		error = ps_os_sema_wait(psdmx->sema);
		mutex_lock(&psdmx->ps_msg_mutex);
		message = list_first_entry(&psdmx->ps_msg_list,
			te_parser_message_t, entry);
		pr_debug("message received %p with buf index 0x%08x\n",
				message,
				message->buf_index);
		list_del(&message->entry);
		mutex_unlock(&psdmx->ps_msg_mutex);

		if (message->buf_index == 0) {
			pr_err("zero size message fatal error\n");
			continue;
		}

		if (message->buf_index == -1) {
			kfree(message);
			while (!kthread_should_stop()) {
				schedule_timeout_interruptible(100);
				/* 100 jiffies */
			}
			pr_debug("terminating thread\n");
			return 0;
		}
		if (mutex_lock_interruptible(&bwq->m) != 0)
			continue;

		bw = radix_tree_lookup(&bwq->root, message->buf_index);
		if (bw) {
			pr_debug("Found entry for buffer 0x%08x\n",
					message->buf_index);
			bw->queued = true;
			queue_work(bwq->wq, &bw->work);
		} else {
			pr_debug("Buffer 0x%08x not in radix tree\n",
				message->buf_index);
		}
		mutex_unlock(&bwq->m);
		kfree(message);
	}
	return 0;
}

/*!
 * \brief despatch work to object handlers
 */
static void te_ps_bwq_despatch(struct work_struct *work)
{
	struct te_ps_buffer_work *bw
		= container_of(work, struct te_ps_buffer_work, work);

	pr_debug("despatching buffer 0x%08x\n", bw->buf_obj->buf_index);
	bw->func(bw->obj, bw->buf_obj);
	bw->queued = false;
	pr_debug("despatched buffer 0x%08x\n", bw->buf_obj->buf_index);

}

/*!
 * \brief create a buffer work queue
 * \param bwq pointer to store created work queue in
 * \param q the work queue to use (NULL to create new)
 * \returns 0 on success, otherwise errno
 */
int te_ps_bwq_create(struct te_psdemux *psdmx)
{
	int err = 0;
	struct te_ps_buffer_work_queue *new_bwq;

	struct sched_param		param;

	if (psdmx->work_que == NULL) {
		pr_err("must provide work queue\n");
		return -EINVAL;
	}

	new_bwq = kzalloc(sizeof(*new_bwq), GFP_KERNEL);
	if (new_bwq == NULL) {
		pr_err("no memory for buffer work queue\n");
		err = -ENOMEM;
		goto error;
	}
	new_bwq->wq = psdmx->work_que;
	new_bwq->next_idle = jiffies + msecs_to_jiffies(PS_BWQ_IDLE_WAIT);

	INIT_RADIX_TREE(&new_bwq->root, GFP_KERNEL);
	mutex_init(&new_bwq->m);

	new_bwq->thd = kthread_run(te_ps_bwq_task,
					psdmx,
				"PSDMX-BufWd");

	if (!new_bwq->thd) {
		pr_err("couldn't spin PS BWQ thread\n");
		err = -ENOMEM;
		goto error;
	}
	/* switch to real time scheduling (if requested) */
	if (thread_psdmx_bufwd[1]) {
		param.sched_priority = thread_psdmx_bufwd[1];
		if (0 != sched_setscheduler(new_bwq->thd,
						thread_psdmx_bufwd[0],
						&param)) {
			pr_err("FAILED to set scheduling parameters to priority %d Mode :(%d)\n",
						thread_psdmx_bufwd[1],
						thread_psdmx_bufwd[0]);
		}
	}

	psdmx->bwq = new_bwq;

	return 0;

error:
	kfree(new_bwq);
	return err;
}

/*!
 * \brief destroy a buffer work queue
 * \param bwq to destroy
 */
int te_ps_bwq_destroy(struct te_ps_buffer_work_queue *bwq)
{
#if (LINUX_VERSION_CODE < KERNEL_VERSION(3, 4, 0))
	struct te_ps_buffer_work *bw;
	unsigned int nr_slots = PS_BWQ_MAX_LOOKUP;
	unsigned int i;

	void **slots[PS_BWQ_MAX_LOOKUP];

	if (!bwq)
		return -EINVAL;

	kthread_stop(bwq->thd);

	/* Iterate through the tree, disassociate buffers and cancel any work */
	while (nr_slots == PS_BWQ_MAX_LOOKUP) {
		nr_slots = radix_tree_gang_lookup_slot(
					&bwq->root,
					slots,
#if (LINUX_VERSION_CODE > KERNEL_VERSION(3, 0, 0))
					NULL,
#endif
					0,
					PS_BWQ_MAX_LOOKUP);

		for (i = 0; i < nr_slots; i++) {
			bw = radix_tree_deref_slot(slots[i]);

			pr_debug("cancelling work for buffer 0x%08x\n",
				bw->buf_obj->buf_index);

			cancel_work_sync(&bw->work);

			pr_debug("cancelled work for buffer 0x%08x\n",
				bw->buf_obj->buf_index);

			te_buffer_obj_dec(bw->buf_obj);

			kfree(bw);
		}
	}
	return 0;
#else
	struct te_ps_buffer_work *bw;
	struct radix_tree_iter iter;
	void **slot;

	if (!bwq)
		return -EINVAL;

	kthread_stop(bwq->thd);

	/* Iterate through the tree, disassociate buffers and cancel any work */
	radix_tree_for_each_slot(slot, &bwq->root, &iter, 0) {
		bw = radix_tree_deref_slot(slot);

		pr_debug("cancelling work for buffer 0x%08x\n",
				bw->buf_obj->buf_index);

		cancel_work_sync(&bw->work);

		pr_debug("cancelled work for buffer 0x%08x\n",
				bw->buf_obj->buf_index);

		te_buffer_obj_dec(bw->buf_obj);
		kfree(bw);
	}

	kfree(bwq);
	return 0;
#endif
}

/*!
 * \brief register an object, its buffer and the work callback
 * \param bwq the work queue to register on
 * \param obj the TE object associated with this work package
 * \param buffer the buffer to monitor
 * \param func the function that will be called when work is to be done
 */
int te_ps_bwq_register(struct te_ps_buffer_work_queue *bwq,
			struct te_obj *obj,
			struct te_buffer_obj *buffer_obj,
			TE_PS_BWQ_WORK func)
{
	int err = 0;
	struct te_ps_buffer_work *bw;

	pr_debug("registering obj %p buffer (0x%08x) func %p\n",
			obj, buffer_obj->buf_index, func);

	/* Check if this buffer already has the same work associated. If so
	* ignore. If the requested work is different print warning but keep
	* original work function
	*
	* TODO: This shouldn't be needed when the connection model is reworked
	* to prevent potential multiple associations of work with buffers */

	if (mutex_lock_interruptible(&bwq->m) != 0)
		return -EINTR;

	bw = radix_tree_lookup(&bwq->root, buffer_obj->buf_index);
	mutex_unlock(&bwq->m);
	if (bw) {
		if (bw->func != func) {
			pr_warn("Buffer 0x%x already has a work function\n",
					buffer_obj->buf_index);
			return -EEXIST;
		} else {
			pr_debug("Buffer 0x%x already has same work func\n",
					buffer_obj->buf_index);
			return 0;
		}
	}
	/* Create and initialise the buffer work struct */
	bw = kzalloc(sizeof(*bw), GFP_KERNEL);
	if (!bw) {
		pr_err("no memory for work\n");
		return -ENOMEM;
	}
	te_buffer_obj_inc(buffer_obj);

	bw->obj = obj;
	bw->buf_obj = buffer_obj;
	bw->func = func;
	INIT_WORK(&bw->work, te_ps_bwq_despatch);

	pr_debug("registering buffer_obj %p buffer (0x%08x) func %p\n",
			buffer_obj, buffer_obj->buf_index, func);

	/* Store the work struct in the tree, and associate the buffer
	* to signal */
	if (mutex_lock_interruptible(&bwq->m) != 0)
		goto error_mtx;

	err = radix_tree_insert(&bwq->root, buffer_obj->buf_index, bw);
	mutex_unlock(&bwq->m);

	if (err != 0) {
		pr_err("unable to insert in radix tree\n");
		goto error;
	}

	return 0;

error:
	/*
	* Not sure what to do if the mutex is interrupted...
	* We'll leave a hanging entry in the list and an associated signal,
	* but hopefully bwq destroy will look after it later.
	*/
	if (mutex_lock_interruptible(&bwq->m) == 0) {
		radix_tree_delete(&bwq->root, buffer_obj->buf_index);
		mutex_unlock(&bwq->m);
	}

error_mtx:
	kfree(bw);
	te_buffer_obj_dec(buffer_obj);

	return err;
}

/*!
 * \brief unregister a buffer and stop monitoring it
 * \param bwq the work queue to unregister from
 * \param buffer the buffer to unregister
 * \returns non-zero if buffer was freed
 */
int te_ps_bwq_unregister(struct te_ps_buffer_work_queue *bwq,
			struct te_buffer_obj *buffer_obj)
{
	struct te_ps_buffer_work *bw = NULL;
	int ret = 0;

	if (mutex_lock_interruptible(&bwq->m) != 0)
		return 0; /* Return 0 on error due to buffer ref'ing */

	pr_debug("unregistering buffer obj %p (0x%08x)\n", buffer_obj,
			buffer_obj->buf_index);

	bw = radix_tree_lookup(&bwq->root,
				buffer_obj->buf_index);
	if (bw) {
		cancel_work_sync(&bw->work);
		radix_tree_delete(&bwq->root,
				buffer_obj->buf_index);
		ret = te_buffer_obj_dec(bw->buf_obj);

		kfree(bw);

		pr_debug("unregistered buffer 0x%08x\n", buffer_obj->buf_index);
	}
	mutex_unlock(&bwq->m);

	return ret;
}
