/******************************************************************************
Copyright (C) 2012, 2013 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_buffer_work.h

Defines buffer work queue operations
******************************************************************************/
#ifndef __TE_PS_BUFFER_WORK
#define __TE_PS_BUFFER_WORK

#include "te_object.h"
#include "te_ps_buffer.h"

struct te_ps_buffer_work_queue;

int te_ps_bwq_destroy(struct te_ps_buffer_work_queue *bwq);

typedef void (*TE_PS_BWQ_WORK)(struct te_obj *, struct te_buffer_obj *);

int te_ps_bwq_register(struct te_ps_buffer_work_queue *bwq,
			struct te_obj *obj,
			struct te_buffer_obj *buffer,
			TE_PS_BWQ_WORK func);

int te_ps_bwq_unregister(struct te_ps_buffer_work_queue *bwq,
			struct te_buffer_obj *buffer);

#endif
