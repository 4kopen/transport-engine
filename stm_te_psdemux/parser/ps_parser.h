/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : ps_parser.h

Defines Parser specific operations
******************************************************************************/
#ifndef __PS_PARSER_H
#define __PS_PARSER_H

#include "te_internal_cfg.h"
/* Constants --------------------------------------- */
#define BITS_PER_BYTE		8
#define PS_BYTES_PER_SEC	50

#define MIN_PES_HEADER_LENGTH	24
#define PS_PACK_HEADER_LENGTH	14

/*Program Stream System Header contains a 12 byte fixed portion followed by
any number of 3-byte stream_bound entries*/
#define PS_SYS_HEADER_LENGTH	11 /* excluding 4 byte start code (8 + 3)*/

#define PES_TYPE_PACK_START	0xBA
#define PES_TYPE_SYS_HEADER	0xBB
#define PES_TYPE_program_stream_map	0xBC
#define PES_TYPE_PRIVATE_STREAM_1	0xBD
#define PES_TYPE_PADDING_STREAM		0xBE
#define PES_TYPE_PRIVATE_STREAM_2	0xBF
#define PES_DEFAULT_SUB_STREAM_ID	0x80

#define PS_STREAM_ID_MAX_VALUE		0xff
#define PS_STREAM_ID_VIDEO_MIN		0xE0
#define PS_STREAM_ID_VIDEO_MAX		0xEF
#define PS_STREAM_ID_AUDIO_MIN		0xC0
#define PS_STREAM_ID_AUDIO_MAX		0xDF

/* this is after system header length */
#define PS_SYS_HDR_STREAM_ID_OFFSET	0x7

typedef enum {
	PES_PH_SYNC_0_00, /* pack header start code*/
	PES_PH_SYNC_1_00,
	PES_PH_SYNC_2_01,
	PES_PH_SYNC_3_BA,
	PES_PH_BUILDING_HDR,
	PES_PH_SKIP_STUFFING,
	PES_SH_SYNC_0_00,/* sys header start code*/
	PES_SH_SYNC_1_00,
	PES_SH_SYNC_2_01,
	PES_SH_SYNC_3_BB,
	PES_SH_BUILDING_HDR,
	PES_SH_SKIP_HDR,
	PES_SYNC_0_00,
	PES_SYNC_1_00,
	PES_SYNC_2_01,
	PES_SYNC_3_STREAMID,
	PES_PACKET_LENGTH_1,
	PES_PACKET_LENGTH_2,
	PES_PACKET_SKIP_FLAG1,
	PES_PACKET_SKIP_FLAG2,
	PES_PACKET_PES_HDR_LENGTH,
	PES_PACKET_SKIP_PES_HDR,
	PES_PACKET_HANDLE_SUB_STREAM_ID,
	PES_BUILDING_HDR,
	PES_BUILDING,
	PES_SKIP
} ps_parser_state_t;


typedef struct {
	uint8_t start_code[4];
	uint16_t header_length;
	uint8_t header_length1;
	uint8_t header_length2;
} ps_system_header_t;

typedef struct ps_parser_data_s {
	uint8_t			pes_packet_length1;
	uint8_t			pes_packet_length2;
	uint8_t			pack_header_parsed;
	uint8_t			pack_bytes_to_skip;
	uint8_t			sys_bytes_to_skip;
	uint8_t			sys_stream_id_offset;
	uint8_t			current_stream_id;
	/* Used to store length of pes hdr */
	uint8_t			pes_header_length;
	uint8_t			pes_header[MIN_PES_HEADER_LENGTH];
	/* Used to store the start of a split PES header */
	uint8_t			pes_header_size;
	/* Used to store the number of split pes header bytes storesd */
	uint8_t			pack_header_bytes[PS_PACK_HEADER_LENGTH];
	/* Used to store the split pack header*/
	uint8_t			pack_header_size;
	/* Used to store the number of split pack hdr bytes storesd */
	uint8_t			sys_header_size;
	/* Used to store the number of split sys header bytes storesd */
	ps_parser_state_t	state;
	uint32_t		pes_packet_length;
	uint32_t		pes_remain_len;
	stm_te_psdemux_pack_header_t	pack_header;
	ps_system_header_t	sys_header;
	uint8_t			current_sub_stream_id;
} ps_parser_data_t;

typedef struct {
	ps_parser_data_t	*parser;
	void			*memory_start;
	uint32_t		size;
	uint32_t		offset;
	uint8_t			value;
} ps_parser_params_t;

int te_ps_parser_send_message(uint32_t buffer_index,
				void *data_p,
				bool at_front);

#endif /*  __PS_PARSER_H */
