/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : ps_parser.c

Defines base output filter specific operations
******************************************************************************/
#include <linux/list.h>

#include "te_psdemux.h"
#include "te_ps_output_filter.h"
#include "te_ps_stream_filter.h"

void te_ps_parser_reset_params(ps_parser_data_t *parser_p)
{
	parser_p->pes_packet_length1 = 0;
	parser_p->pes_packet_length2 = 0;
	parser_p->state = PES_PH_SYNC_0_00;
	parser_p->pes_packet_length = 0;
	parser_p->pes_remain_len = 0;
	parser_p->current_stream_id = 0;
	parser_p->current_sub_stream_id = PES_DEFAULT_SUB_STREAM_ID;
	memset(&parser_p->pack_header, 0x00,
			sizeof(stm_te_psdemux_pack_header_t));

	memset(&parser_p->sys_header, 0x00,
			sizeof(ps_system_header_t));
	parser_p->pack_header_parsed = 0;
	parser_p->pack_bytes_to_skip = 0;
	parser_p->sys_bytes_to_skip = 0;

	memset(&parser_p->pes_header, 0x00,
			sizeof(parser_p->pes_header));
	parser_p->pes_header_size = 0;
	memset(&parser_p->pack_header_bytes, 0x00,
			sizeof(parser_p->pack_header_bytes));
	parser_p->pack_header_size = 0;

	parser_p->sys_header_size = 0;
	parser_p->sys_stream_id_offset = 0;

}

int te_ps_parser_init(ps_parser_data_t **parser_pp)
{
	/*Init the Required Data Strcuture for the Parser */
	*parser_pp = kzalloc(sizeof(ps_parser_data_t), GFP_KERNEL);

	if (*parser_pp == NULL) {
		pr_err("Could not allocate for PS parser");
		return -ENOMEM;
	}
	te_ps_parser_reset_params(*parser_pp);

	return 0;
}

int te_ps_parser_send_message(uint32_t buffer_index,
		void *data_p,
		bool at_front)
{
	struct te_psdemux *psdemux;
	te_parser_message_t *message;

	message = kzalloc(sizeof(te_parser_message_t), GFP_KERNEL);
	if (!message) {
		pr_err("message NULL\n");
		return -ENOMEM;
	}

	message->buf_index = buffer_index;
	message->data_p = data_p;

	INIT_LIST_HEAD(&message->entry);

	psdemux = (struct te_psdemux *)data_p;
	mutex_lock(&psdemux->ps_msg_mutex);
	list_add_tail(&message->entry, &psdemux->ps_msg_list);
	mutex_unlock(&psdemux->ps_msg_mutex);

	ps_os_sema_signal(psdemux->sema);

	return 0;
}

static inline void parser_handle_ph_sync0(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	if (localparams_p->value == 0x00) {
		parser_p->pack_header_bytes[parser_p->pack_header_size] =
			localparams_p->value;
		parser_p->pack_header_size++;

		parser_p->state = PES_PH_SYNC_1_00;
	}
	localparams_p->offset++;
}

static inline void parser_handle_ph_sync1(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->state = PES_PH_SYNC_0_00;

	if (localparams_p->value == 0x00) {
		parser_p->pack_header_bytes[parser_p->pack_header_size] =
			localparams_p->value;
		parser_p->pack_header_size++;

		parser_p->state = PES_PH_SYNC_2_01;
	}
	localparams_p->offset++;
}

static inline void parser_handle_ph_sync2(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->state = PES_PH_SYNC_0_00;

	if (localparams_p->value == 0x01) {
		parser_p->pack_header_bytes[parser_p->pack_header_size] =
					localparams_p->value;
		parser_p->pack_header_size++;

		parser_p->state = PES_PH_SYNC_3_BA;
	} else if (localparams_p->value == 0x00) {
		parser_p->state = PES_PH_SYNC_2_01;
	}
	localparams_p->offset++;
}

static inline void parser_packheader_process(ps_parser_data_t *parser_p)
{
	uint8_t bytes[PS_PACK_HEADER_LENGTH - 4];
	/* this excludes 4 start code bytes */

	stm_te_psdemux_pack_header_t ps_pack_header;

	pr_debug("state %d\n", parser_p->state);

	if (parser_p->pack_header_parsed) {
		memcpy(&bytes, &parser_p->pack_header_bytes[4],
			sizeof(parser_p->pack_header_bytes) - 4);

		ps_pack_header.system_clock_ref_base =
			(((bytes[0] & 0x38) << 27) |
			((bytes[0] & 0x03) << 28) |
			((bytes[1] & 0xFF) << 20) |
			((bytes[2] & 0xF8) << 12) |
			((bytes[2] & 0x03) << 13) |
			((bytes[3] & 0xFF) << 5) |
			((bytes[4] & 0xF8) >> 3));

		ps_pack_header.system_clock_ref_ext =
			(((bytes[4] & 0x03) << 7) |
			((bytes[5] & 0xFE) >> 1));

		pr_debug("scr=%llx and scr_ext=%x\n",
				ps_pack_header.system_clock_ref_base,
				ps_pack_header.system_clock_ref_ext);

		ps_pack_header.program_mux_rate = (((bytes[6] & 0xFF) << 14) |
			((bytes[7] & 0xFF) << 6) |
			((bytes[8] & 0xFC) >> 2));

		ps_pack_header.program_mux_rate =
				(ps_pack_header.program_mux_rate
					* PS_BYTES_PER_SEC
					* BITS_PER_BYTE) / SZ_1K;/*in kbps*/

		ps_pack_header.pack_stuffing_length = (bytes[9] & 0x07);

		memcpy(&parser_p->pack_header, &ps_pack_header,
			sizeof(stm_te_psdemux_pack_header_t));
	}
}

static inline void parser_handle_ph_sync3(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	if (localparams_p->value == PES_TYPE_PACK_START) {
		parser_p->pack_header_bytes[parser_p->pack_header_size] =
					localparams_p->value;
		parser_p->pack_header_size++;

		localparams_p->offset++;
		parser_p->state = PES_PH_BUILDING_HDR;

	} else if (localparams_p->value == 0x00) {
		parser_p->state = PES_PH_SYNC_0_00;
	} else {
		/* packet starting without PH/SH */
		if (parser_p->pack_header_parsed == 1) {
			parser_p->pes_header[0] = 0x00;
			parser_p->pes_header[1] = 0x00;
			parser_p->pes_header[2] = 0x01;
			parser_p->pes_header_size = 3;
			parser_p->state = PES_SYNC_3_STREAMID;
		}
	}
}

static inline void parser_handle_pack_header_bytes(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{

	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->pack_header_bytes[parser_p->pack_header_size] =
						localparams_p->value;
	parser_p->pack_header_size++;

	if (parser_p->pack_header_size == PS_PACK_HEADER_LENGTH) {
		parser_p->pack_header_parsed = 1;
		parser_packheader_process(parser_p);

		parser_p->state = PES_PH_SKIP_STUFFING;

		/* the pack header is 14 bytes = 4 + 10 */
		/* Maximum 8 bytes of stuffing possible
		* these to be discarded by decoder */
		parser_p->pack_bytes_to_skip =
				(parser_p->pack_header.pack_stuffing_length);
	}
	localparams_p->offset++;
}

static inline void parser_handle_sh_sync0(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	if (localparams_p->value == 0x00)
		parser_p->state = PES_SH_SYNC_1_00;

	localparams_p->offset++;
}

static inline void parser_handle_sh_sync1(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);
	parser_p->state = PES_SH_SYNC_0_00;

	if (localparams_p->value == 0x00)
		parser_p->state = PES_SH_SYNC_2_01;

	localparams_p->offset++;
}
static inline void parser_handle_sh_sync2(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->state = PES_SH_SYNC_0_00;
	if (localparams_p->value == 0x01) {
		parser_p->state = PES_SH_SYNC_3_BB;
		pr_debug("Parser state -> PES_SH_SYNC_3_BB\n");

	} else if (localparams_p->value == 0x00) {
		parser_p->state = PES_PH_SYNC_1_00;
	} else {
		parser_p->state = PES_PH_SYNC_0_00;
	}
	localparams_p->offset++;

}
static inline void parser_handle_sh_sync3(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->state = PES_SH_SYNC_0_00;

	if (localparams_p->value == PES_TYPE_SYS_HEADER) {
		parser_p->state = PES_SH_BUILDING_HDR;
		localparams_p->offset++;

	} else if (localparams_p->value == 0x00) {
		parser_p->state = PES_SH_SYNC_3_BB;
		localparams_p->offset++;
		/* to be checked */
	} else {
		pr_debug("optional system header absent offset %d value %d\n",
				localparams_p->offset, localparams_p->value);

		if (parser_p->pack_header_parsed == 1) {
			parser_p->pes_header[0] = 0x00;
			parser_p->pes_header[1] = 0x00;
			parser_p->pes_header[2] = 0x01;
			parser_p->pes_header_size = 3;
			parser_p->state = PES_SYNC_3_STREAMID;
		}
	}
}

static inline void parser_handle_sys_header_bytes(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	if (parser_p->sys_header_size == 0) {
		parser_p->sys_header.header_length1 = localparams_p->value;
		parser_p->sys_header_size++;

	} else if (parser_p->sys_header_size == 1) {
		parser_p->sys_header.header_length2 = localparams_p->value;
		parser_p->sys_header.header_length =
			(parser_p->sys_header.header_length1 << 8) |
				(parser_p->sys_header.header_length2);

		/* offset should be increased by 2 bytes defining
		* header length + header length bytes */
		parser_p->sys_bytes_to_skip =
				parser_p->sys_header.header_length;
		parser_p->state = PES_SH_SKIP_HDR;
	}
	localparams_p->offset++;
}

static inline void parser_handle_pes_sync0(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	if (localparams_p->value == 0x00) {
		parser_p->state = PES_SYNC_1_00;
		parser_p->pes_header_size = 0;
		parser_p->pes_header[parser_p->pes_header_size] =
					localparams_p->value;
		parser_p->pes_header_size++;
	}
	localparams_p->offset++;
}

static inline void parser_handle_pes_sync1(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->state = PES_SYNC_0_00;

	if (localparams_p->value == 0x00) {
		parser_p->state = PES_SYNC_2_01;
		parser_p->pes_header[parser_p->pes_header_size] =
							localparams_p->value;
		parser_p->pes_header_size++;
		localparams_p->offset++;
	}
}
static inline void parser_handle_pes_sync2(ps_parser_data_t *parser_p,
					ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->state = PES_SYNC_0_00;

	if (localparams_p->value == 0x01) {
		parser_p->state = PES_SYNC_3_STREAMID;
		parser_p->pes_header[parser_p->pes_header_size] =
						localparams_p->value;
		parser_p->pes_header_size++;

	} else if (localparams_p->value == 0x00) {
		parser_p->state = PES_SYNC_2_01;
	}
	localparams_p->offset++;
}

static inline void parser_handle_pes_sync3(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	uint8_t value = localparams_p->value;

	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	if (((value >= PS_STREAM_ID_VIDEO_MIN) &&
		(value <= PS_STREAM_ID_VIDEO_MAX)) ||
		((value >= PS_STREAM_ID_AUDIO_MIN) &&
		(value <= PS_STREAM_ID_AUDIO_MAX)) ||
		(value == PES_TYPE_PRIVATE_STREAM_1) ||
		(value == PES_TYPE_PRIVATE_STREAM_2) ||
		(value == PES_TYPE_PADDING_STREAM)) {

		parser_p->state = PES_PACKET_LENGTH_1;
		parser_p->pes_header[parser_p->pes_header_size] =
					localparams_p->value;
		parser_p->pes_header_size++;
		parser_p->current_stream_id = value;
	} else {
		pr_err("invalid stream id 0x%x\n", value);
		parser_p->state = PES_SYNC_0_00;
	}
	localparams_p->offset++;
}

static inline void parser_handle_pes_packetlength1(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->pes_packet_length1 = localparams_p->value;
	parser_p->pes_header[parser_p->pes_header_size] =
					localparams_p->value;
	parser_p->pes_header_size++;

	parser_p->state = PES_PACKET_LENGTH_2;
	localparams_p->offset++;
}

static inline void parser_handle_pes_packetlength2(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->pes_header[parser_p->pes_header_size] =
					localparams_p->value;
	parser_p->pes_header_size++;

	parser_p->pes_packet_length2 = localparams_p->value;
	parser_p->pes_packet_length =
			(parser_p->pes_packet_length1 << 8) +
				parser_p->pes_packet_length2;

	parser_p->state = PES_BUILDING_HDR;
	parser_p->pes_remain_len = parser_p->pes_packet_length;
	if (parser_p->current_stream_id == PES_TYPE_PRIVATE_STREAM_1) {
		pr_debug("Handle private data");
		parser_p->state = PES_PACKET_SKIP_FLAG1;
	}
	localparams_p->offset++;
}

static inline void parser_skip_and_copy_byte(ps_parser_data_t *parser_p,
				ps_parser_params_t *localparams_p)
{
	pr_debug("state %d value 0x%x used size %d\n", parser_p->state,
				localparams_p->value, localparams_p->offset);

	parser_p->pes_header[parser_p->pes_header_size] = localparams_p->value;
	parser_p->pes_header_size++;
	parser_p->pes_remain_len--;
	localparams_p->offset++;
}

static bool is_sub_stream_id_valid(struct te_psdemux *psdemux,
					uint32_t sub_stream_id)
{
	bool valid = false;
	struct te_stream_filter *input;

	list_for_each_entry(input, &psdemux->stream_filters, obj.lh) {
		if (input->obj.type == TE_OBJ_PS_STREAM_FILTER) {
			if (input->sub_stream_id != sub_stream_id) {
				valid = false;
			} else {
				valid = true;
				psdemux->parser_p->current_sub_stream_id =
								sub_stream_id;
				break;
			}
		}
	}
	return valid;
}

static bool is_strem_id_valid(struct te_psdemux *psdemux)
{
	bool valid = false;
	struct te_stream_filter *input;

	list_for_each_entry(input, &psdemux->stream_filters, obj.lh) {
		if (input->obj.type == TE_OBJ_PS_STREAM_FILTER) {
			if (input->stream_id !=
					psdemux->parser_p->current_stream_id) {
				valid = false;
			} else {
				valid = true;
				break;
			}
		}
	}
	return valid;
}
static int te_ps_parser_write_data(struct te_psdemux *psdemux,
			uint8_t *buffer_p,
			uint32_t bytes_to_write,
			uint32_t offset,
			uint32_t *bytes_written_p,
			struct te_stream_filter *input)
{
	int index = 0;
	struct te_ps_out_filter *output;
	struct te_obj *of;
	int buffer_threshold_reached = 0;
	int ret = 0;

	for (index = 0; index < PS_MAX_INT_CONNECTIONS; index++) {
		of = input->output_filters[index];

		if (of != NULL) {
			if (of->type == TE_OBJ_PS_PES_FILTER) {
				output = te_ps_out_filter_from_obj(of);
				pr_debug("bytes to write %d offset %d(obj:%p)\n",
						bytes_to_write,
						offset,
						of);

				ret = te_buffer_write_bytes(output,
					&buffer_p[offset],
					bytes_to_write,
					bytes_written_p,
					&buffer_threshold_reached);

				if (ret != 0) {
					pr_err("Error in write_bytes:%d obj:%p\n",
						ret, of);
					return ret;
				}

				if (buffer_threshold_reached == 1)
					ret = te_ps_parser_send_message(
					output->buffer_obj->buf_index,
					psdemux,
					false);
			}
		}
	}
	return ret;
}

static int te_ps_output_data(struct te_psdemux *psdemux,
			uint8_t *buffer_p,
			uint32_t bytes_to_write,
			uint32_t offset,
			uint32_t *bytes_written_p)
{
	int ret = 0;

	struct te_stream_filter *input;

	list_for_each_entry(input, &psdemux->stream_filters, obj.lh) {
	if (input->obj.type == TE_OBJ_PS_STREAM_FILTER
		&& input->stream_id ==
			psdemux->parser_p->current_stream_id) {

		/* Only Private stream 1 can have sub stream ids,
		* Sub-stream id should also match in case of priv stream id 1*/
		if ((psdemux->parser_p->current_stream_id ==
			PES_TYPE_PRIVATE_STREAM_1
			&& psdemux->parser_p->current_sub_stream_id ==
			input->sub_stream_id)
			|| (psdemux->parser_p->current_stream_id !=
			PES_TYPE_PRIVATE_STREAM_1)){

			ret = te_ps_parser_write_data(psdemux,
				buffer_p,
				bytes_to_write,
				offset,
				bytes_written_p,
				input);
			}
		}
	}
	return ret;
}

static int te_ps_output_scr_data(struct te_psdemux *psdemux,
			uint8_t *buffer_p,
			uint32_t bytes_to_write,
			uint32_t offset,
			uint32_t *bytes_written_p)
{
	int ret = 0;
	int buffer_threshold_reached = 0;

	struct te_ps_out_filter *output;
	ps_parser_data_t *parser_p = (ps_parser_data_t *)psdemux->parser_p;

	list_for_each_entry(output, &psdemux->out_filters, obj.lh) {
		if (output->obj.type == TE_OBJ_PS_SCR_FILTER) {
			pr_debug("state %d SCR found\n",
				parser_p->state);

				ret = te_buffer_write_bytes(output,
					&buffer_p[offset],
					bytes_to_write,
					bytes_written_p,
					&buffer_threshold_reached);

				if (ret != 0)
					return ret;

				if (buffer_threshold_reached == 1)
					ret = te_ps_parser_send_message(
					output->buffer_obj->buf_index,
					psdemux,
					false);
		}
	}
	return ret;
}

int te_ps_parse_frame(struct te_psdemux *psdemux,
		uint8_t *buffer,
		uint32_t buffer_len,
		uint32_t *injected_count)
{
	int ret = 0;
	uint32_t pes_start_offset = 0;
	uint32_t bytes_w_p = 0;
	uint8_t *pos = (U8 *)buffer;
	ps_parser_data_t *parser_p = (ps_parser_data_t *)psdemux->parser_p;
	ps_parser_params_t localparams = {parser_p, buffer, buffer_len,
			0};

	while (localparams.offset < localparams.size) {
		localparams.value = pos[localparams.offset];
		switch (parser_p->state) {
		case PES_PH_SYNC_0_00:
			parser_handle_ph_sync0(parser_p,
					&localparams);
			break;

		case PES_PH_SYNC_1_00:
			parser_handle_ph_sync1(parser_p,
					&localparams);
			break;

		case PES_PH_SYNC_2_01:
			parser_handle_ph_sync2(parser_p,
					&localparams);
			break;

		case PES_PH_SYNC_3_BA:
			parser_handle_ph_sync3(parser_p,
					&localparams);
			break;

		case PES_PH_BUILDING_HDR:
			parser_handle_pack_header_bytes(parser_p,
					&localparams);
			break;

		case PES_PH_SKIP_STUFFING:
			if (parser_p->pack_bytes_to_skip != 0) {
				localparams.offset++;
				parser_p->pack_bytes_to_skip--;
			} else {
				if (parser_p->pack_header_parsed) {
					psdemux->program_mux_rate =
					parser_p->pack_header.program_mux_rate;
					/*in kbps*/
					bytes_w_p = 0;
					ret = te_ps_output_scr_data(psdemux,
					(uint8_t *)parser_p->pack_header_bytes,
					(uint32_t)
					sizeof(parser_p->pack_header_bytes) - 8,
						4,
						&bytes_w_p);
				}
				parser_p->state = PES_SH_SYNC_0_00;
			}
			break;

		case PES_SH_SYNC_0_00:
			parser_handle_sh_sync0(parser_p,
					&localparams);
			break;

		case PES_SH_SYNC_1_00:
			parser_handle_sh_sync1(parser_p,
					&localparams);
			break;

		case PES_SH_SYNC_2_01:
			parser_handle_sh_sync2(parser_p,
					&localparams);
			break;

		case PES_SH_SYNC_3_BB:
			parser_handle_sh_sync3(parser_p,
					&localparams);
			break;

		case PES_SH_BUILDING_HDR:
			parser_handle_sys_header_bytes(parser_p,
					&localparams);
			break;

		case PES_SH_SKIP_HDR:
			if (parser_p->sys_bytes_to_skip != 0) {
				localparams.offset++;
				parser_p->sys_stream_id_offset++;
				parser_p->sys_bytes_to_skip--;

				/* offset of system header stream id */
				if (parser_p->sys_stream_id_offset ==
						PS_SYS_HDR_STREAM_ID_OFFSET)
					psdemux->sys_hdr_stream_id =
						localparams.value;
			} else {
				parser_p->sys_header_size = 0;
				parser_p->sys_stream_id_offset = 0;
				parser_p->state = PES_SYNC_0_00;
			}
			break;

		case PES_SYNC_0_00:
			parser_handle_pes_sync0(parser_p,
					&localparams);
			break;

		case PES_SYNC_1_00:
			parser_handle_pes_sync1(parser_p,
					&localparams);
			break;

		case PES_SYNC_2_01:
			parser_handle_pes_sync2(parser_p,
					&localparams);
			break;

		case PES_SYNC_3_STREAMID:
			parser_handle_pes_sync3(parser_p,
					&localparams);
			break;

		case PES_PACKET_LENGTH_1:
			parser_handle_pes_packetlength1(parser_p,
					&localparams);
			break;

		case PES_PACKET_LENGTH_2:
			parser_handle_pes_packetlength2(parser_p,
					&localparams);

			pes_start_offset = localparams.offset;
			/* this marks start of PES payload after PES header */
			break;
		case PES_PACKET_SKIP_FLAG1:
			parser_skip_and_copy_byte(parser_p,&localparams);
			parser_p->state = PES_PACKET_SKIP_FLAG2;
			break;
		case PES_PACKET_SKIP_FLAG2:
			parser_skip_and_copy_byte(parser_p,&localparams);
			parser_p->state = PES_PACKET_PES_HDR_LENGTH;
			break;
		case PES_PACKET_PES_HDR_LENGTH:
			parser_skip_and_copy_byte(parser_p,&localparams);
			parser_p->pes_header_length = localparams.value;
			parser_p->state = PES_PACKET_SKIP_PES_HDR;
			break;
		case PES_PACKET_SKIP_PES_HDR:
			if (parser_p->pes_header_length!=0 ) {
				parser_skip_and_copy_byte(parser_p,&localparams);
				parser_p->pes_header_length--;
			}
			else
				parser_p->state = PES_PACKET_HANDLE_SUB_STREAM_ID;
			break;
		case PES_PACKET_HANDLE_SUB_STREAM_ID:
			parser_skip_and_copy_byte(parser_p,&localparams);
			if (is_sub_stream_id_valid(psdemux, localparams.value))
				parser_p->state = PES_BUILDING_HDR;
			else
				parser_p->state = PES_SKIP;
			pes_start_offset = localparams.offset;
			break;

		case PES_BUILDING_HDR:
			pr_debug("state %d value 0x%x used size %d\n",
					parser_p->state,
					localparams.value,
					localparams.offset);

			if (is_strem_id_valid(psdemux)) {
				/* Send PES hdr to output buffer */
				/* write PES header of 6 bytes to
				* output buffer */
				pr_debug("state %d PES found with stream id 0x%x\n",
				parser_p->state,
				parser_p->pes_header[3]);

				parser_p->state = PES_BUILDING;
				bytes_w_p = 0;
				ret = te_ps_output_data(psdemux,
					(uint8_t *)parser_p->pes_header,
					(uint32_t)parser_p->pes_header_size,
					0,
					&bytes_w_p);
			} else {
				parser_p->state = PES_SKIP;
			}
			break;

		case PES_BUILDING:
			pr_debug("state %d value 0x%x used size %d pes payload %d bytes\n",
					parser_p->state,
					localparams.value,
					localparams.offset,
					parser_p->pes_remain_len);

			if (localparams.size - localparams.offset >=
					parser_p->pes_remain_len) {
				bytes_w_p = 0;

				/* write data after 6 bytes of PES header */
				ret = te_ps_output_data(psdemux,
						buffer,
						parser_p->pes_remain_len,
						pes_start_offset,
						&bytes_w_p);

				/* write data = pes_remain_len*/
				/* end of PES reached */
				localparams.offset += parser_p->pes_remain_len;

				/* reset the parser here for PES */
				parser_p->state = PES_PH_SYNC_0_00;
				parser_p->pes_remain_len = 0;
				parser_p->current_stream_id = 0;
				parser_p->current_sub_stream_id =
					PES_DEFAULT_SUB_STREAM_ID;
				parser_p->pes_packet_length = 0;
				parser_p->pes_packet_length1 = 0;
				parser_p->pes_packet_length2 = 0;
				parser_p->pes_header_size = 0;
				parser_p->pack_header_size = 0;
				parser_p->sys_header_size = 0;

			} else {
			/* write data = (localparams.size -localparams.offset)*/
			/* write data after 6 bytes of PES header */
				ret = te_ps_output_data(psdemux,
					buffer,
					localparams.size - localparams.offset,
					pes_start_offset,
					&bytes_w_p);

				/* INCOMPLETE Pes write*/
				parser_p->pes_remain_len -=
					(localparams.size - localparams.offset);

				localparams.offset +=
					(localparams.size - localparams.offset);
				/* stay in this state till full PES is
				* written */
			}
			break;

		case PES_SKIP:
			pr_debug("state %d value 0x%x used size %d pes payload %d bytes\n",
					parser_p->state,
					localparams.value,
					localparams.offset,
					parser_p->pes_remain_len);

			/* same as building as no write */
			if (localparams.size - localparams.offset >=
					parser_p->pes_remain_len) {
				localparams.offset += parser_p->pes_remain_len;

				/* end of PES reached */
				/* reset the parser here for PES */
				parser_p->state = PES_PH_SYNC_0_00;
				parser_p->pes_remain_len = 0;
				parser_p->current_stream_id = 0;
				parser_p->current_sub_stream_id =
					PES_DEFAULT_SUB_STREAM_ID;
				parser_p->pes_packet_length = 0;
				parser_p->pes_packet_length1 = 0;
				parser_p->pes_packet_length2 = 0;
				parser_p->pes_header_size = 0;
				parser_p->pack_header_size = 0;
				parser_p->sys_header_size = 0;

			} else {
				parser_p->pes_remain_len -=
					(localparams.size - localparams.offset);
				localparams.offset +=
					(localparams.size - localparams.offset);
				/*stay in this state till full PES is written */
			}
			break;

		default:
			break;
		}
		}
		*injected_count = 1;
		return ret;
}
