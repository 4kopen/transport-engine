/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_psdemux.c

Defines demux object-specific functions
******************************************************************************/
#include "te_object.h"
#include "te_global.h"
#include "te_psdemux.h"
#include "te_internal_cfg.h"
#include "te_interface.h"
#include "te_sysfs.h"
#include "te_ps_internal.h"
#include "te_ps_buffer_work.h"
#include "te_ps_output_filter.h"
#include "te_ps_stream_filter.h"

static int te_psdemux_get_pack_header(struct te_obj *obj,
				stm_te_psdemux_pack_header_t *pack_header);
static int te_psdemux_attach(struct te_obj *obj, stm_object_h target);
static int te_psdemux_detach(struct te_obj *obj, stm_object_h target);

static uint32_t te_psdemux_get_min_free_size(struct te_psdemux *psdemux)
{
	int err = 0;
	unsigned int bufsz = 0, bytes_in_buffer = 0;
	uint32_t free_space = 0, min_free_space = 0xffffffff;
	struct te_stream_filter *input;
	struct te_obj *of = NULL;

	list_for_each_entry(input, &psdemux->stream_filters, obj.lh) {
		of = input->output_filters[0];

		if ((of != NULL) && (of->type == TE_OBJ_PS_PES_FILTER)) {
			err = te_ps_out_filter_get_sizes(of,
					&bufsz, &bytes_in_buffer,
					&free_space);
			if (err != 0) {
				free_space = 0;
			} else {
				pr_debug("obj %p size %d bytes in buffer %d free space = %d\n",
						of, bufsz,
						bytes_in_buffer,
						free_space);
				min_free_space = min_t(
						uint32_t, min_free_space,
						free_space);
			}
		}
	}
	return min_free_space;
}


static bool te_psdemux_is_out_filter_pacing(struct te_psdemux *psdemux)
{
	struct te_stream_filter *input;
	struct te_ps_out_filter *output;
	struct te_obj *of = NULL;
	bool pacing = false;

	list_for_each_entry(input, &psdemux->stream_filters, obj.lh) {
		of = input->output_filters[0];
		output = te_ps_out_filter_from_obj(of);

		if ((of != NULL) && (of->type == TE_OBJ_PS_PES_FILTER) &&
				(output->pacing == true)) {
			pacing = true;
		}
	}
	return pacing;
}

static int throttle_on_sinks(struct te_psdemux *psdemux)
{
	int err = 0;
	unsigned int timeout = 1;

	if (te_psdemux_is_out_filter_pacing(psdemux)) {
		if (psdemux->sink_interface.mode ==
				STM_IOMODE_BLOCKING_IO)
			timeout = MAX_SCHEDULE_TIMEOUT;
		pr_debug("waiting for sink for dp if pacing is set %p\n",
						psdemux);
		err = wait_event_interruptible_timeout(psdemux->pacing_waitq,
			atomic_read(&psdemux->data_process_pending) == 0,
			msecs_to_jiffies(timeout));
		if (err < 0) {
			pr_err("psdmx %p waitq interrupted\n", psdemux);
		} else if (err > 0) {
			pr_debug("No data process pending %p\n", psdemux);
			err = 0;
		} else {
			pr_debug("dp pending timeout %p\n",	psdemux);
			err = -EAGAIN;
		}
	}
	return err;

}
/*
 * PS Demux push data interface
 */
static int te_psdemux_push_connect(stm_object_h src_object,
		stm_object_h sink_object)
{
	int err = 0;
	struct te_obj *psdmx_obj;
	struct te_psdemux *psdmx;
	uint32_t	if_notify_size;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	stm_object_h target_type;
	uint32_t if_size;

	stm_te_trace_in();

	err = te_psdemux_from_hdl(sink_object, &psdmx_obj);
	if (err) {
		pr_err("Bad ps demux handle %p\n", sink_object);
		goto out;
	}
	psdmx = te_psdemux_from_obj(psdmx_obj);

	if (psdmx->upstream_obj) {
		pr_err("PS Demux %s already connected to by %p\n",
				psdmx_obj->name, psdmx->upstream_obj);
		return -EPERM;
	}

	if (mutex_lock_interruptible(&psdmx_obj->lock) != 0) {
		err = -EINTR;
		goto out;
	}

	psdmx->upstream_obj = src_object;

	/*Iomode and notify support : check dmx_obj if it has been
	* updated for iomode from src_obj If yes , check if we have
	* notify function defined with src_obj */

	if (0 == stm_registry_get_attribute(psdmx_obj,
				STM_DATA_INTERFACE_PUSH,
				STM_REGISTRY_ADDRESS,
				sizeof(psdmx->sink_interface),
				&psdmx->sink_interface,
				&if_size)) {

		if (0 !=
		stm_registry_get_object_type(src_object, &target_type)) {
			pr_err("unable to get type of src object %p\n",
				src_object);
			mutex_unlock(&psdmx_obj->lock);
			goto out;
		}
		err = stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PUSH_NOTIFY,
				type_tag,
				sizeof(stm_data_interface_push_notify_t),
				&psdmx->push_notify,
				&if_notify_size);
		if (0 != err)
			pr_warn("no push_notify attribute\n");

	}
	err = te_ps_parser_init(&psdmx->parser_p);
	if (err)
		pr_err("Bad ps parser handle %p\n", psdmx->parser_p);

	mutex_unlock(&psdmx_obj->lock);

out:
	stm_te_trace_out_result(err);
	return err;
}

static int te_psdemux_push_disconnect(stm_object_h src_object,
		stm_object_h sink_object)
{
	struct te_obj *psdmx_obj;
	struct te_psdemux *psdmx;
	int err = 0;

	stm_te_trace_in();

	err = te_psdemux_from_hdl(sink_object, &psdmx_obj);
	if (err) {
		pr_err("Bad PS demux handle %p\n", sink_object);
		return err;
	}
	psdmx = te_psdemux_from_obj(psdmx_obj);

	if (src_object != psdmx->upstream_obj) {
		pr_err("Demux %s not connected to %p (upstream_obj=%p)\n",
			psdmx_obj->name, psdmx->upstream_obj, src_object);
		return -EPERM;
	}

	psdmx->disconnect_pending = true;

	if (mutex_lock_interruptible(&psdmx_obj->lock) != 0) {
		err = -EINTR;
		goto error;
	}

	psdmx->upstream_obj = NULL;
	psdmx->disconnect_pending = false;

	memset(&psdmx->sink_interface, 0, sizeof(psdmx->sink_interface));
	memset(&psdmx->push_notify, 0, sizeof(psdmx->push_notify));

	/* free the parser memory */
	if (psdmx->parser_p) {
		te_ps_parser_reset_params(psdmx->parser_p);
		kfree(psdmx->parser_p);
		psdmx->parser_p = NULL;
	}

	mutex_unlock(&psdmx_obj->lock);

error:
	stm_te_trace_out();
	return err;
}

static int te_psdemux_push_data(stm_object_h psdemux_h,
		struct stm_data_block *block_list, uint32_t block_count,
		uint32_t *data_blocks)
{
	int err = 0;
	uint32_t i = 0;
	struct te_obj *psdmx_obj;
	struct te_psdemux *psdemux;
	uint32_t injected_count = 0;
	uint32_t free_space = 0;
	*data_blocks = 0;

	err = te_psdemux_from_hdl(psdemux_h, &psdmx_obj);
	if (err) {
		pr_err("Bad PS demux handle %p\n", psdemux_h);
		return err;
	}

	if (mutex_lock_interruptible(&psdmx_obj->lock) != 0)
		return -EINTR;

	psdemux = te_psdemux_from_obj(psdmx_obj);

	/* Only allow data push if we have an upstream connection */
	if (!psdemux->upstream_obj) {
		pr_err("PS demux %p not connected to upstream object\n",
				psdemux);
		err = -EPERM;
		goto out;
	}
	while (block_count > 0) {
		err = throttle_on_sinks(psdemux);
		/* EAGAIN indicates waitq timeout */
		if (err == -EAGAIN) {
			err = 0;
			goto out;
		} else if (err)
			goto out;

		free_space = te_psdemux_get_min_free_size(psdemux);
		if (free_space < block_list->len) {
			if (psdemux->sink_interface.mode ==
				STM_IOMODE_BLOCKING_IO){
				msleep(30);
				continue;
			} else{
				pr_debug("No space in buffer dp pending %p\n",
						psdemux);
				err = 0;
				goto out;
			}
		}

		pr_debug("Parse frame i =%d BC= %d block addr %p len %d\n",
			i, block_count , block_list[i].data_addr,
			block_list[i].len);

		err = te_ps_parse_frame(psdemux, block_list[i].data_addr,
			block_list[i].len, &injected_count);
		if (!err)
			*data_blocks += injected_count;
		block_count--;
		i++;
	}

out:
	mutex_unlock(&psdmx_obj->lock);
	return err;
}

stm_data_interface_push_sink_t stm_te_ps_data_push_interface = {
	te_psdemux_push_connect,
	te_psdemux_push_disconnect,
	te_psdemux_push_data,
	KERNEL,
	STM_IOMODE_BLOCKING_IO,
	0, /*aligment not used*/
	0, /* max transfer field not used*/
	0
};

/*!
 * \brief Get global psdemux pack header
 *
 * Get statistics value, using offset from reset point reference.
 *
 * \param obj	demux object
 * \param stat	pointer to stat
 *
 * \retval 0 Success
 * \retval -EINVAL HAL error
 **/
static int te_psdemux_get_pack_header(struct te_obj *obj,
				stm_te_psdemux_pack_header_t *pack_header)
{
	int err = 0;

	struct te_psdemux *psdmx = te_psdemux_from_obj(obj);
	ps_parser_data_t *parser_p = psdmx->parser_p;

	if (parser_p) {
		if (parser_p->pack_header_parsed == 0) {
			pr_err("pack header not found\n");
			return -ENODATA;
		}
		memcpy(pack_header, &parser_p->pack_header,
				sizeof(stm_te_psdemux_pack_header_t));
	}
	return err;
}

/*!
 * \brief return psdemux pointer from an obj
 */
struct te_psdemux *te_psdemux_from_obj(struct te_obj *obj)
{
	if (!obj || obj->type != TE_OBJ_PSDEMUX)
		return NULL;

	return container_of(obj, struct te_psdemux, obj);
}

static void te_psdemux_set_defaults(struct te_psdemux *psdmx_p)
{
	/* Global Parameters */
	/* Internal parameters */
	psdmx_p->program_mux_rate = TE_DEFAULT_PROGRAM_MUX_RATE;
	psdmx_p->sys_hdr_stream_id = TE_DEFAULT_SYS_HDR_STREAM_ID;
}

static int te_psdemux_init(void)
{
	return 0;
}

static int te_psdemux_term(void)
{

	return 0;
}
/*!
 * \brief Deallocates all resources owned by a TE psdemux object
 *
 * \param dmx TE psdemux object whose resource to free
 *
 * Note that if resources are allocated against this psdemux, this function
 * will return 0 (success)
 *
 * \retval 0	Success
 */
static int te_psdemux_obj_stop(struct te_psdemux *psdmx)
{
	int err = 0;

	pr_debug("Deallocating resources for %s\n", psdmx->obj.name);

	/* signal the message queue by sending dummy message*/
	err = te_ps_parser_send_message(-1, psdmx, false);

	if (psdmx->bwq) {
		err = te_ps_bwq_destroy(psdmx->bwq);
		psdmx->bwq = NULL;
	}
	if (err != 0)
		pr_warn("unable to stop buffer work queue\n");

	if (psdmx->work_que)
		destroy_workqueue(psdmx->work_que);

	if (psdmx->sema)
		ps_os_sema_terminate(psdmx->sema);

	list_del(&psdmx->ps_msg_list);

	return err;
}

/*!
 * \brief Allocates the resources associated with a new TE psdemux object
 *
 * \param dmx TE psdemux object to allocate resource for
 *
 * \retval 0 Success
 */
static int te_psdemux_obj_start(struct te_psdemux *psdmx)
{
	int err = 0;

	err = ps_os_sema_initialize(&psdmx->sema , 0);
	if (err) {
		pr_err("failed to initialize semaphore\n");
		return err;
	}

	INIT_LIST_HEAD(&psdmx->ps_msg_list);
	mutex_init(&psdmx->ps_msg_mutex);
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 36))
	{
		char wq_name[16];

		snprintf(wq_name, sizeof(wq_name), "PSDMX-WQ");
		psdmx->work_que = create_workqueue(wq_name);
	}
#else
	psdmx->work_que = alloc_workqueue("PSDMX-WQ", WQ_UNBOUND, 0);
#endif
	if (!psdmx->work_que) {
		pr_err("failed to create workqueue\n");
		err = -ENOMEM;
		goto error;
	}

	err = te_ps_bwq_create(psdmx);
	if (err != 0) {
		pr_err("failed to create buffer work queue\n");
		goto error;
	}
	return 0;

error:
	te_psdemux_obj_stop(psdmx);
	return err;
}

/*!
 * \brief Starts a te psdemux object
 *
 *
 * \param obj TE obj for psdemux to start
 *
 * \retval 0	Success
 */
int te_psdemux_start(struct te_obj *obj)
{
	/* Note: It is not an error to start a started demux */
	if (TE_OBJ_STOPPED == obj->state)
		obj->state = TE_OBJ_STARTED;
	return 0;
}

/*!
 * \brief Stops a te demux object
 *
 * A demux object is "stopped" by setting the "NULL" TSIN
 *
 * \param obj TE obj for demux to start
 *
 * \retval 0 Success
 */
int te_psdemux_stop(struct te_obj *obj)
{
	/* Note: It is not an error to stop a stopped demux */
	if (TE_OBJ_STOPPED != obj->state)
		obj->state = TE_OBJ_STOPPED;
	return 0;
}
/*!
 * \brief Retrieves stm_te control for an stm_te demux object
 *
 * \param obj TE object for psdemux to read the control value from
 * \param control Control to get
 * \param buf	Buffer populated with the read value
 * \param size	Size of buf
 *
 * \retval 0	Success
 * \retval -ENOSYS Control not available
 * \retval -EINVAL Buffer size too small to read data
 */
static int te_psdemux_get_control(struct te_obj *obj, uint32_t control,
		void *buf,
		uint32_t size)
{
	int err = 0;
	struct te_psdemux *psdmx = te_psdemux_from_obj(obj);

	switch (control) {
	case STM_TE_PS_PACK_HDR:
		err = te_psdemux_get_pack_header(obj,
				(stm_te_psdemux_pack_header_t *)buf);
		break;
	case STM_TE_PS_SYSTEM_HDR_STREAM_ID:
		err = GET_CONTROL(psdmx->sys_hdr_stream_id, buf, size);
		break;
	case STM_TE_PS_PROGRAM_MUX_RATE:
		err = GET_CONTROL(psdmx->program_mux_rate, buf, size);
		break;
	default:
		err = -ENOSYS;
		break;
	}
	return err;
}

/*!
 * \brief Stores stm_te control data for an stm_te demux object
 *
 * \param obj	TE object for the demux to set the control value on
 * \param control Control to set
 * \param buf	Buffer containing the control value to set
 * \param size	Size of the control value in buf
 *
 * \retval 0	Success
 * \retval -ENOSYS Control not available
 * \retval -EINVAL Buffer size too small for specified control
 */
static int te_psdemux_set_control(struct te_obj *obj, uint32_t control,
		const void *buf,
		uint32_t size)
{
	int err = 0;
	switch (control) {
	case STM_TE_PS_PACK_HDR:
	case STM_TE_PS_SYSTEM_HDR_STREAM_ID:
	case STM_TE_PS_PROGRAM_MUX_RATE:
	default:
		pr_err("incorrect value for set controlt\n");
		err = -ENOSYS;
		break;
	}
	return err;
}

static int te_psdemux_attach(struct te_obj *obj, stm_object_h target)
{
	return -ENOSYS;
}

static int te_psdemux_detach(struct te_obj *obj, stm_object_h target)
{
	return -ENOSYS;
}

/*!
 * \brief stm_te psdemux object destructor
 *
 * Deletes an stm_te psdemux object and any resource allocated to it
 *
 * \param obj Pointer to the psdemux object to destroy. After this function
 * has returned this pointer should not be dereferenced
 *
 * \retval 0	psdemux successfully deleted
 * \retval -EBUSY psdemux busy - it cannot be deleted
 */
static int te_psdemux_delete(struct te_obj *obj)
{
	int err;
	struct te_psdemux *psdmx = te_psdemux_from_obj(obj);

	/* Return any error here immediately, since it likely means that the
	* psdemux has connections or children and cannot be deleted */
	err = te_obj_deinit(&psdmx->obj);
	if (err)
		return err;

	/* Force the psdemux to stop */
	err = te_psdemux_obj_stop(psdmx);
	if (err)
		pr_warn("Failed to free psdemux resources\n");

	/* Stop the psdemux if this is the last psdemux */
	list_del(&psdmx->obj.lh);
	if (list_empty(&te_global.psdemuxes))
		err = te_psdemux_term();

	kfree(psdmx);

	return err;
}

static struct te_obj_ops te_psdemux_ops = {
	.delete = te_psdemux_delete,
	.set_control = te_psdemux_set_control,
	.get_control = te_psdemux_get_control,
	.attach = te_psdemux_attach,
	.detach = te_psdemux_detach,
};
/*!
 * \brief stm_te psdemux object constructor
 *
 * Creates a new psdemux object. Initialises all the internal data structures in
 * the psdemux object to their default values
 *
 * \param name String name of the new psdemux object
 * \param new_psdemux Returned pointer to the new psdemux object
 *
 * \retval 0		Success
 * \retval -ENOMEM	No memory for new psdemux
 * \retval -ENAMETOOLONG String name too long
 */
int te_psdemux_new(char *name, struct te_obj **new_psdemux)
{
	struct te_psdemux *psdmx;
	int err;
	bool started = false;

	/* Sanity check name. The registry will do this for us later anyway.
	* This check is so that we can comply with our documented return code
	* of -ENAMETOOLONG */
	if (strnlen(name, STM_REGISTRY_MAX_TAG_SIZE) >
			STM_REGISTRY_MAX_TAG_SIZE - 1)
		return -ENAMETOOLONG;

	psdmx = kzalloc(sizeof(struct te_psdemux), GFP_KERNEL);
	if (!psdmx) {
		pr_err("No memory for new psdemux\n");
		return -ENOMEM;
	}

	err = te_obj_init(&psdmx->obj, TE_OBJ_PSDEMUX, name, NULL,
			&te_global.psdemux_class);
	if (err)
		goto err0;
	psdmx->obj.ops = &te_psdemux_ops;

	/* Init demux-specific data */
	INIT_LIST_HEAD(&psdmx->stream_filters);
	INIT_LIST_HEAD(&psdmx->out_filters);
	init_waitqueue_head(&psdmx->pacing_waitq);

	/* Initialise the ps demux default parameters */
	te_psdemux_set_defaults(psdmx);

	/* Start the psdemux if this is the first PS instance */
	if (list_empty(&te_global.psdemuxes)) {
		err = te_psdemux_init();
		if (err)
			goto err1;
		started = true;
	}
	/* Allocate resources for this demux*/
	err = te_psdemux_obj_start(psdmx);
	if (err)
		goto err2;

	list_add_tail(&psdmx->obj.lh, &te_global.psdemuxes);
	rwlock_init(&psdmx->out_flt_rw_lock);
	*new_psdemux = &psdmx->obj;
	return 0;

err2:
	if (started)
		te_psdemux_term();
err1:
	te_obj_deinit(&psdmx->obj);

err0:
	kfree(psdmx);
	return err;
}
int te_psdemux_update_data_process_cnt(struct te_psdemux *psdemux, uint8_t val)
{
	int value, ret = 0;

	if (val == PSDMX_INC_DP_COUNT) {
		atomic_inc(&psdemux->data_process_pending);
		pr_debug("PS demux 0x%p data process count incremented to %d\n",
			psdemux, atomic_read(&psdemux->data_process_pending));
	} else if (val == PSDMX_DEC_DP_COUNT) {
		value = atomic_read(&psdemux->data_process_pending);
		if (value)
			ret = atomic_dec_and_test(
					&psdemux->data_process_pending);
		/* Non zero return value means no data_process is pending.
		* So we go ahead and signal the wait queue */
		pr_debug("PS demux 0x%p data process count decremented to %d\n",
			psdemux, atomic_read(&psdemux->data_process_pending));
		if (ret == true || value == 0) {
			pr_debug("wake up pacing waitq\n");
			wake_up_interruptible(&psdemux->pacing_waitq);
		}
		ret = 0;
	}
	return ret;
}
