/******************************************************************************
Copyright (C) 2011, 2012 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Transport Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_input_filter.c

Defines input filter specific operations
******************************************************************************/

#include <stm_te_dbg.h>
#include <te_object.h>
#include <te_global.h>
#include <te_input_filter.h>
#include <te_output_filter.h>
#include <te_demux.h>
#include <te_sysfs.h>
#include <pti_hal_api.h>

/* Local function prototypes */
static int te_in_filter_detach_path(struct te_in_filter *filter,
		const stm_object_h target);
static int te_in_filter_attach_out_filter(struct te_in_filter *in_filter,
		struct te_obj *out_filter_p);
static int te_in_filter_detach_out_filter(struct te_in_filter *in_filter,
		struct te_obj *out_filter);
static int te_in_filter_get_stat(struct te_in_filter *filter,
			stm_te_input_filter_stats_t *stat);
static int priority_filtering_ctrl_te_to_hal(struct te_in_filter *filter);

/*!
 * \brief Initialises a TE input filter object
 *
 * Initialises all the internal data structures in the input filter object to
 * their default values
 *
 * \param filter Input filter object to initialise
 * \param demux Parent demux for input filter
 * \param name   Name of input filter
 * \param type   Type of input filter
 * \param pid    Initial pid to be captured by the input filter object
 *
 * \note This function allocates and initialises only stm_te-level resources
 * and does not perform any HAL operations or allocate HAL resources
 *
 * \retval 0       Success
 * \retval -EINVAL Bad parameter
 */
int te_in_filter_init(struct te_in_filter *filter, struct te_obj *demux_obj,
		char *name, enum te_obj_type_e type, uint16_t pid)
{
	int err = 0;
	struct te_demux *demux = NULL;
	stm_object_h demux_object, demod_object;

	demux = te_demux_from_obj(demux_obj);
	if (!demux) {
		pr_err("Bad demux handle\n");
		return -EINVAL;
	}

	/* Initialise embedded TE obj */
	err = te_obj_init(&filter->obj, type, name, demux_obj,
			&te_global.in_filter_class);
	if (err)
		goto err1;

	/* Add input filter attributes to registry */
	err = te_sysfs_entries_add(&filter->obj);
	if (err)
		goto err0;

	err = stm_registry_add_attribute(&filter->obj, "pid",
			STM_REGISTRY_UINT32, &filter->pid,
			sizeof(uint32_t));
	if (err) {
		pr_err("Failed to add attr pid to %s (%d)\n", name, err);
		goto err0;
	}

	err = stm_registry_add_attribute(&filter->obj, "path_id",
			STM_REGISTRY_UINT32, &filter->path_id,
			sizeof(filter->path_id));
	if (err) {
		pr_err("Failed to add attr path_id to %s (%d)\n", name, err);
		goto err0;
	}

	err = stm_registry_add_attribute(&filter->obj, "flushing",
			STM_REGISTRY_UINT32, &filter->flushing_behaviour,
			sizeof(filter->flushing_behaviour));
	if (err) {
		pr_err("Failed to add attr flushing to %s (%d)\n", name, err);
		goto err0;
	}

	err = stm_registry_add_attribute(&filter->obj,
			TE_SYSFS_NAME_HAL_HDL "slots",
			TE_SYSFS_TAG_HAL_HDL_LIST,
			&filter->slots,
			sizeof(struct list_head));
	if (err) {
		pr_warn("Failed to add HAL slots attr to obj %s (%d)\n",
				name, err);
		goto err1;
	}

	/* Initialise input filter fields with non-zero defaults to default
	 * values */
	filter->pid = pid;
	filter->ts_priority_bit_filter = stptiHAL_PRIORITY_FILTERING_IGNORE;

	/* if the loopback function exists, use it */
	if (demux->stm_fe_bc_pid_set != NULL) {
		te_hdl_from_obj(demux_obj, &demux_object);
		demod_object = demux->upstream_obj ;
		demux->stm_fe_bc_pid_set(demod_object, demux_object, filter->pid);
	}

	INIT_LIST_HEAD(&filter->slots);

	list_add_tail(&filter->obj.lh, &demux->in_filters);

	return 0;
err0:
	te_obj_deinit(&filter->obj);
err1:
	return err;
}

/*!
 * \brief Uninitializes a TE input filter object
 *
 * \param filter Pointer to the input filter object to destroy
 *
 * \retval 0 Success
 */
int te_in_filter_deinit(struct te_in_filter *filter)
{
	struct te_demux *demux = te_demux_from_obj(filter->obj.parent);
	stm_object_h demux_object, demod_object;
	int err;

	te_sysfs_entries_remove(&filter->obj);

	err = te_obj_deinit(&filter->obj);
	if (err)
		return err;

	/* if the loopback function exists, use it */
	if (demux && demux->stm_fe_bc_pid_clear != NULL) {
		te_hdl_from_obj(&demux->obj, &demux_object);
		demod_object = demux->upstream_obj ;
		demux->stm_fe_bc_pid_clear(demod_object, demux_object, filter->pid);
	}

	list_del(&filter->obj.lh);
	return 0;
}

/*!
 * \brief Sets a control value on a input filter object
 *
 * \param obj     Pointer to the input filter object to set the control on
 * \param control Control to set
 * \param buf     Buffer containing the new control value
 * \param size    Size of the new control value in buf
 *
 * \retval 0       Success
 * \retval -ENOSYS Control not available
 * \retval -EINVAL Incorrect size for this control
 */
int te_in_filter_set_control(struct te_in_filter *filter, uint32_t control,
		const void *buf, uint32_t size)
{
	int err = 0;
	int val = 0;
	stm_te_pid_t old_pid;
	struct te_demux *demux = te_demux_from_obj(filter->obj.parent);
	stm_object_h demux_object, demod_object;

	switch (control) {
	case STM_TE_INPUT_FILTER_CONTROL_PID:
		old_pid = filter->pid;
		err = SET_CONTROL(filter->pid, buf, size);
		if (err == 0)
			err = te_demux_pid_announce_change(filter, old_pid,
					filter->pid);

		/* if the loopback function exists, use it */
		if (demux && demux->stm_fe_bc_pid_set != NULL) {
			te_hdl_from_obj(&demux->obj, &demux_object);
			demod_object = demux->upstream_obj ;
			err = demux->stm_fe_bc_pid_set(demod_object,
					demux_object,
					filter->pid);
		}
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		/* TODO: This control is mis-named, since it also applies to
		 * input filters */
		err = SET_CONTROL(filter->flushing_behaviour, buf, size);
		break;
	case STM_TE_INPUT_FILTER_CONTROL_TSPRIORITY_FILTERING:
		err = SET_CONTROL(val, buf, size);
		if (err == 0) {
                       if (val >= STM_TE_TS_PRIORITY_0_FILTERING &&
                               val <= STM_TE_TS_IGNORE_PRIORITY_FILTERING)
                               filter->ts_priority_bit_filter = val;
                       else
                               pr_err("IF:0x%p Invalid value(%d) for  STM_TE_INPUT_FILTER_CONTROL_TSPRIORITY_FILTERING control\n",
                                               filter, val);
               }
		break;
	default:
		err = -ENOSYS;
		break;
	}

	/* If the new control was valid, update the input filter config */
	if (!err)
		err = te_in_filter_update(filter);

	return err;
}

/*!
 * \brief Reads a control value from a input filter object
 *
 * \param obj     Point to the input filter object to read
 * \param control Control to read
 * \param buf     Buffer populated with the read value
 * \param size    Size of buf
 *
 * \retval 0       Success
 * \retval -ENOSYS Control not available
 * \retval -EINVAL Buffer size too small to read data
 */
int te_in_filter_get_control(struct te_in_filter *filter, uint32_t control,
		void *buf, uint32_t size)
{
	int err = 0;

	switch (control) {
	case STM_TE_INPUT_FILTER_CONTROL_PID:
		err = GET_CONTROL(filter->pid, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		/* TODO: This control is mis-named, since it also applies to
		 * input filters */
		err = GET_CONTROL(filter->flushing_behaviour, buf, size);
		break;
	case __deprecated_STM_TE_FILTER_CONTROL_STATUS:
	case STM_TE_INPUT_FILTER_CONTROL_STATUS:
		err = te_in_filter_get_stat(filter,
					(stm_te_input_filter_stats_t *) buf);
		break;
	case STM_TE_INPUT_FILTER_CONTROL_TSPRIORITY_FILTERING:
		err = GET_CONTROL(filter->ts_priority_bit_filter, buf, size);
		break;
	default:
		err = -ENOSYS;
	}
	return err;
}

/*!
 * \brief Connects a input filter to another STKPI object
 *
 * \param[in] obj    Pointer to TE obj for the input filter to connect
 * \param[in] target Object handle of the object being connected to
 *
 * \retval 0      Success
 * \retval -EPERM Do not know how to connect the specified objects
 **/
int te_in_filter_attach(struct te_in_filter *filter, stm_object_h target)
{
	int err = 0;
	stm_object_h target_type;
	struct te_out_filter *out_filter;
	struct in_filter_list *filters_list;

	/* Try pid -> output filter connection */
	if (0 == te_out_filter_from_hdl(target, &out_filter)) {
		err = te_in_filter_attach_out_filter(filter,
				&out_filter->obj);
		if (0 == err) {
			filters_list = kmalloc(sizeof(struct in_filter_list), GFP_KERNEL);
			if (!filters_list) {
				err = -ENOMEM;
			}
			filters_list->in_filter = filter;
			list_add_tail(&filters_list->lh, &out_filter->in_filters);
		}
		goto out;
	}

	err = stm_registry_get_object_type(target, &target_type);
	if (0 != err) {
		pr_err("unable to get type of target object %p\n", target);
		goto out;
	}

out:
	return err;
}

/*!
 * \brief Disconnects a input filter from another object
 *
 * \param[in] obj    Pointer to TE obj for the input filter to disconnect
 * \param[in] target Object handle of the object being disconnected from
 *
 * Objects must have previously been connected
 */
int te_in_filter_detach(struct te_in_filter *filter, stm_object_h target)
{
	int err = 0;
	struct te_out_filter *out_filter;
	struct in_filter_list *input;

	/* Try pid -> output filter disconnection */
	if (0 == te_out_filter_from_hdl(target, &out_filter)) {
		err = te_in_filter_detach_out_filter(filter,
				&out_filter->obj);
		list_for_each_entry(input, &out_filter->in_filters, lh) {
			if(input->in_filter == filter) {
				list_del(&input->lh);
				kfree(input);
				break;
			}
		}
		goto out;
	}

	if (0 == err )
		return te_in_filter_detach_path(filter, target);

out:
	return err;
}


/*!
 * \brief Disconnects a input filter object from another object, which
 * supplies the interface STM_PATH_ID_INTERFACE
 *
 * \param[in] filter Pointer to input filter object to disconnect
 * \param[in] target Object handle of CE transform to disconnect from
 */
static int te_in_filter_detach_path(struct te_in_filter *filter,
		const stm_object_h target)
{
	int err;

	filter->path_id = 0;

	/* Update any HAL objects owned by this input filter */
	err = te_in_filter_update(filter);
	return err;
}

/*!
 * \brief Attaches a input filter to an output filter
 *
 * \param in_filter  Pointer to input filter to attach
 * \param out_filter TE obj for output filter to attach to
 *
 * \retval 0       Success
 * \retval -EEXIST Filters are already connected
 */
static int te_in_filter_attach_out_filter(struct te_in_filter *in_filter,
		struct te_obj *out_filter)
{
	int err = 0;
	int i;
	char link_name[STM_REGISTRY_MAX_TAG_SIZE];

	snprintf(link_name, STM_REGISTRY_MAX_TAG_SIZE, "in(%p)->output(%p)",
			in_filter, out_filter);

	/* Check if objects are already attached */
	for (i = 0; i < MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] == out_filter) {
			pr_err("IF:%s and OF:%s are already attached\n",
					in_filter->obj.name, out_filter->name);
			return -EEXIST;
		}
	}

	/* Check for free internal connections */
	for (i = 0; i < MAX_INT_CONNECTIONS; i++) {
		if (NULL == in_filter->output_filters[i]) {
			break;
		}
	}
	if (MAX_INT_CONNECTIONS == i) {
		pr_err("IF:%s has too many connections\n", in_filter->obj.name);
		goto error;
	}

	/* Inform registry of connections */
	err = stm_registry_add_connection(&in_filter->obj, link_name,
			out_filter);
	if (err) {
		pr_err("IF:%s Failed to register connection (%d)\n",
				in_filter->obj.name, err);
		goto error;
	}

	/* Create HAL objects resulting from this attachment */
	err = te_out_filter_connect_input(out_filter, &in_filter->obj);
	if (err)
		goto error;

	/* Update filter states and connection */
	in_filter->obj.state = TE_OBJ_STARTED;
	out_filter->state = TE_OBJ_STARTED;
	in_filter->output_filters[i] = out_filter;

	pr_debug("Attached IF:%s <-> OF:%s\n", in_filter->obj.name,
			out_filter->name);

	return 0;

error:
	if (stm_registry_remove_connection(&in_filter->obj, link_name))
		pr_warn("IF:%s Cleanup:Error removing link %s\n",
				in_filter->obj.name, link_name);
	for (i = 0; i < MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] == out_filter)
			in_filter->output_filters[i] = NULL;
	}

	return err;
}

/*!
 * \brief Disconnects a input filter from an output filter
 *
 * \param in_filter  Input filter object to disconnect
 * \param out_filter TE Output filter object to disconnect from
 *
 * \retval 0       Success
 * \retval -EINVAL No connection
 */
static int te_in_filter_detach_out_filter(struct te_in_filter *in_filter,
		struct te_obj *out_filter)
{
	int err = 0;
	int i;
	bool connected = false;
	char link_name[STM_REGISTRY_MAX_TAG_SIZE];

	snprintf(link_name, STM_REGISTRY_MAX_TAG_SIZE, "in(%p)->output(%p)",
			in_filter, out_filter);

	/* All errors in this function are treated as non-fatal: we
	 * print a warning but continue */


	/* Remove internal connections */
	for (i = 0; i < MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] == out_filter) {
			in_filter->output_filters[i] = NULL;
			connected = true;
		}
	}

	if (!connected) {
		pr_err("OF:%s is not connected to IF:%s\n", out_filter->name,
				in_filter->obj.name);
		return -EINVAL;
	}

	/* Unregister connection */
	err = stm_registry_remove_connection(&in_filter->obj, link_name);
	if (err) {
		pr_warn("IF:%s Failed to remove connection %s (%d)\n",
				in_filter->obj.name, link_name,
				err);
	}
	/* If the input filter has no other connections, set it's state to
	 * stopped */
	for (i = 0; i < MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] != NULL)
			break;
	}
	if (MAX_INT_CONNECTIONS == i)
		in_filter->obj.state = TE_OBJ_STOPPED;

	/* Destroy HAL objects */
	err = te_out_filter_disconnect_input(out_filter, &in_filter->obj);
	if (err)
		pr_warn("Failed to disconnect HAL objects (err %d)"
			"filter 0x%p -> out filter 0x%p\n",
			err, &in_filter->obj, out_filter);
	else
		pr_debug("Detached IF:%s >-< OF:%s\n", in_filter->obj.name,
			out_filter->name);

	return err;
}

/*!
 * \brief Updates the HAL objects associated with a started TE input filter
 *
 * Updates the HAL slots associated with a input filter to set
 * 1. The pid
 * 2. Update the flushing behaviour
 *
 * \param filter TE input filter object to update
 *
 * \retval 0    Success
 * \retval -EIO HAL error
 */
int te_in_filter_update(struct te_in_filter *filter)
{
	int err = 0;
	int temp = 0;
	bool suppress_reset = true;
	ST_ErrorCode_t hal_err;

	struct te_hal_obj *slot;

	if (filter->obj.state != TE_OBJ_STARTED)
		return 0;

	if (STM_TE_FILTER_CONTROL_FLUSH_ON_PID_CHANGE
			&filter->flushing_behaviour)
		suppress_reset = false;

	list_for_each_entry(slot, &filter->slots, entry) {
		hal_err = stptiHAL_call(Slot.HAL_SlotSetPID,
				slot->hdl, filter->pid,
				suppress_reset);
		if (ST_NO_ERROR != hal_err) {
			pr_err("HAL_SlotSet_PID for filter %p return 0x%x\n",
					&filter->obj, hal_err);
			err = te_hal_err_to_errno(hal_err);
		}
		hal_err = stptiHAL_call(Slot.HAL_SlotSetSecurePathID,
				slot->hdl, filter->path_id);
		if (ST_NO_ERROR != hal_err) {
			pr_err("HAL_SlotSetSecurePathID return 0x%x\n",
					hal_err);
			err = te_hal_err_to_errno(hal_err);
		}

		/* Configure the slot to filter packets based on requested
		 * priority settings */
		temp = priority_filtering_ctrl_te_to_hal(filter);
		pr_debug("IF:0x%p Set priority filtering mode(%d) on slot:0x%x\n",
				filter, filter->ts_priority_bit_filter,
				slot->hdl.word);
		if (temp >= 0)
			hal_err = stptiHAL_call(Slot.HAL_SlotSetProperty,
					slot->hdl,
					stptiHAL_SLOT_PROPERTY_TSPRIORITY_FILTERING,
					temp);
		else
			err = temp;

		if (ST_NO_ERROR != hal_err) {
			pr_err("Filter %p HAL_SlotSetProperty failed(%d) Ctrl:stptiHAL_SLOT_CONTROL_TSPRIORITY_FILTERING for value:%d\n",
					filter, hal_err, temp);
			err = te_hal_err_to_errno(hal_err);
		}
	}

	return err;
}

/*
 * PID filter interface (CE transform backlink) definition
 */


struct te_in_filter *te_in_filter_from_obj(struct te_obj *filter)
{
	return container_of(filter, struct te_in_filter, obj);
}

bool te_in_filter_has_slot_space(struct te_in_filter *filter)
{
	struct te_hal_obj *slot;
	int count = 0;

	list_for_each_entry(slot, &filter->slots, entry) {
		count ++;
		if (count >= MAX_SLOT_CHAIN) {
			return false;
		}
	}

	return true;
}

/*!
 * \brief Get input filter statistics
 *
 * \param in_filter Input filter object
 * \param stat	pointer to stat
 *
 * \retval 0 Success
 * \retval -EINVAL HAL error
 */
static int te_in_filter_get_stat(struct te_in_filter *filter,
			stm_te_input_filter_stats_t *stat)
{
	ST_ErrorCode_t hal_err;
	int err = 0;
	uint32_t packet_count = 0;
	struct te_hal_obj *slot;
	struct timespec ts;
	ktime_t	ktime;

	stptiHAL_ScrambledState_t scramble_state;

	if (list_empty(&filter->slots)) {
		pr_warn("No valid slots found\n");
		stat->packet_count = 0;
		goto exit;
	}

	/* We will only use the first slot in this input filter */
	slot = list_first_entry(&filter->slots, struct te_hal_obj, entry);

	hal_err = stptiHAL_call(Slot.HAL_SlotGetState, slot->hdl, &packet_count,
				&scramble_state);
	if (ST_NO_ERROR != hal_err) {
		pr_err("could not get slot state slot (0x%x)", hal_err);
		err = hal_err;
		goto exit;
	}

	stat->packet_count = packet_count;
	getrawmonotonic(&ts);
	ktime = timespec_to_ktime(ts);
	stat->system_time = ktime_to_us(ktime);
exit:
	return err;
}



static int priority_filtering_ctrl_te_to_hal(struct te_in_filter *filter)
{
       int ret = 0;

       pr_debug("IF:0x%p Set Priority filtering Mode to %d\n", filter,
                       filter->ts_priority_bit_filter);

       switch (filter->ts_priority_bit_filter) {
       case STM_TE_TS_IGNORE_PRIORITY_FILTERING:
               ret = stptiHAL_PRIORITY_FILTERING_IGNORE;
               break;
       case STM_TE_TS_PRIORITY_0_FILTERING:
               ret = stptiHAL_PRIORITY_FILTERING_EVEN;
               break;
       case STM_TE_TS_PRIORITY_1_FILTERING:
               ret = stptiHAL_PRIORITY_FILTERING_ODD;
               break;
       default:
               pr_err("Filter %p: invalid input(%d) for control STM_TE_INPUT_FILTER_CONTROL_TSPRIORITY_FILTERING\n",
                               filter, filter->ts_priority_bit_filter);
               ret = -EINVAL;
       }


       return ret;
}
