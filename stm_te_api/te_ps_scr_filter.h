/******************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_scr_filter.h

Defines PS SCR filter specific operations
******************************************************************************/

#ifndef __TE_PS_SCR_FILTER_H
#define __TE_PS_SCR_FILTER_H

#define SCR_DATA_LEN (6)

typedef struct {
	uint8_t raw_data[SCR_DATA_LEN];
} te_ps_scr_data_t;

typedef struct {
	uint64_t sys_time;
	uint64_t scr_time;
} te_ps_scr_time_point_t;

int te_ps_scr_filter_new(struct te_obj *psdemux, struct te_obj **new_filter);


#endif

