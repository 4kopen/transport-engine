/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_output_filter.c

Defines output filter specific operations
******************************************************************************/

#include <stm_te_dbg.h>
#include <te_object.h>
#include <te_interface.h>
#include <linux/dma-direction.h>
#include <asm/cacheflush.h>

#include "te_object.h"
#include "te_ps_output_filter.h"
#include "te_ps_stream_filter.h"
#include "te_ps_pes_filter.h"
#include "te_psdemux.h"
#include "te_time.h"
#include "te_global.h"
#include "te_constants.h"
#include <stm_memsink.h>
#include <stm_memsrc.h>
#include "te_sysfs.h"

#define TE_PS_PAUSE 1
#define TE_PS_RESUME 0

static int te_ps_out_filter_attach_to_pull_sink(struct te_obj *filter,
		stm_object_h target);
static int te_ps_out_filter_attach_to_push_sink(struct te_obj *filter,
		stm_object_h target);
static int te_ps_out_filter_detach_from_pull_sink(struct te_obj *filter,
		stm_object_h target);

/*!
 * \brief object to PS output filter cast
 */
struct te_ps_out_filter *te_ps_out_filter_from_obj(struct te_obj *filter)
{
	return container_of(filter, struct te_ps_out_filter, obj);
}
int te_ps_out_filter_from_hdl(stm_object_h hdl,
		struct te_ps_out_filter **output)
{
	int err;
	struct te_obj *object;

	err = te_obj_from_hdl(hdl, &object);
	if (err == 0)
		*output = te_ps_out_filter_from_obj(object);
	else
		*output = NULL;
	return err;
}
static int te_ps_out_filter_detach_from_push_sink(
		struct te_ps_out_filter *output,
		stm_object_h target)
{
	int err = 0;

	struct te_psdemux *demux = te_psdemux_from_obj(output->obj.parent);

	pr_debug("Detaching out filter %p from push sink %p\n", &output->obj,
								target);

	err = stm_registry_remove_connection(&output->obj,
						STM_DATA_INTERFACE_PUSH);
	if (err) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_DATA_INTERFACE_PUSH, err);
	}

	/* Disconnect the push interface before unregistering the WQ
	* This is to ensure the push sink doesn't block any pending push
	* and release it.
	* Inside WQ de-registration we wait for all pending WORK before
	* releasing HAL buffer.
	* */
	err = output->push_intf.disconnect(&output->obj, target);
	if (err)
		pr_err("Failed to disconnect %p from %p\n",
				&output->obj,
				target);

	if (output->buffer_obj) {
		if(te_ps_bwq_unregister(demux->bwq, output->buffer_obj))
			output->buffer_obj = NULL;
	}

	if (err == 0)
		memset(&output->push_intf, 0, sizeof(output->push_intf));
	if (output->pacing == true && atomic_read(&output->dp_set))
		te_psdemux_update_data_process_cnt(demux, PSDMX_DEC_DP_COUNT);

	return err;
}
int te_ps_out_filter_attach(struct te_obj *filter, stm_object_h target)
{
	int err;
	stm_object_h target_type;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;
	struct te_ps_out_filter *output = NULL;

	if (filter)
		output = te_ps_out_filter_from_obj(filter);
	else
		return -EINVAL;

	/* Check if this output filter is already attached to an external
	* object */
	if (output->external_connection) {
		pr_err("Output filter %p already has an attachment\n", filter);
		return -EBUSY;
	}

	err = stm_registry_get_object_type(target, &target_type);
	if (0 != err) {
		pr_err("unable to get type of target object %p\n", target);
		return err;
	}

	/* Look for supported interfaces of the target object.
	* Currently looks for the following interfaces:
	* 1. STM_DATA_INTERFACE_PULL
	* 2. STM_DATA_INTERFACE_PUSH
	*/
	if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PULL, type_tag,
				sizeof(stm_data_interface_pull_sink_t),
				&output->pull_intf,
				&iface_size)) {
		pr_debug("attaching pull sink %p -> %p\n", filter, target);
		err = te_ps_out_filter_attach_to_pull_sink(filter, target);
	} else if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PUSH, type_tag,
				sizeof(stm_data_interface_push_sink_t),
				&output->push_intf,
				&iface_size)) {
		pr_debug("attaching push sink %p -> %p\n", filter, target);
		err = te_ps_out_filter_attach_to_push_sink(filter, target);
	} else {
		pr_err("Attach %p -> %p: no supported interfaces\n", filter,
				target);
		err = -EPERM;
	}

	if (err == 0 && output->pull_intf.connect) {
		stm_data_interface_pull_sink_t tmp_sink;

		if (0 == stm_registry_get_attribute(target,
				STM_DATA_INTERFACE_PULL, type_tag,
				sizeof(stm_data_interface_pull_sink_t),
				&tmp_sink,
				&iface_size))
			output->pull_intf.mode = tmp_sink.mode;
	}

	pr_debug("interfaces: push.connect %p, pull.connect %p\n",
		output->push_intf.connect, output->pull_intf.connect);

	if (!err)
		output->external_connection = target;

	return err;
}

/*!
 * \brief Creates and associates the necessary HAL objects to connect a stream
 * id filter to an PS output filter
 *
 * \param out_flt TE Output filter to attach pid_filt to
 * \param in_flt TE PID filter object to attach
 *
 * \retval 0 Success
 * \retval -EEXIST The objects are already attached
 * \retval -EINVAL The supplied objects cannot be attached
 * \retval -ENOMEM Failed to allocate the required HAL resource
 */
int te_ps_out_filter_connect_input(struct te_obj *out_flt,
		struct te_obj *in_flt)
{
	int result = 0;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(out_flt);
	struct te_psdemux *demux = te_psdemux_from_obj(out_flt->parent);

	pr_debug("Stream Filter 0x%p Output Filter 0x%p\n", in_flt, out_flt);

	if ((in_flt->type == TE_OBJ_PS_STREAM_FILTER)
		&& out_flt->type != TE_OBJ_PS_PES_FILTER) {
		pr_err("stream id filter->PS PES output filter\n");
		return -EINVAL;
	}

	/* Find out the slot type required from the output filter object */
	switch (out_flt->type) {
	case TE_OBJ_PS_PES_FILTER:
		pr_debug("connect to PES\n");
		break;
	case TE_OBJ_PS_SCR_FILTER:
		pr_debug("connect to SCR\n");
		break;
	default:
		pr_err("couldn't find right output filter type\n");
		result = -EINVAL;
		break;
	}
	/* Only create the output filter's buffer handle if required */
	if (output->buffer_obj == NULL) {
		pr_debug("creating output buffer\n");

		/* We're the first, alloc the buffer */
		result = te_buffer_allocator(&output->obj,
						&output->buffer_obj);
		if (result != 0) {
			pr_err("couldn't allocate buffer\n");
			result = -ENOMEM;
			goto error;
		}
		pr_debug("%s: Allocating buffer size %d buffer index 0x%x\n",
				output->obj.name,
				output->buffer_obj->buf_size,
				(unsigned)output->buffer_obj->buf_index);

		if (out_flt->type == TE_OBJ_PS_PES_FILTER) {
			te_buffer_set_threshold(
				output,
				(3 * output->buffer_obj->buf_size) / 4);
				/* 75% threshold */

		} else if (out_flt->type == TE_OBJ_PS_SCR_FILTER) {
			te_buffer_set_threshold(
					output,
					1);
		}
		te_buffer_set_overflow_control(
				output,
				output->overflow_behaviour);

	} else {
		/* Reuse existing buffer, but increment refcount */
		pr_debug("reusing output buffer\n");
		te_buffer_obj_inc(output->buffer_obj);
	}

	/* Register buffer workqueue function */
	if (output->external_connection != NULL) {
		result = te_ps_bwq_register(demux->bwq, out_flt,
				output->buffer_obj,
				output->buffer_work_func);
		if (result != 0) {
			pr_err("unable to register buffer work func\n");
			goto error;
		}
	}

	return result;

error:
	if (output->buffer_obj) {
		if (te_buffer_obj_dec(output->buffer_obj))
			output->buffer_obj = NULL;
	}
	return result;
}

int te_ps_out_filter_detach(struct te_obj *filter, stm_object_h target)
{
	int err;
	stm_object_h target_type;
	stm_object_h connected_obj = NULL;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;
	struct te_ps_out_filter *output = NULL;

	if (filter)
		output = te_ps_out_filter_from_obj(filter);
	else
		return -EINVAL;

	if (output->external_connection != target) {
		pr_err("Output filter %p not attached to %p\n", filter, target);
		return -EINVAL;
	}

	/* Check for pull or push data connection */
	err = stm_registry_get_object_type(target, &target_type);
	if (0 != err) {
		pr_err("unable to get type of target object %p\n", target);
		return err;
	}

	/* Look for supported interfaces of the target object.
	* Currently looks for the following interfaces:
	* 1. STM_DATA_INTERFACE_PULL
	* 2. STM_DATA_INTERFACE_PUSH
	*/
	if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PULL, type_tag,
				sizeof(stm_data_interface_pull_sink_t),
				&output->pull_intf,
				&iface_size)) {
		pr_debug("detaching from pull sink %p\n", target);
		err = stm_registry_get_connection(te_obj_to_hdl(filter),
				STM_DATA_INTERFACE_PULL, &connected_obj);
		if (0 == err && connected_obj == target)
			err = te_ps_out_filter_detach_from_pull_sink(filter,
					target);
	} else if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PUSH, type_tag,
				sizeof(stm_data_interface_push_sink_t),
				&output->push_intf,
				&iface_size)) {
		pr_debug("attached to push sink %p\n", target);
		err = stm_registry_get_connection(te_obj_to_hdl(filter),
				STM_DATA_INTERFACE_PUSH, &connected_obj);
		if (0 == err && connected_obj == target)
			err = te_ps_out_filter_detach_from_push_sink(output,
					target);
	} else {
		pr_err("Detach %p -> %p: no supported interfaces\n", filter,
				target);
		err = -EPERM;
	}

	if (!err) {
		/* Ensure output filter external connection is unset */
		output->external_connection = NULL;
	}

	return err;
}

int te_ps_out_filter_deinit(struct te_ps_out_filter *filter)
{
	int err;
	struct te_psdemux *psdemux = te_psdemux_from_obj(filter->obj.parent);
	u_long flags = 0;

	err = te_sysfs_entries_remove(&filter->obj);
	if (err)
		pr_warn("Failed to remove sysfs entries\n");

	err = te_obj_deinit(&filter->obj);
	if (err)
		return err;

	write_lock_irqsave(&psdemux->out_flt_rw_lock, flags);
	list_del(&filter->obj.lh);
	write_unlock_irqrestore(&psdemux->out_flt_rw_lock, flags);

	return 0;
}

static int te_ps_out_filter_flush(struct te_ps_out_filter *filter)
{
	int err = 0;
	struct te_psdemux *psdemux = te_psdemux_from_obj(filter->obj.parent);

	struct te_buffer_obj *buffer_obj;

	/* Grab the object's lock to prevent other threads updating output
	* rd/wr offset */
	err = mutex_lock_interruptible(&filter->obj.lock);
	if (err < 0)
		return err;

	/* Flush the buffer */
	if (filter->buffer_obj) {
		buffer_obj = filter->buffer_obj;

		te_buffer_obj_inc(buffer_obj);

		/* reset the write/read offsets */
		buffer_obj->wr_offs = buffer_obj->rd_offs = 0;
		buffer_obj->dma_overflow_flag = 0;

		te_buffer_obj_dec(buffer_obj);
	}

	if (filter->pacing && atomic_read(&filter->dp_set))
		te_psdemux_update_data_process_cnt(psdemux, PSDMX_DEC_DP_COUNT);

	mutex_unlock(&filter->obj.lock);
	return err;
}

int te_ps_out_filter_get_sizes(struct te_obj *obj, unsigned int *total_size,
				unsigned int *bytes_in_buffer,
				unsigned int *free_space)
{
	int err;
	struct te_ps_out_filter *filter = te_ps_out_filter_from_obj(obj);
	struct te_buffer_obj *buffer_obj;

	err = mutex_lock_interruptible(&te_global.lock);
	if (err < 0)
		return err;

	if (filter == NULL || filter->buffer_obj == NULL) {
		pr_warn("Pacing filter %s is not started\n", obj->name);
		goto err_unlk;
	}
	if (!filter->external_connection) {
		pr_warn("Pacing filter %s has no downstream connections\n",
				obj->name);
		goto err_unlk;
	}

	te_buffer_obj_inc(filter->buffer_obj);

	buffer_obj = filter->buffer_obj;

	mutex_unlock(&te_global.lock);

	err = te_buffer_status(filter, bytes_in_buffer,
				free_space , NULL);

	if (filter->buffer_obj)
		*total_size = filter->buffer_obj->buf_size;

	te_buffer_obj_dec(filter->buffer_obj);

	if (0 != err) {
		pr_err("te_buffer_status return 0x%x\n", err);
		return -EINVAL;
	}

	return 0;

err_unlk:
	mutex_unlock(&te_global.lock);
	return -EINVAL;
}

/*!
 * \brief stm_te ps output filter object constructor
 *
 * Initialises a new output filter.r default values
 *
 * \param filter pointer to the ps output filter to initialise
 * \param demux pointer to the TE obj of the parent demux for this ps output
 * filter
 * \param name String name for this ps output filter
 * \param type Type of this ps output filter
 *
 * \retval 0 Success
 * \retval -ENOMEM No memory for new object
 */
int te_ps_out_filter_init(struct te_ps_out_filter *filter,
		struct te_obj *demux_obj,
		char *name, enum te_obj_type_e type)
{
	int err = 0;
	struct te_psdemux *demux;
	u_long flags = 0;

	demux = te_psdemux_from_obj(demux_obj);
	if (!demux) {
		pr_err("Bad demux handle\n");
		return -EINVAL;
	}

	err = te_obj_init(&filter->obj, type, name, demux_obj,
			&te_global.out_filter_class);
	if (err)
		goto error;

	/* Add output filter registry attributes */
	err = te_sysfs_entries_add(&filter->obj);
	if (err) {
		pr_err("Failed to add sysfs entries for filter %p %s\n", filter,
			name);
		err = -ENOMEM;
		goto error;
	}

	/*
	* Initialise output filter fields with non-zero defaults to default
	* values
	*/
	filter->flushing_behaviour = TE_DEFAULT_OUTPUT_FILTER_FLUSHING;
	filter->overflow_behaviour = TE_DEFAULT_OUTPUT_FILTER_OVERFLOW;
	filter->buf_size = 0;

	/* Set output filter pull src interface */
	filter->pull_src_intf = &stm_te_ps_pull_byte_interface;

	/* Set default read_bytes and next_read_size functions
	* Sub-classes of output filter may override these */
	filter->read_bytes = &te_ps_out_filter_read_bytes;
	filter->next_read_size = &te_ps_out_filter_next_read_size;

	if (type == TE_OBJ_PS_PES_FILTER)
		filter->pacing = true;

	/* Set default buffer work function */
	filter->buffer_work_func = &te_ps_out_filter_work;

	init_waitqueue_head(&filter->reader_waitq);

	write_lock_irqsave(&demux->out_flt_rw_lock, flags);
	list_add_tail(&filter->obj.lh, &demux->out_filters);
	write_unlock_irqrestore(&demux->out_flt_rw_lock, flags);

	/* Set default object functions */
	filter->flush = &te_ps_out_filter_flush;

	return 0;

error:
	te_obj_deinit(&filter->obj);
	return err;
}

/*!
 * \brief Destroys the HAL objects created when the given pid and output
 * filters were attached
 *
 * \param out_flt TE Output filter to detach pid_filter_p from
 * \param in_flt TE PID filter object to detach
 *
 * \retval 0 Success
 * \retval -EINVAL Detachment of the specified filters is not supported
 */
int te_ps_out_filter_disconnect_input(struct te_obj *out_flt,
		struct te_obj *in_flt)
{
	int err = 0;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(out_flt);

	/* validate functions arguments */
	if (in_flt == NULL || out_flt == NULL) {
		pr_err("input or output filter is NULL\n");
		err = -EINVAL;
		goto done;
	} else if (in_flt->type != TE_OBJ_PS_STREAM_FILTER) {
		pr_err("input is not a stream filter\n");
		err = -EINVAL;
		goto done;
	}
	/* Check to see if there are any HAL objects that need disassociating.
	* Where there is a 1:1 mapping between hal objects
	* (slots with buffers filters & signals etc)
	* the HAL objects can be deallocated after disassociating.
	* However if more than one association exists then we need to check
	* it is safe to deallocate the HAL objects */

	switch (out_flt->type) {
	case TE_OBJ_PS_PES_FILTER:
	case TE_OBJ_PS_SCR_FILTER:
			if (te_buffer_obj_dec(output->buffer_obj)) {
				output->buffer_obj = NULL;
				output->obj.state = TE_OBJ_STOPPED;
			} else {
			/* Buffer still exists, check + flush */
				if (output->flushing_behaviour
				== STM_TE_FILTER_CONTROL_FLUSH_ON_DETACH) {
					output->flush(output);
				}
			}
		break;
	default:
		pr_err("unsupported output filter type %d\n", out_flt->type);
		err = -EINVAL;
		goto done;
	}

done:
	return err;
}

static int te_ps_out_filter_pull_testfordata(stm_object_h filter_h,
		uint32_t *size)
{
	int err = 0;
	struct te_ps_out_filter *output;
	int32_t sz;

	*size = 0;

	err = te_ps_out_filter_from_hdl(filter_h, &output);
	if (err) {
		pr_err("Bad filter handle %p\n", filter_h);
		return err;
	}

	/* output->next_read_size can return a negative errno if interrupted by
	* a signal*/
	sz = output->next_read_size(output);
	if (sz >= 0)
		*size = sz;
	else
		err = sz;

	return err;
}

static int te_ps_out_filter_pull_data(stm_object_h filter_h,
		struct stm_data_block *data_block,
		uint32_t block_count,
		uint32_t *filled_blocks)
{
	int err = 0;
	struct te_ps_out_filter *output;
	int32_t total_read = 0;
	int32_t available_size = 0;
	int32_t read_size = 0;
	int32_t req_size = 0;
	int32_t min_read_size;
	int i;
	struct te_psdemux *psdemux;
	*filled_blocks = 0;

	/* Check handle */
	err = te_ps_out_filter_from_hdl(filter_h, &output);
	if (err) {
		pr_err("Bad filter handle %p\n", filter_h);
		return err;
	}

	psdemux = te_psdemux_from_obj(output->obj.parent);
	if (psdemux == NULL)
		return -ENODEV;

	if (output->buffer_obj == NULL) {
		pr_debug("output filter buffer not valid\n");
		return -ENODEV;
	}

	/* Determine total requested size */
	for (i = 0; i < block_count; i++)
		req_size += data_block[i].len;

	/* Check attachment status */
	if (!output->external_connection) {
		pr_warn("Pull request on detached filter %s\n",
				output->obj.name);
		return -EPERM;
	}

	/* If the output filter is in quantisation unit reading mode we will
	* return at most one quantisation unit, so regardless of req_size, the
	* minimum read size is 1 byte.
	* In byte-stream mode the minimum read size is the size of the buffer.
	* This means that blocking pull requests will block until the whole
	* buffer is filled. */
	if (output->read_quantisation_units)
		min_read_size = 1;
	else
		min_read_size = req_size;

	/* If output->next_read_size returns an error, it has been interrupted
	* and we should return now */
	available_size = output->next_read_size(output);
	if (available_size < 0)
		return available_size;

	/* If we cannot satisfy the pull_request now force pull_work to happen
	* now, so we have an up-to-date view of the output filter's buffer */
	if (available_size < min_read_size && output->buffer_obj->buf_start) {
		te_buffer_obj_inc(output->buffer_obj);
		output->buffer_work_func(&output->obj, output->buffer_obj);
		te_buffer_obj_dec(output->buffer_obj);
		available_size = output->next_read_size(output);
		if (available_size < 0)
			return available_size;
	}

	/* If this output filter has enough data to satisty the pull request or
	* we are in non-blocking mode populate the buffer
	*
	* Otherwise this task must wait until enough data becomes available
	* (or the output filter is disconnected)
	*/
	while (available_size < min_read_size &&
		!(output->pull_intf.mode & STM_IOMODE_NON_BLOCKING_IO))	{
		int wait_err;

		pr_debug("Requested %d (minimum=%d), %d available. Blocking\n",
				req_size, min_read_size, available_size);
		wait_err = wait_event_interruptible(output->reader_waitq,
				output->next_read_size(output) >= min_read_size
				|| output->external_connection == NULL);
		if (wait_err) {
			pr_warn("Pull request on %s interrupted\n",
					output->obj.name);
			return wait_err;
		}

		/* Check attachment status */
		if (!output->external_connection) {
			pr_warn("%s disconnected. Aborting pull request\n",
					output->obj.name);
			return -EPERM;
		}

		available_size = output->next_read_size(output);
		if (available_size < 0)
			return available_size;
	}

	read_size = min_t(uint32_t, available_size, req_size);

	pr_debug("Reading %d bytes (%d requested, %d available)\n", read_size,
			req_size, available_size);

	while (!err && block_count > 0 && read_size > 0) {
		uint32_t bytes_read = 0;

		err = output->read_bytes(output, data_block->data_addr,
				min_t(uint32_t, data_block->len, read_size),
				&bytes_read);
		if (err || 0 == bytes_read)
			break;

		data_block->len = bytes_read;
		read_size -= bytes_read;
		(*filled_blocks)++;
		total_read += bytes_read;
		block_count--;
		data_block++;
	}
	if (output->pacing == true) {
		if (atomic_read(&output->dp_set)) {
			te_psdemux_update_data_process_cnt(psdemux,
					PSDMX_DEC_DP_COUNT);
			atomic_dec(&output->dp_set);
		}
		pr_debug("Output filter %p is pacing\n", &output->obj);
	}
	if (psdemux->push_notify.notify) {
		err = psdemux->push_notify.notify(
				psdemux->upstream_obj,
				STM_MEMSRC_EVENT_CONTINUE_INJECTION);
		if (unlikely(err)) {
			pr_err_ratelimited("Notify src %p failed(%d)\n",
					psdemux->upstream_obj,
					err);
		}
	}

	return total_read;
}
stm_data_interface_pull_src_t stm_te_ps_pull_byte_interface = {
	te_ps_out_filter_pull_data,
	te_ps_out_filter_pull_testfordata,
};

int te_ps_out_filter_memcpy(void **dest_pp, const void *src_p,
		size_t size)
{
	/* Note: dest_pp is not a void *, but a void ** */
	memcpy(*dest_pp, src_p, size);
	*dest_pp = (void *)((uint8_t *)(*dest_pp) + size);
	return 0;
}

/*!
 * \brief Copies data from an output filter into a kernel buffer
 *
 * Reads data directly from the output filter's HAL buffer, ignoring data
 * quantisation
 *
 * \param output	Output filter to read from
 * \param buf		Kernel virtual buffer to copy data into
 * \param buf_size	Size of buffer pointed to by buf
 * \param bytes_read	Pointer to uint32_t set to the actual number of bytes
 *			copied by this function
 *
 * \retval 0		Success
 * \retval -EINVAL HAL error occurrred
 */
int te_ps_out_filter_read_bytes(struct te_ps_out_filter *output, uint8_t *buf,
		uint32_t buf_size, uint32_t *bytes_read)
{
	uint32_t read_offs = PS_CURRENT_READ_OFFSET;
	int err;

	*bytes_read = 0;

	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return err;

	err = te_buffer_read_bytes(output,
			&read_offs,
			buf,
			buf_size,
			&te_ps_out_filter_memcpy,
			bytes_read);

	/* Update read pointer and resignal if necessary (intentionally
	* ignore returned error) */
	if (err == 0)
		te_buffer_set_read_offset(output, read_offs);

	pr_debug("Updated read offset %d\n",
		output->buffer_obj->rd_offs);

	/* read function here */
	mutex_unlock(&output->obj.lock);
	return err;
}

/*!
 * \brief Determines how much data will be read by the next read call on this
 * output filter using the current HAL buffer read/write offsets
 *
 * \param output Output filter to test
 *
 * \return Size of data available to read in the next read call
 */
int32_t te_ps_out_filter_next_read_size(struct te_ps_out_filter *output)
{
	/* Currently we ignore any quantisation units so the amount of data
	* that can be read is equal to the current amount data in the buffer
	*/
	int err;
	int32_t size = 0;
	struct te_buffer_obj *buffer_obj;

	if (output == NULL || output->buffer_obj == NULL) {
		pr_debug("output filter not valid\n");
		err = -ENOSYS;
		return err;
	}

	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return err;

	buffer_obj = output->buffer_obj;

	/* if buffer overflow = false & W> R */
	if (buffer_obj->wr_offs >= buffer_obj->rd_offs) {
		size = buffer_obj->wr_offs -
				buffer_obj->rd_offs;
	} else {
		/* if buffer overflow = false & R > W */
		size = buffer_obj->buf_size -
				buffer_obj->rd_offs;
		size += buffer_obj->wr_offs;
	}

	/* find out read size */
	mutex_unlock(&output->obj.lock);

	pr_debug("size=%u\n", size);
	return size;
}

/*!
 * \brief Gets the current output filter statistics information
 *
 * \param obj Filter to check
 * \param stat Pointer to stat
 *
 * \retval 0 Success
 * \retval -EIO HAL error
 */
static int te_ps_out_filter_get_stat(struct te_obj *obj,
				stm_te_output_filter_stats_t *stat)
{
	int err = 0;
	bool buffer_overflows = false;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(obj);

	memset(stat, 0, sizeof(*stat));

	if (obj->state == TE_OBJ_STARTED && (output->buffer_obj != NULL)) {
		stat->crc_errors = 0;

		err = te_buffer_status(output,
				&stat->bytes_in_buffer, NULL,
				&buffer_overflows);

		if (0 != err) {
			pr_err("te_buffer_status return 0x%x\n", err);
			goto exit;
		}
		stat->buffer_overflows = (uint32_t)buffer_overflows;
	}
exit:

	return err;
}

/*!
 * \brief Gets stm_te control data for an stm_te output filter object
 *
 * \param filter output filter object to interrogate
 * \param control Control to get
 * \param value Set with the current value of the control
 *
 * \retval 0	Success
 * \retval -ENOSYS Invalid control coptrol for this output filter
 */
int te_ps_out_filter_get_control(struct te_obj *filter, uint32_t control,
				void *buf, uint32_t size)
{
	int err = 0;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(filter);

	switch (control) {
	case STM_TE_OUTPUT_FILTER_CONTROL_BUFFER_SIZE:
		if (output->buffer_obj)
			err = GET_CONTROL(output->buffer_obj->buf_size, buf,
					size);
		else
			err = GET_CONTROL(output->buf_size, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_OVERFLOW_BEHAVIOUR:
		err = GET_CONTROL(output->overflow_behaviour, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_ERROR_RECOVERY:
		err = GET_CONTROL(output->error_recovery, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		err = GET_CONTROL(output->flushing_behaviour, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS:
		err = GET_CONTROL(output->read_quantisation_units, buf, size);
		break;
	case __deprecated_STM_TE_FILTER_CONTROL_STATUS:
	case STM_TE_OUTPUT_FILTER_CONTROL_STATUS:
		err = te_ps_out_filter_get_stat(filter,
					(stm_te_output_filter_stats_t *) buf);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_PACING:
		err = GET_CONTROL(output->pacing, buf, size);
		break;
	default:
		err = -ENOSYS;
	}

	return err;
}

/*!
 * \brief Sets stm_te control data for an stm_te ps output filter object
 *
 * \param filter PS output filter object to update
 * \param control Control to set
 * \param value New value of the control
 *
 * \retval 0	Success
 * \retval -ENOSYS Invalid control control for this output filter
 */
int te_ps_out_filter_set_control(struct te_obj *filter, uint32_t control,
				const void *buf, uint32_t size)
{
	int err = 0;
	unsigned int val;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(filter);

	switch (control) {
	case STM_TE_OUTPUT_FILTER_CONTROL_BUFFER_SIZE:
		if (filter->state == TE_OBJ_STARTED) {
			pr_err("Unable to change buffer size\n");
			err = -EINVAL;
		} else if (buf && (*((int *)buf) < DVB_PAYLOAD_SIZE)) {
			pr_err("Buffer size atleast be equal to 184 bytes\n");
			err = -EINVAL;
		} else
			if (output->buffer_obj)
				err = SET_CONTROL(output->buffer_obj->buf_size,
						buf, size);
			else
				err = SET_CONTROL(output->buf_size, buf, size);
		break;

	case STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS:
		err = SET_CONTROL(val, buf, size);
		if (!err)
			output->read_quantisation_units = (val != 0);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_OVERFLOW_BEHAVIOUR:
		err = SET_CONTROL(output->overflow_behaviour, buf, size);
		if (err == 0)
			err = te_buffer_set_overflow_control(output,
						output->overflow_behaviour);
		break;

	case STM_TE_OUTPUT_FILTER_CONTROL_ERROR_RECOVERY:
		/* Setting these values is currently unsupported */
		err = -ENOSYS;
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		err = SET_CONTROL(output->flushing_behaviour, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSH:
		err = output->flush(output);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_PAUSE:
		if (*(int *)buf == TE_PS_PAUSE && filter->state ==
						TE_OBJ_STARTED) {
			filter->state = TE_OBJ_PAUSED;
		} else if (*(int *)buf == TE_PS_RESUME && filter->state ==
						TE_OBJ_PAUSED) {
			filter->state = TE_OBJ_STARTED;
		} else {
			pr_err("Invalid set control request buf val %d %d\n",
					*(int *)buf,
					filter->state);
			err = -EINVAL;
		}
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_PACING:
		err = SET_CONTROL(val, buf, size);
		if (!err)
			output->pacing = (val != 0);
		break;
	default:
		err = -ENOSYS;
	}

	return err;
}

static void te_ps_out_filter_pull_work(struct te_ps_out_filter *output,
		struct te_buffer_obj *buf_obj)
{
	uint32_t wr_offs = 0;
	int err = 0;
	struct te_buffer_obj *buffer_obj;
	struct te_psdemux *psdemux = te_psdemux_from_obj(output->obj.parent);


	/* Update buffer write offset */
	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return;

	if (output->buffer_obj == NULL) {
		mutex_unlock(&output->obj.lock);
		pr_debug("output filter buffer not valid\n");
		return;
	}
	buffer_obj = output->buffer_obj;

	/* get the write offset */
	wr_offs = buffer_obj->wr_offs;

	/* Write offset has not changed. Do nothing */
	if (buffer_obj->wr_offs == buffer_obj->rd_offs) {
		mutex_unlock(&output->obj.lock);
		return;
	}

	pr_debug("rd_offs %d wr_offs %d\n", buffer_obj->rd_offs,
			buffer_obj->wr_offs);

	if (output->pacing && !atomic_read(&output->dp_set)) {
		te_psdemux_update_data_process_cnt(psdemux, PSDMX_INC_DP_COUNT);
		atomic_inc(&output->dp_set);
	}

	/* New data is available
	* 1. Notify pull sink
	* 2. Wake up any sleeping reader thread
	*/
	if (output->pull_intf.notify) {
		err = output->pull_intf.notify(output->external_connection,
				STM_MEMSINK_EVENT_DATA_AVAILABLE);
		if (unlikely(err))
			pr_err_ratelimited("Notify sink %p failed (%d)\n",
					output->external_connection, err);
	}
	mutex_unlock(&output->obj.lock);

	/* Wake up any waiting read thread */
	wake_up_interruptible(&output->reader_waitq);
}

static int te_ps_out_filter_attach_to_pull_sink(struct te_obj *filter,
		stm_object_h target)
{
	int err;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(filter);
	struct te_psdemux *demux = te_psdemux_from_obj(filter->parent);

	pr_debug("Attaching out filter obj %p(%p) to pull sink %p\n", filter,
			output,
			target);
	err = output->pull_intf.connect(filter, target, output->pull_src_intf);
	if (err) {
		pr_err("Failed to connect filter %p to pull sink %p (%d)\n",
				filter, target, err);
		goto error;
	}

	if (output->buffer_obj) {
		pr_debug("registering buffer index 0x%08x on sink connect %p\n",
				output->buffer_obj->buf_index,
				output->buffer_work_func);

		err = te_ps_bwq_register(demux->bwq, &output->obj,
				output->buffer_obj, output->buffer_work_func);
		if (err != -EEXIST && err != 0) {
			pr_err("unable to register buffer\n");
			goto error;
		}
	}
	err = stm_registry_add_connection(filter, STM_DATA_INTERFACE_PULL,
			target);
	if (err) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_DATA_INTERFACE_PULL, err);
		if (output->pull_intf.disconnect(filter, target))
			pr_warn("Cleanup: disconnect failed\n");
	}
	return 0;
error:
	if (output->buffer_obj)
		if (te_ps_bwq_unregister(demux->bwq, output->buffer_obj))
			output->buffer_obj = NULL;
	return err;
}

static int te_ps_out_filter_detach_from_pull_sink(struct te_obj *filter,
		stm_object_h target)
{
	int err;
	int result = 0;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(filter);
	struct te_psdemux *demux = te_psdemux_from_obj(filter->parent);

	pr_debug("Detaching out filter %p from pull sink %p\n", filter,
			target);

	err = stm_registry_remove_connection(filter, STM_DATA_INTERFACE_PULL);
	if (err) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_DATA_INTERFACE_PULL, err);
		result = err;
	}

	/* Check if the output filter is of type SECTION.
	* Section filters are special as they share the same HAL buffer.
	* For Section filters Work Q unregistration is only done when the
	* section to be deleted is the last user on the same slot.
	* This decision is taken during deattach of Input filter with the
	* section filter.
	* */
	if (filter->type != TE_OBJ_SECTION_FILTER)
		if (output->buffer_obj) {
			err = te_ps_bwq_unregister(demux->bwq,
					output->buffer_obj);
			if (err)
				output->buffer_obj = NULL;
		}

	/* Wake up any thread currently blocked in pull_data */
	err = mutex_lock_interruptible(&filter->lock);
	if (err >= 0) {
		output->external_connection = NULL;
		mutex_unlock(&filter->lock);
		wake_up_interruptible(&output->reader_waitq);
	}

	err = output->pull_intf.disconnect(filter, target);
	if (err) {
		pr_err("Failed to disconnect %p from pull sink %p (%d)\n",
				filter, target, err);
		result = err;
	} else {
		memset(&output->pull_intf, 0, sizeof(output->pull_intf));
	}
	if (output->pacing == true && atomic_read(&output->dp_set))
		te_psdemux_update_data_process_cnt(demux, PSDMX_DEC_DP_COUNT);

	return result;
}

static void te_ps_out_filter_push_work(struct te_ps_out_filter *output,
		struct te_buffer_obj *buf)
{
	struct stm_data_block block[2];
	int read;
	int blocks = 0;
	int err = 0;
	struct te_psdemux *psdemux = te_psdemux_from_obj(output->obj.parent);
	struct te_buffer_obj *buffer_obj;

	if (output->buffer_obj == NULL) {
		pr_debug("output filter buffer not valid\n");
		return;
	}

	if (mutex_lock_interruptible(&output->obj.lock) != 0)
		return;

	buffer_obj = output->buffer_obj;

	/* get the write offset and compare with read */
	if (buffer_obj->rd_offs == buffer_obj->wr_offs)
		goto done; /* nothing to do */

	pr_debug("rd_offs %d wr_offs %d\n", buffer_obj->rd_offs,
			buffer_obj->wr_offs);

	if (output->pacing && !atomic_read(&output->dp_set)) {
		te_psdemux_update_data_process_cnt(psdemux,
							PSDMX_INC_DP_COUNT);
		atomic_inc(&output->dp_set);
	}

	if (buffer_obj->rd_offs < buffer_obj->wr_offs) {
		block[0].data_addr = buffer_obj->buf_start +
					buffer_obj->rd_offs;
		block[0].len = buffer_obj->wr_offs -
					buffer_obj->rd_offs;
		blocks = 1;
	} else {
		block[0].data_addr = buffer_obj->buf_start +
					buffer_obj->rd_offs;
		block[0].len = buffer_obj->buf_size -
					buffer_obj->rd_offs;
		blocks = 1;

		if (buffer_obj->wr_offs != 0) {
			block[1].data_addr = buffer_obj->buf_start;
			block[1].len = buffer_obj->wr_offs;
			blocks = 2;
		}
	}
	err = output->push_intf.push_data(output->external_connection, block,
					blocks, &read);
	if (err)
		pr_warn_ratelimited("Push_data failed %d\n", err);

	/* set the read offset equal to write */
	te_buffer_set_read_offset(output, buffer_obj->wr_offs);

	if (output->pacing == true) {
		if (atomic_read(&output->dp_set)) {
			te_psdemux_update_data_process_cnt(psdemux,
					PSDMX_DEC_DP_COUNT);
			atomic_dec(&output->dp_set);
		}
	}
	if (psdemux->upstream_obj && psdemux->push_notify.notify) {
		err = psdemux->push_notify.notify(
				psdemux->upstream_obj,
				STM_MEMSRC_EVENT_CONTINUE_INJECTION);
		if (unlikely(err)) {
			pr_err_ratelimited("Notify src %p failed(%d)\n",
				psdemux->upstream_obj,
				err);
		}
	}

done:
	mutex_unlock(&output->obj.lock);
}
void te_ps_out_filter_work(struct te_obj *obj, struct te_buffer_obj *buffer_obj)
{
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(obj);

	if (obj->state == TE_OBJ_PAUSED) {
		pr_debug("%s(): Buffer %s paused\n", __func__, obj->name);
		return;
	}
	if (output->pull_intf.connect)
		te_ps_out_filter_pull_work(output, buffer_obj);

	if (output->push_intf.connect)
		te_ps_out_filter_push_work(output, buffer_obj);
}

static int te_ps_out_filter_attach_to_push_sink(struct te_obj *filter,
		stm_object_h target)
{
	int err;
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(filter);
	struct te_psdemux *demux = te_psdemux_from_obj(filter->parent);

	pr_debug("Attaching out filter %p to push sink %p\n", filter,
			target);
	err = output->push_intf.connect(filter, target);
	if (err) {
		pr_err("Failed to connect filter %p to push sink %p (%d)\n",
				filter, target, err);
		goto error;
	}

	if (output->buffer_obj) {
		pr_debug("registering buffer on sink connect\n");
		err = te_ps_bwq_register(demux->bwq, &output->obj,
				output->buffer_obj, output->buffer_work_func);
		if (err != -EEXIST && err != 0) {
			pr_err("unable to register buffer\n");
			goto error;
		}
	}

	err = stm_registry_add_connection(filter, STM_DATA_INTERFACE_PUSH,
			target);
	if (err) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_DATA_INTERFACE_PUSH, err);
		goto error;
	}
	return 0;
error:
	if (output->buffer_obj)
		if (te_ps_bwq_unregister(demux->bwq, output->buffer_obj))
			output->buffer_obj = NULL;
	if (output->push_intf.disconnect(filter, target))
		pr_warn("Cleanup: disconnect failed\n");
	return err;
}
