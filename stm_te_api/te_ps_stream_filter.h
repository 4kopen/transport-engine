/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_stream_filter.h

Defines stream filter specific operations
******************************************************************************/

#ifndef __TE_PS_STREAM_FILTER_H
#define __TE_PS_STREAM_FILTER_H

#include "te_object.h"
/* Max number of output filters that a stream filter may attach to
 *
 * Note that this is only the TE API limit. The actual max number of
 * connections depends on the type of input and output filter */
#define PS_MAX_INT_CONNECTIONS 1


/* TE STREAM filter object */
struct te_stream_filter {
	struct te_obj obj;
	/* Controls */
	uint32_t flushing_behaviour;

	/* Connections */
	struct te_obj *output_filters[PS_MAX_INT_CONNECTIONS];
	uint8_t stream_id;
	uint32_t sub_stream_id;
};

int te_stream_filter_new(struct te_obj *psdmx, uint8_t stream_id,
		struct te_obj **new_filter);

struct te_stream_filter *te_stream_filter_from_obj(struct te_obj *filter);

#endif
