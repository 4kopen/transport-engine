/******************************************************************************
Copyright (C) 2011, 2012 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Transport Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_hal_obj.c

Declares TE HAL objects
******************************************************************************/
#include "te_hal_obj.h"
#include "te_internal_cfg.h"
#include "te_object.h"

#include <stm_te_dbg.h>

static void release_obj(struct kref *ref)
{
	struct te_hal_obj *obj = container_of(ref, struct te_hal_obj, refcount);

	te_hal_obj_print(obj, "destroying object", __func__);

	if (ST_NO_ERROR != stptiOBJMAN_DeallocateObject(obj->hdl, true)) {
		pr_err("unable to deallocate handle 0x%08x\n", obj->hdl.word);
	}

	list_del(&obj->entry);
	list_del(&obj->c_entry);

	if (obj->parent)
		kref_put(&obj->parent->refcount, obj->parent->release);

	kfree(obj);
}

int __te_hal_obj_alloc(struct te_hal_obj **obj, struct te_hal_obj *parent,
		ObjectType_t type, void *params)
{
	int res = 0;
	FullHandle_t parent_hdl = stptiOBJMAN_NullHandle;
	ST_ErrorCode_t hal_err;
	char caller[100];

	struct te_hal_obj *new_obj = kzalloc(sizeof(*new_obj), GFP_KERNEL);

	if (!new_obj) {
		pr_err("couldn't allocate object\n");
		res = -ENOMEM;
		goto error;
	}

	kref_init(&new_obj->refcount);

	new_obj->release = release_obj;

	if (parent)
		parent_hdl = parent->hdl;

	hal_err = stptiOBJMAN_AllocateObject(
			parent_hdl, type, &new_obj->hdl, params);

	if (ST_NO_ERROR != hal_err) {
		pr_err("failed to allocate device type %d (err 0x%x)\n",
			type, hal_err);
		res = te_hal_err_to_errno(hal_err);
		goto error;
	}

	if (HAL_HDL_IS_NULL(new_obj->hdl)) {
		pr_err("allocated returned NULL pointer but didn't fail\n");
		res = -ENOMEM;
		goto error;
	}

	INIT_LIST_HEAD(&new_obj->entry);
	INIT_LIST_HEAD(&new_obj->children);
	INIT_LIST_HEAD(&new_obj->c_entry);

	if (parent) {
		new_obj->parent = parent;
		kref_get(&parent->refcount);
		list_add(&new_obj->c_entry, &parent->children);
	} else {
		pr_warning("Orphan HAL object created!\n");
	}


	*obj = new_obj;
	snprintf(caller, 100, "{%pS}", __builtin_return_address(0));
	te_hal_obj_print(new_obj, "#New", caller);

	return 0;

error:
	*obj = NULL;
	kfree(new_obj);

	return res;
}


static const char *__stringfy_hal_obj(ObjectType_t obj_type)
{
	const char *type;

	switch (obj_type) {
	case OBJECT_TYPE_INVALID:
		type = "Invalid";
		break;
	case OBJECT_TYPE_PDEVICE:
		type = "PDEVICE";
		break;
	case OBJECT_TYPE_VDEVICE:
		type = "VDEVICE";
		break;
	case OBJECT_TYPE_SESSION:
		type = "SESSION";
		break;
	case OBJECT_TYPE_SOFTWARE_INJECTOR:
		type = "SWINJECTOR";
		break;
	case OBJECT_TYPE_BUFFER:
		type = "BUFFER";
		break;
	case OBJECT_TYPE_FILTER:
		type = "FILTER";
		break;
	case OBJECT_TYPE_SIGNAL:
		type = "SIGNAL";
		break;
	case OBJECT_TYPE_INDEX:
		type = "INDEX";
		break;
	case OBJECT_TYPE_DATA_ENTRY:
		type = "DATA_ENTRY";
		break;
	case OBJECT_TYPE_SLOT:
		type = "SLOT";
		break;
	case OBJECT_TYPE_CONTAINER:
		type = "CONTAINER";
		break;

	case OBJECT_TYPE_ANY:
		type = "ANY";
		break;

	case STPTI_HANDLE_INVALID_OBJECT_TYPE:
		type = "INVALID";
		break;
	default:
		type = "UNKNOWN";
		break;
	}

	return type;
}


void te_hal_obj_print(struct te_hal_obj *obj, const char *pfx, const char *func)
{
	const char *type;

	if (!obj) {
		pr_debug("%s\nHAL obj NULL!\n", pfx);
		return;
	}

	type = __stringfy_hal_obj(obj->hdl.member.ObjectType);

	pr_debug("%s@[%s] HAL obj 0x%p: handle:0x%08x type:%s vDevice:%d num:%d\n",
			func, pfx, obj, obj->hdl.word, type,
			obj->hdl.member.vDevice, obj->hdl.member.Object);
}



void te_hal_obj_dump_tree(struct te_hal_obj *obj)
{
	struct te_hal_obj *next;

	if (!obj) {
		pr_debug("HAL obj NULL!\n");
		return;
	}

	te_hal_obj_print(obj, "", __func__);

	list_for_each_entry(next, &obj->children, c_entry) {
		te_hal_obj_dump_tree(next); /* bad bad recursion */
	}
}

int te_hal_obj_get_refcount(struct te_hal_obj *obj)
{
	int count = 0 ;
	struct kref *kref_count;

	if(!obj) {
		pr_debug("HAL obj NULL!\n");
		return count;
	}

	kref_count = &obj->refcount;
	count = atomic_read(&(kref_count->refcount));
	pr_debug("get hal_buffer:0x%p Hanlde:0x%x object ref count:%d\n",
			obj, obj->hdl.word, count);
	return count ;
}

