/******************************************************************************
Copyright (C) 2011, 2012 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Transport Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_output_filter.c

Defines base output filter specific operations
******************************************************************************/

#include <stm_te_dbg.h>
#include <te_object.h>
#include <pti_hal_api.h>
#include <te_interface.h>
#include <linux/dma-direction.h>
#include <asm/cacheflush.h>
#include <asm/page.h>
#include <asm/uaccess.h>

#include "te_object.h"
#include "te_global.h"
#include "te_output_filter.h"
#include "te_ts_filter.h"
#include "te_input_filter.h"
#include "te_pid_filter.h"
#include "te_ins_filter.h"
#include "te_rep_filter.h"
#include "te_ts_index_filter.h"
#include "te_pes_filter.h"
#include "te_section_filter.h"
#include "te_pcr_filter.h"
#include "te_demux.h"
#include "te_time.h"
#include "te_global.h"
#include <stm_memsrc.h>
#include <stm_event.h>
#include "te_sysfs.h"

#define TE_PAUSE 1
#define TE_RESUME 0
#define DEFAULT_FREE_BUF_SZ (512*1024)
#define IN_PKT_HAS_NO_EXTRA_BYTES(in_pkt_size)\
	(stptiHAL_DVB_PACKET_SIZE == in_pkt_size) 


#define get_free_space_info(of, buf) \
{ \
	ST_ErrorCode_t ret; \
	uint32_t bytes_in_buf = 0; \
	\
	ret = stptiHAL_call(Buffer.HAL_BufferStatus, \
					buf->hdl, \
				NULL, &bytes_in_buf, NULL, NULL, \
				NULL, NULL, (BOOL *) &of->overflow); \
	of->free_space = (ret == 0) ? \
		of->buf_aligned_size - bytes_in_buf : of->free_space; \
	pr_debug("OF:%s Cur Free space for Buf:0x%x is %d bytes Overflow:%d\n", \
			of->obj.name, buf->hdl.word, of->free_space, \
			of->overflow); \
}

static void __update_read_ptr(struct te_out_filter *of,
		struct te_hal_obj *buf, int read, int aligned_rd_offset);


static int update_pacing_info_for_pull(struct te_out_filter *of,
		uint32_t last_read);

/* Data struct for storing queued data packets */
struct te_queued_data {
	struct list_head lh;
	uint8_t *data;
	uint32_t size;
	bool valid;
};


static void  __update_read_ptr(struct te_out_filter *of,
		struct te_hal_obj *buf, int read, int aligned_rd_offset)
{
	int offset = of->rd_offs + read;

	if (of->push_intf.mode & STM_IOMODE_NON_BLOCKING_IO) {
		if (offset > of->buf_size)
			offset = (of->rd_offs + read) - of->buf_size;
		of->rd_offs = offset;
	} else
		of->rd_offs = aligned_rd_offset;

	pr_debug("Filter:%s RdOffset:%d Consumed:%d\n", of->obj.name,
			offset, read);
	stptiHAL_call(Buffer.HAL_BufferSetReadOffset, buf->hdl, of->rd_offs);
}


int te_out_filter_from_hdl(stm_object_h hdl, struct te_out_filter **output)
{
	int err;
	struct te_obj *object;

	err = te_obj_from_hdl(hdl, &object);
	if (err == 0)
		*output = te_out_filter_from_obj(object);
	else
		*output = NULL;
	return err;
}

static void te_out_filter_pull_work(struct te_out_filter *output,
		struct te_hal_obj *buf)
{
	ST_ErrorCode_t hal_err = ST_NO_ERROR;
	uint32_t wr_offs = 0, prev_wr_offs;
	int cur_bytes = 0, total = 0;
	int err = 0;
	struct te_demux *demux = te_demux_from_obj(output->obj.parent);

	/* Update buffer write offset */
	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return;

	/* Check for buffer overflow.
	 * Notify pull sink in case of overflow
	 */

	get_free_space_info(output, buf);
	if (output->overflow)
		te_out_filter_notify_pull_sink(output,
                        STM_MEMSINK_EVENT_BUFFER_OVERFLOW);


	hal_err = stptiHAL_call(Buffer.HAL_BufferGetWriteOffset, buf->hdl,
			&wr_offs);
	if (hal_err != ST_NO_ERROR) {
		pr_err("HAL_BufferGetWriteOffset returned 0x%x\n", hal_err);
		mutex_unlock(&output->obj.lock);
		return;
	}

	/* Write offset has not changed. Do nothing */
	if (output->wr_offs == wr_offs) {
		mutex_unlock(&output->obj.lock);
		return;
	}

	/* Update write offset */
	prev_wr_offs = output->wr_offs;
	output->wr_offs = wr_offs;

	if (output->pacing == true && !atomic_read(&output->dp_set)) {
		get_free_space_info(output, buf);
		output->data_in_buf = output->buf_aligned_size -
			output->free_space;
		te_demux_inc_dp_cnt(demux, output);
		atomic_inc(&output->dp_set);
	}

	total = (output->wr_offs - output->rd_offs);
	if (total < 0)
		total += output->buf_size;

	cur_bytes = (output->wr_offs - prev_wr_offs);
	if (cur_bytes < 0)
		cur_bytes += output->buf_size;

	pr_debug("OF:0x%s HalHdl:0x%x DataAvailable:%d CurData:%d wr_offs %d\n",
			output->obj.name, buf->hdl.word, total, cur_bytes,
			output->wr_offs);

	/* New data is available
	 * 1. Notify pull sink if not an overflow case.
	 * 2. Wake up any sleeping reader thread
	 */

	if (!output->overflow) {
		te_out_filter_notify_pull_sink(output,
                        STM_MEMSINK_EVENT_DATA_AVAILABLE);
	}
	mutex_unlock(&output->obj.lock);

	/* Wake up any waiting read thread */
	wake_up_interruptible(&output->reader_waitq);
}

static int te_out_filter_attach_to_pull_sink(struct te_obj *filter,
		stm_object_h target)
{
	int err;
	struct te_out_filter *output = te_out_filter_from_obj(filter);
	struct te_demux *demux = te_demux_from_obj(filter->parent);

	pr_debug("#ATTACH >> OF:%s <-> PullSink:0x%p\n", filter->name, target);

	err = output->pull_intf.connect(filter, target, output->pull_src_intf);
	if (err) {
		pr_err("Failed to connect filter %p to pull sink %p (%d)\n",
				filter, target, err);
		goto exit;
	}

	output->external_connection = target;

	err = stm_registry_add_connection(filter, STM_DATA_INTERFACE_PULL,
			target);
	if (err) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_DATA_INTERFACE_PULL, err);
		goto error;
	}

	if (output->hal_buffer) {
		pr_debug("OF:%s BWQ regisrter for buffer:0x%x\n",
				filter->name, output->hal_buffer->hdl.word);
		err = te_bwq_register(demux->bwq, &output->obj,
				output->hal_buffer, output->buffer_work_func);
		if (err != -EEXIST && err != 0) {
			pr_err("Output 0x%p unable to register buffer\n",
					output);
			goto error_bw;
		}
	}

	pr_debug("#ATTACH << OF:%s <-> PullSink:0x%p\n", filter->name, target);

	return 0;

error_bw:
	if (stm_registry_remove_connection(filter, STM_DATA_INTERFACE_PULL))
		pr_warn("Cleanup: Output 0x%p failed to unregister connection %s\n",
			output, STM_DATA_INTERFACE_PULL);

error:
	if (output->pull_intf.disconnect(filter, target))
		pr_warn("Cleanup: Output 0x%p disconnect failed\n", output);
	output->external_connection = NULL;

exit:
	return err;
}

static int te_out_filter_detach_from_pull_sink(struct te_obj *filter,
		stm_object_h target)
{
	int err;
	int result = 0;
	struct te_out_filter *output = te_out_filter_from_obj(filter);
	struct te_demux *demux = te_demux_from_obj(filter->parent);

	pr_debug("#DETACH >> OF:%s >-< PullSink:0x%p\n", filter->name, target);

	err = stm_registry_remove_connection(filter, STM_DATA_INTERFACE_PULL);
	if (err) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_DATA_INTERFACE_PULL, err);
		result = err;
	}

	err = mutex_lock_interruptible(&filter->lock);
	if(err < 0)
		return err;


	/* reset the external connection before unregsiter to ensure
	 * hal buffer is not freed while pull.
	 * */
	output->external_connection = NULL;
	mutex_unlock(&filter->lock);


	/* Wake up any thread currently blocked in pull_data */
	/* Check if the output filter is of type SECTION.
	 * Section filters are special as they share the same HAL buffer.
	 * For Section filters Work Q unregistration is only done when the
	 * section to be deleted is the last user on the same slot.
	 * This decision is taken during deattach of Input filter with the
	 * section filter.
	 * */
	if (filter->type != TE_OBJ_SECTION_FILTER)
		if (output->hal_buffer) {
			err = te_bwq_unregister(demux->bwq,
					output->hal_buffer);
			if (err < 0) {
				pr_err("Error:%d DMX:0x%p OF:0x%p HalBuf:0x%x WQ unregsitration failed\n",
						err, demux, output,
						output->hal_buffer->hdl.word);
			} else if (err > 0) {
				pr_debug("DMX:0x%p OF:0x%p halBuf:0x%x WQ unregsitered and HalBuf released\n",
						demux, output,
						output->hal_buffer->hdl.word);

				output->hal_buffer = NULL;
				output->buf_manual = false;
				output->obj.state = TE_OBJ_STOPPED;
			} else {
				pr_debug("DMX:0x%p OF:0x%p halBuf:0x%x WQ unregsitered, HalBuf reference active\n",
						demux, output,
						output->hal_buffer->hdl.word);
			}
		}

	/* deatch is in progress, wakeup any blocking reader to ensure
	 * disconnect call successfull on sink
	 * */
	wake_up_interruptible(&output->reader_waitq);

	err = output->pull_intf.disconnect(filter, target);
	if (err) {
		pr_err("Failed to disconnect %p from pull sink %p (%d)\n",
				filter, target, err);
		result = err;
	} else {
		memset(&output->pull_intf, 0, sizeof(output->pull_intf));
	}

	if (output->pacing == true && atomic_read(&output->dp_set)) {
		te_demux_dec_dp_cnt(demux, output);
		atomic_dec(&output->dp_set);
	}

	pr_debug("#DETACH << OF:%s >-< PullSink:0x%p\n", filter->name, target);

	return result;
}

static void invalidate_block_data(struct te_out_filter *output,
		int prev_wr_offs)
{
	int len;

	if (prev_wr_offs < output->wr_offs)
		len = output->wr_offs - prev_wr_offs;
	else
		len = (output->buf_size - prev_wr_offs + output->wr_offs);

	stptiHAL_call(Buffer.HAL_BufferInvalidateCache,
		output->hal_buffer->hdl, prev_wr_offs, len);

	return;
}

static int __get_blocks_from_offsets(struct te_out_filter *output,
		struct stm_data_block *block, int *count)
{
	uint32_t alignment_mask;
	int32_t new_rd_offs = -1;
	int32_t len = 0;

	/* Compute alignment mask */
	if (output->push_intf.alignment)
		alignment_mask = ~(output->push_intf.alignment - 1);
	else
		alignment_mask = ~0;

	if (output->rd_offs < output->wr_offs) {
		len = (output->wr_offs - output->rd_offs) & alignment_mask;
		if (!len)
			goto skip; /* nothing to do */
		block[0].data_addr = output->buf_start + output->rd_offs;
		block[0].len = len;
		*count = 1;
		new_rd_offs = output->rd_offs + len;
	} else {
		len = (output->buf_size - output->rd_offs + output->wr_offs) &
			alignment_mask;
		if (!len)
			goto skip; /* nothing to do */

		block[0].data_addr = output->buf_start + output->rd_offs;
		block[0].len = min((output->buf_size - output->rd_offs), len);
		*count = 1;

		len -= block[0].len;

		if (!len) {
			new_rd_offs = output->rd_offs + (int)block[0].len;
			if (new_rd_offs == output->buf_size)
				new_rd_offs = 0;
		} else {
			block[1].data_addr = output->buf_start;
			block[1].len = len;
			*count = 2;
			new_rd_offs = len;
		}
	}
skip:
	return new_rd_offs;
}

static void te_out_filter_push_work(struct te_out_filter *output,
		struct te_hal_obj *buf)
{
	ST_ErrorCode_t Error = ST_NO_ERROR;
	struct stm_data_block block[2] = {};
	int read = 0;
	int blocks = 0;
	int err = 0;
	struct te_demux *demux;
	int new_rd_offs, prev_wr_offs;
	struct te_push_stats_s *push_stat;
	struct timespec ts;
	ktime_t start_time, t1, t2;


	getrawmonotonic(&ts);
	start_time = timespec_to_ktime(ts);

	if (mutex_lock_interruptible(&output->obj.lock) != 0)
		return;

	if (!output->external_connection) {
		mutex_unlock(&output->obj.lock);
		return;
	}

	demux = te_demux_from_obj(output->obj.parent);
	push_stat = &output->push_stats;

	push_stat->latency = ktime_to_ms(ktime_sub(start_time,
				push_stat->last_stamp));

	prev_wr_offs = output->wr_offs;
	/* Read the data from the buffer */
	Error = stptiHAL_call(Buffer.HAL_BufferGetWriteOffset, buf->hdl, &output->wr_offs);
	if (Error != ST_NO_ERROR) {
		pr_err("HAL_BufferGetWriteOffset returned 0x%x\n", Error);
		goto done;
	}

	if (output->rd_offs == output->wr_offs)
		goto done; /* nothing to do */


	if (output->pacing == true && !atomic_read(&output->dp_set)) {
		get_free_space_info(output, buf);
		te_demux_inc_dp_cnt(demux, output);
		atomic_inc(&output->dp_set);
	}

	new_rd_offs = __get_blocks_from_offsets(output, block, &blocks);
	if (new_rd_offs < 0)
		goto skip_push;

	push_stat->request_bytes = __get_bytes_from_blocks(block, 2);

	pr_debug("#PUSHWORK >> OF:0x%s ReqBytes:%d  SinkFunc:%pS\n",
			output->obj.name, push_stat->request_bytes,
			output->push_intf.push_data);


	invalidate_block_data(output, prev_wr_offs);

	getrawmonotonic(&ts);
	t1 = timespec_to_ktime(ts);

	err = output->push_intf.push_data(output->external_connection, block,
					blocks, &read);
	if (err)
		pr_warn_ratelimited("Push_data failed %d\n", err);

	getrawmonotonic(&ts);
	t2 = timespec_to_ktime(ts);

	push_stat->consumption = ktime_to_ms(ktime_sub(t2, t1));
	__update_read_ptr(output, buf, read, new_rd_offs);

	getrawmonotonic(&ts);
	push_stat->last_stamp = timespec_to_ktime(ts);

	push_stat->processing = ktime_to_ms(ktime_sub(push_stat->last_stamp,
				start_time));
	pr_debug("#PUSHWORK << OF:0x%s Latency:%ld Push:%ld Total:%ld\n",
			output->obj.name, push_stat->latency,
			push_stat->consumption, push_stat->processing);

skip_push:
	if (output->pacing == true) {
		get_free_space_info(output, buf);
		if (atomic_read(&output->dp_set)) {
			te_demux_dec_dp_cnt(demux, output);
			atomic_dec(&output->dp_set);
		}
	}

done:

	mutex_unlock(&output->obj.lock);
	if (output->buf_monitoring == true) {
		pr_debug("#PUSHEVENT OF:0x%s DMX:0x%p Src:0x%p\n",
				output->obj.name, demux, demux->upstream_obj);
		te_demux_notify_src(demux, &output->obj, output->free_space);
	}
	return;
}

void te_out_filter_work(struct te_obj *obj, struct te_hal_obj *buf)
{
	struct te_out_filter *output = te_out_filter_from_obj(obj);

	if (obj->state == TE_OBJ_PAUSED) {
		pr_debug("OF:%s: Buffer %s paused\n", __func__, obj->name);
		return;
	}

	if (output->pull_intf.connect)
		te_out_filter_pull_work(output, buf);

	if (output->push_intf.connect)
		te_out_filter_push_work(output, buf);
}

static int te_out_filter_attach_to_push_sink(struct te_obj *filter,
		stm_object_h target)
{
	int err;
	struct te_out_filter *output = te_out_filter_from_obj(filter);
	struct te_demux *demux = te_demux_from_obj(filter->parent);

	pr_debug("#ATTACH >> OF:0x%s <-> PushSink:0x%p\n",
			filter->name, target);

	err = output->push_intf.connect(filter, target);
	if (err) {
		pr_err("Failed to connect filter %p to push sink %p (%d)\n",
				filter, target, err);
		goto exit;
	}

	output->external_connection = target;

	err = stm_registry_add_connection(filter, STM_DATA_INTERFACE_PUSH,
			target);
	if (err) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_DATA_INTERFACE_PUSH, err);
		goto error;
	}

	if (output->hal_buffer) {
		if ((filter->type == TE_OBJ_PES_FILTER || filter->type ==
			TE_OBJ_TS_FILTER) && (output->push_intf.mode ==
			STM_IOMODE_NON_BLOCKING_IO) && (output->buf_size > 0)) {
			 /* For non blocking sinks, once buffer threshold is
			 * reached, TE will schedule buffer work on every
			 * packet received. This can flood the system with too
			 * many push work calls. Therefore, we set the buffer
			 * threshold for PES and TS filter equal to buffer size.
			 */
			stptiHAL_call(Buffer.HAL_BufferSetThreshold,
					output->hal_buffer->hdl,
					output->buf_size);
		}
		pr_debug("DMX:0x%p OF:0x%s hal_buf:0x%x registering work\n",
				demux, filter->name,
				output->hal_buffer->hdl.word);

		err = te_bwq_register(demux->bwq, &output->obj,
				output->hal_buffer, output->buffer_work_func);
		if (err != -EEXIST && err != 0) {
			pr_err("Output 0x%p unable to register buffer\n",
					output);
			goto error_bw;
		}

	}

	pr_debug("#ATTACH << OF:0x%s <-> PushSink:0x%p\n", filter->name,
			target);
	return 0;

error_bw:
	if (stm_registry_remove_connection(&output->obj, STM_DATA_INTERFACE_PUSH))
		pr_warn("Cleanup: Output 0x%p failed to unregister connection %s\n",
			output, STM_DATA_INTERFACE_PUSH);

error:
	if (output->push_intf.disconnect(filter, target))
		pr_warn("Cleanup: Output 0x%p disconnect failed\n", output);
	output->external_connection = NULL;

exit:
	return err;
}

/*!
 * \brief Copies data from an output filter into a kernel buffer
 *
 * Reads data directly from the output filter's HAL buffer, ignoring data
 * quantisation
 *
 * \param output      Output filter to read from
 * \param buf         Kernel virtual buffer to copy data into
 * \param buf_size    Size of buffer pointed to by buf
 * \param bytes_read  Pointer to uint32_t set to the actual number of bytes
 *                    copied by this function
 *
 * \retval 0       Success
 * \retval -EINVAL HAL error occurrred
 */
int te_out_filter_read_bytes(struct te_out_filter *output, uint8_t *buf,
		uint32_t buf_size, uint32_t *bytes_read)
{
	uint32_t read_offs = stptiHAL_CURRENT_READ_OFFSET;
	ST_ErrorCode_t hal_err;
	FullHandle_t buf_hdl;
	int err;

	*bytes_read = 0;

	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return err;

	if (output->hal_buffer)
		buf_hdl = output->hal_buffer->hdl;
	else {
		mutex_unlock(&output->obj.lock);
		return -EINVAL;
	}

	hal_err = stptiHAL_call(Buffer.HAL_BufferRead, buf_hdl,
			stptiHAL_READ_IGNORE_QUANTISATION,
			&read_offs, 0,
			buf, buf_size, NULL, 0, NULL, 0,
			output->copy_function, bytes_read);

	/* Update read pointer and resignal if necessary (intentionally
	 * ignore returned error) */
	stptiHAL_call(Buffer.HAL_BufferSetReadOffset, buf_hdl, read_offs);

	output->rd_offs = read_offs;

	mutex_unlock(&output->obj.lock);

	return te_hal_err_to_errno(hal_err);
}

/*!
 * \brief Determines how much data will be read by the next read call on this
 * output filter using the current HAL buffer read/write offsets
 *
 * \param output Output filter to test
 *
 * \return Size of data available to read in the next read call
 */
int32_t te_out_filter_next_read_size(struct te_out_filter *output)
{
	/* Currently we ignore any quantisation units so the amount of data
	 * that can be read is equal to the current amount data in the buffer
	 */
	int32_t size = 0;
	int err;

	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return err;

	if (output->wr_offs >= output->rd_offs) {
		size = output->wr_offs - output->rd_offs;
	} else {
		size = output->buf_size - output->rd_offs;
		size += output->wr_offs;
	}

	mutex_unlock(&output->obj.lock);

	pr_debug("OF:%s available size=%u\n",
			output->obj.name, size);

	return size;
}

static int te_out_filter_detach_from_push_sink(struct te_out_filter *output,
		stm_object_h target)
{
	int err, ret;

	struct te_demux *demux = te_demux_from_obj(output->obj.parent);

	pr_debug("#DETACH >> OF:%s >-< PushSink:0x%p\n", output->obj.name,
			target);

	err = stm_registry_remove_connection(&output->obj,
						STM_DATA_INTERFACE_PUSH);
	if (err) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_DATA_INTERFACE_PUSH, err);
	}

	/* Disconnect the push interface before unregistering the WQ
	 * This is to ensure the push sink doesn't block any pending push
	 * and release it.
	 * Inside WQ de-registration we wait for all pending WORK before
	 * releasing HAL buffer.
	 * */
	err = output->push_intf.disconnect(&output->obj, target);
	if (err)
		pr_err("Failed to disconnect %p from %p\n",
				&output->obj,
				target);

	ret = mutex_lock_interruptible(&output->obj.lock);
	if (ret) {
		pr_err("Failed to detach filter %p from push sink %p\n",
			&output->obj, target);
		return ret;
	}

	/* Reset the external_connection unconditionally. */
	output->external_connection = NULL;

	mutex_unlock(&output->obj.lock);

	if (output->hal_buffer) {
		pr_debug("OF:%s DMX:0x%p HalBuf:0x%x PushSink:0x%p\n",
				output->obj.name, demux,
				output->hal_buffer->hdl.word, target);

		ret = te_bwq_unregister(demux->bwq, output->hal_buffer);
		if (ret < 0) {
			pr_err("Error:%d DMX:0x%p OF:0x%p HalBuf:0x%x WQ unregsitration failed\n",
					err, demux, output,
					output->hal_buffer->hdl.word);
		} else if (ret > 0) {
			pr_debug("DMX:0x%p OF:0x%p halBuf:0x%x WQ unregsitered and HalBuf released\n",
					demux, output,
					output->hal_buffer->hdl.word);

			output->hal_buffer = NULL;
			output->buf_manual = false;
			output->obj.state = TE_OBJ_STOPPED;
		} else {
			pr_debug("DMX:0x%p OF:0x%p halBuf:0x%x WQ unregsitered, HalBuf reference active\n",
					demux, output,
					output->hal_buffer->hdl.word);
		}
	}

	if (err == 0)
		memset(&output->push_intf, 0, sizeof(output->push_intf));
	if (output->pacing == true && atomic_read(&output->dp_set)) {
		te_demux_dec_dp_cnt(demux, output);
		atomic_dec(&output->dp_set);
	}

	pr_debug("#DETACH << OF:%s >-< PushSink:0x%p\n", output->obj.name,
			target);
	return err;
}

ST_ErrorCode_t te_out_filter_memcpy(void **dest_pp, const void *src_p,
		size_t size)
{
	/* Note: dest_pp is not a void *, but a void ** */
	memcpy(*dest_pp, src_p, size);
	*dest_pp = (void *)((uint8_t *)(*dest_pp) + size);
	return ST_NO_ERROR;
}

ST_ErrorCode_t te_out_filter_user_memcpy(void **dest_pp, const void *src_p,
		size_t size)
{
	/* Note: dest_pp is not a void *, but a void ** */
	int size_cc;
	ST_ErrorCode_t hal_err = ST_NO_ERROR;
	size_cc = copy_to_user(*dest_pp, src_p, size);
	/* In case copy is not successfull, instead of partial update
	 * roll back to original locations.
	 * Although if copy_to_user is not successful, we have major bug
	 * in system */
	if (size_cc) {
		pr_err("bytes not copied %d, to copy %d src %p dst %p\n",
		size_cc, size, src_p, dest_pp);
		hal_err = ST_ERROR_NO_MEMORY;
	}
	else
		*dest_pp = (void *)((uint8_t *)(*dest_pp) + size);
	return hal_err;
}

int te_out_filter_attach(struct te_obj *filter, stm_object_h target)
{
	int err;
	stm_object_h target_type;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;
	struct te_out_filter *output = NULL;

	if (filter)
		output = te_out_filter_from_obj(filter);
	else
		return -EINVAL;

	/* Check if this output filter is already attached to an external
	 * object */
	if (output->external_connection) {
		pr_err("OF:%s already has an attachment\n", filter->name);
		return -EBUSY;
	}

	err = stm_registry_get_object_type(target, &target_type);
	if (0 != err) {
		pr_err("unable to get type of target object %p\n", target);
		return err;
	}

	/* Look for supported interfaces of the target object.
	 * Currently looks for the following interfaces:
	 * 1. STM_DATA_INTERFACE_PULL
	 * 2. STM_DATA_INTERFACE_PUSH
	 */
	if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PULL, type_tag,
				sizeof(stm_data_interface_pull_sink_t),
				&output->pull_intf,
				&iface_size)) {
		pr_debug("Attaching OF:%s <-> pull sink:0x%p\n", filter->name,
				target);
		err = te_out_filter_attach_to_pull_sink(filter, target);
	} else if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PUSH, type_tag,
				sizeof(stm_data_interface_push_sink_t),
				&output->push_intf,
				&iface_size)) {
		pr_debug("Attaching OF:%s <-> push sink:0x%p\n", filter->name,
				target);
		err = te_out_filter_attach_to_push_sink(filter, target);
	} else {
		pr_err("Attach OF:%s <-> %p: no supported interfaces\n",
				filter->name, target);
		err = -EPERM;
	}

	if (err == 0 && output->pull_intf.connect) {
		stm_data_interface_pull_sink_t tmp_sink;
		if (0 == stm_registry_get_attribute(target,
				STM_DATA_INTERFACE_PULL, type_tag,
				sizeof(stm_data_interface_pull_sink_t),
				&tmp_sink,
				&iface_size)) {
			output->pull_intf.mode = tmp_sink.mode;
			output->pull_intf.mem_type = tmp_sink.mem_type;
		}
		if (output->pull_intf.mem_type == USER &&
			output->pull_src_intf->user_copy_supported == true) {
			pr_debug("using copy to user output filter %p\n",
				output);
			output->copy_function = te_out_filter_user_memcpy;
		}
	}

	pr_debug("Error:%d OF:%s Target:0x%p push.connect:%pS, pull.connect:%pS\n",
			err, filter->name, target, output->push_intf.connect,
			output->pull_intf.connect);

	return err;
}

int te_out_filter_detach(struct te_obj *filter, stm_object_h target)
{
	int err;
	stm_object_h target_type;
	stm_object_h connected_obj = NULL;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;
	struct te_out_filter *output = NULL;

	if (filter)
		output = te_out_filter_from_obj(filter);
	else
		return -EINVAL;

	if (output->external_connection != target) {
		pr_err("Output filter %p not attached to %p\n", filter, target);
		return -EINVAL;
	}

	/* Check for pull or push data connection */
	err = stm_registry_get_object_type(target, &target_type);
	if (0 != err) {
		pr_err("unable to get type of target object %p\n", target);
		return err;
	}

	/* Look for supported interfaces of the target object.
	 * Currently looks for the following interfaces:
	 * 1. STM_DATA_INTERFACE_PULL
	 * 2. STM_DATA_INTERFACE_PUSH
	 */
	if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PULL, type_tag,
				sizeof(stm_data_interface_pull_sink_t),
				&output->pull_intf,
				&iface_size)) {
		pr_debug("detaching OF:%s >-< pull sink:0x%p\n", filter->name,
				target);
		err = stm_registry_get_connection(te_obj_to_hdl(filter),
				STM_DATA_INTERFACE_PULL, &connected_obj);
		if (0 == err && connected_obj == target)
			err = te_out_filter_detach_from_pull_sink(filter,
					target);
	} else if (0 == stm_registry_get_attribute(target_type,
				STM_DATA_INTERFACE_PUSH, type_tag,
				sizeof(stm_data_interface_push_sink_t),
				&output->push_intf,
				&iface_size)) {
		pr_debug("detaching OF:%s >-< push sink:0x%p\n", filter->name,
				target);
		err = stm_registry_get_connection(te_obj_to_hdl(filter),
				STM_DATA_INTERFACE_PUSH, &connected_obj);
		if (0 == err && connected_obj == target)
			err = te_out_filter_detach_from_push_sink(output,
					target);
	} else {
		pr_err("Detach %s >-< %p: no supported interfaces\n",
				filter->name, target);
		err = -EPERM;
	}

	return err;
}

/*!
 * \brief Gets the size of the data unit at the current head of the queue for a
 * output filter
 *
 * \param output output filter to discard data from
 * \param size   Returned size of the data unit at the current head of the queue
 *
 * Note that when the output filter is in bytestream read mode then the this
 * will return the size of all of the data in the queue. Conversely when the
 * filter is in quantisation unit read mode, then this will return the size of
 * the item at the head of the the queue.
 *
 * \retval 0 Success
 * \retval -EINTR Interrupted by signal
 */
int te_out_filter_queue_peek(struct te_out_filter *output, uint32_t *size)
{
	int err;
	struct te_queued_data *q;

	*size = 0;

	err = mutex_lock_interruptible(&output->queue_lock);
	if (err < 0)
		return err;

	if (output->read_quantisation_units) {
		if (!list_empty(&output->queued_data)) {
			q = list_first_entry(&output->queued_data,
					struct te_queued_data, lh);
			*size = q->size;
		}
	} else {
		*size = output->total_queued_data;
	}

	mutex_unlock(&output->queue_lock);
	return err;
}

/*!
 * \brief Gets the current packet count for an output filter
 *
 * \param filter Filter to check
 * \param count  Pointer set with current packet count (or 0 if the filter is
 *               not started)
 *
 * \retval 0    Success
 * \retval -EIO HAL error
 */
int te_out_filter_get_pkt_count(struct te_obj *filter, uint32_t *count)
{
	int err = 0;
	ST_ErrorCode_t hal_err;
	struct te_out_filter *output = te_out_filter_from_obj(filter);

	if (filter->state == TE_OBJ_STARTED && (NULL != output->hal_buffer)) {
		hal_err = stptiHAL_call(Buffer.HAL_BufferStatus,
				output->hal_buffer->hdl,
				NULL, NULL, count, NULL, NULL, NULL, NULL);
		if (ST_NO_ERROR != hal_err) {
			pr_err("OF:%s HAL_BufferStatus return 0x%x\n",
					filter->name, hal_err);
			err = te_hal_err_to_errno(hal_err);
		}
	} else {
		*count = 0;
	}

	return err;
}

/*!
 * \brief Gets the current output filter statistics information
 *
 * \param obj Filter to check
 * \param stat  Pointer to stat
 *
 * \retval 0    Success
 * \retval -EIO HAL error
 */
static int te_out_filter_get_stat(struct te_obj *obj,
				stm_te_output_filter_stats_t *stat)
{
	int err = 0;
	ST_ErrorCode_t hal_err;
	BOOL buffer_overflows = false;
	struct timespec ts;
	ktime_t ktime;

	struct te_out_filter *output = te_out_filter_from_obj(obj);

	memset(stat, 0, sizeof(*stat));

	if (obj->state == TE_OBJ_STARTED && (output->hal_buffer != NULL)) {
		stat->crc_errors = 0;

		hal_err = stptiHAL_call(Buffer.HAL_BufferStatus,
				output->hal_buffer->hdl,
				NULL, &stat->bytes_in_buffer,
				&stat->packet_count, NULL, NULL, NULL,
				&buffer_overflows);
		if (ST_NO_ERROR != hal_err) {
			pr_err("HAL_BufferStatus return 0x%x\n", hal_err);
			err = te_hal_err_to_errno(hal_err);
			goto exit;
		}

		stat->buffer_overflows = (uint32_t)buffer_overflows;

	}

exit:
	getrawmonotonic(&ts);
	ktime = timespec_to_ktime(ts);
	stat->system_time = ktime_to_us(ktime);
	return err;
}

/*!
 * \brief Gets the default HAL buffer parameters for a given output filter type
 *
 * \param type Type of buffer to get HAL parameters for
 * \param hal_buf_params     Returned HAL buffer parameters
 */
void te_out_filter_get_buffer_params(struct te_out_filter *output,
				 stptiHAL_BufferConfigParams_t *hal_buf_params)
{

	memset(hal_buf_params, 0, sizeof(stptiHAL_BufferConfigParams_t));

	hal_buf_params->ManuallyAllocatedBuffer = false;
	hal_buf_params->PhysicalAddressSupplied = false;
	hal_buf_params->BufferStart_p = NULL;

	switch (output->obj.type) {
		case TE_OBJ_TS_FILTER:
			te_ts_filter_get_buffer_param(&output->obj,
					hal_buf_params);
			return;
		case TE_OBJ_PES_FILTER:
			hal_buf_params->BufferSize = te_cfg.pes_buffer_size;
			hal_buf_params->Zone = stptiHAL_ZONE_LARGE_SECURE;
			break;

		case TE_OBJ_SECTION_FILTER:
			hal_buf_params->BufferSize = te_cfg.section_buffer_size;
			hal_buf_params->Zone = stptiHAL_ZONE_HM_SMALL_UC;
			break;

		case TE_OBJ_PCR_FILTER:
			hal_buf_params->BufferSize = te_cfg.pcr_buffer_size;
			hal_buf_params->Zone = stptiHAL_ZONE_HM_SMALL_UC;
			break;

		case TE_OBJ_TS_INDEX_FILTER:
			hal_buf_params->BufferSize = te_cfg.index_buffer_size;
			hal_buf_params->Zone = stptiHAL_ZONE_HM_SMALL_UC;
			break;

			/* if user passes in an input filter give them some default
			 * buffer params */
		default:
			hal_buf_params->BufferSize = te_cfg.ts_buffer_size;
			break;
	}

	if (output->buf_size != 0) {
		/* use user defined size */
		hal_buf_params->BufferSize = output->buf_size;
		if (hal_buf_params->BufferSize <= (100*1024) &&
				hal_buf_params->BufferSize > (16*1024)) {
			hal_buf_params->Zone = stptiHAL_ZONE_HM_MED_UC;
		}
	}
}

/*!
 * \brief Allocates a HAL buffer
 *
 * The size of the HAL buffer depends on the output filter type
 *
 * \param filter            TE output filter object to allocate buffers fro
 * \param hal_buffer_handle Returned HAL buffer handle
 * \param hal_buf_params    Returned HAL buffer parameters used to allocate the
 *                          HAL buffer
 *
 * \retval 0       Success
 * \retval -ENOMEM Failed to allocate HAL buffer
 */
int te_out_filter_buffer_alloc(struct te_obj *filter,
			struct te_hal_obj **hal_buffer,
			stptiHAL_BufferConfigParams_t *params)
{
	struct te_demux *demux = te_demux_from_obj(filter->parent);
	struct te_out_filter *output = te_out_filter_from_obj(filter);

	te_out_filter_get_buffer_params(output, params);

	/* Reset pointers for the new buffer */
	output->wr_offs = 0;
	output->rd_offs = 0;
	output->overflow = false;

	if (te_hal_obj_alloc(hal_buffer, demux->hal_session,
				OBJECT_TYPE_BUFFER, params)) {
		pr_err("couldn't allocate buffer\n");
		return -ENOMEM;
	}
	return 0;
}

/*!
 * \briefs Gets the appropriate HAL slot type for an output filter
 *
 * \param object_type     TE object type to map
 * \param stpti_slot_type Returned HAL slot type
 *
 * \retval 0       Success
 * \retval -EINVAL Bad parameter or the object type does not map to a TE HAL
 *                 slot type
 */
static int te_out_filter_to_stpti_slot_type(struct te_obj *out_flt,
		enum te_obj_type_e type,
		stptiHAL_SlotMode_t *stpti_slot_type)
{
	int result = 0;

	if (stpti_slot_type == NULL) {
		result = -EINVAL;
	} else {
		switch (type) {
		case TE_OBJ_SECTION_FILTER:
			*stpti_slot_type = stptiHAL_SLOT_TYPE_SECTION;			
			break;

		case TE_OBJ_PCR_FILTER:
			*stpti_slot_type = stptiHAL_SLOT_TYPE_PCR;
			break;

		case TE_OBJ_PES_FILTER:
			*stpti_slot_type = stptiHAL_SLOT_TYPE_PES;
			break;

		case TE_OBJ_TS_FILTER:
			*stpti_slot_type = stptiHAL_SLOT_TYPE_RAW;
			break;
 		default:
			/* does not map to a stpti slot type */
			result = -EINVAL;
			break;
		}
	}
	return result;
}

/*!
 * \brief Creates and associates the necessary HAL objects to connect a pid
 * filter to an output filter
 *
 * \param out_flt TE Output filter to attach pid_filt to
 * \param in_flt TE PID filter object to attach
 *
 * \retval 0       Success
 * \retval -EEXIST The objects are already attached
 * \retval -EINVAL The supplied objects cannot be attached
 * \retval -ENOMEM Failed to allocate the required HAL resource
 */
int te_out_filter_connect_input(struct te_obj *out_flt, struct te_obj *in_flt)
{
	ST_ErrorCode_t Error = ST_NO_ERROR;
	stptiHAL_SlotConfigParams_t slot_params;
	int result = 0;

	struct te_hal_obj *hal_slot = NULL, *hal_filter = NULL;

	struct te_in_filter *input = te_in_filter_from_obj(in_flt);
	struct te_out_filter *output = te_out_filter_from_obj(out_flt);
	struct te_demux *demux = te_demux_from_obj(out_flt->parent);
	U32 paddr;
	U32 aligned_size = 0;
	struct timespec ts;

	memset(&slot_params, 0, sizeof(stptiHAL_SlotConfigParams_t));

	pr_debug("#ATTACH >> IF:%s <-> OF:%s\n", in_flt->name, out_flt->name);

	if (!te_in_filter_has_slot_space(input)) {
		pr_err("maximum slots reached\n");
		return -ENOMEM;
	}
	pr_debug("OF:%s free slots available\n", out_flt->name);

	if ((in_flt->type == TE_OBJ_PID_INS_FILTER
	   || in_flt->type == TE_OBJ_PID_REP_FILTER)
	   && out_flt->type != TE_OBJ_TS_FILTER) {
		pr_err("pid ins/rep filter can only be connected to ts output filter\n");
		return -EINVAL;
	}

	/* Find out the slot type required from the output filter object */
	switch (out_flt->type) {
	case TE_OBJ_DEMUX:
		pr_debug("OF:%s connect to demux\n", out_flt->name);
		return -EINVAL;
		break;
	case TE_OBJ_SECTION_FILTER:
		pr_debug("OF:%s connect to section\n", out_flt->name);
		return attach_pid_to_sf(in_flt, out_flt);
		break;
	case TE_OBJ_PES_FILTER:
		pr_debug("OF:%s connect to PES\n", out_flt->name);
		slot_params.SlotMode = stptiHAL_SLOT_TYPE_PES;
		slot_params.SuppressMetaData = true;
		if (output->read_quantisation_units)
			slot_params.SoftwareCDFifo = false;
		else
			slot_params.SoftwareCDFifo = true;
		break;
	case TE_OBJ_PCR_FILTER:
		pr_debug("OF:%s connect to PCR\n", out_flt->name);
		slot_params.SlotMode = stptiHAL_SLOT_TYPE_PCR;
		result = te_pcr_init_time_obj(out_flt);
		if (result) {
			pr_err("OF:%s Time obj init failed\n", out_flt->name);
			goto error;
		}
		break;
	case TE_OBJ_TS_INDEX_FILTER:
		pr_debug("OF:%s connect to TS_INDEX\n", out_flt->name);
		return attach_pid_to_index(in_flt, out_flt);
		break;
	case TE_OBJ_TS_FILTER:
	default:
		pr_debug("Of:%s connect to TS (and everything else)\n",
				out_flt->name);

		slot_params.SlotMode = stptiHAL_SLOT_TYPE_RAW;

		if (in_flt->type == TE_OBJ_PID_INS_FILTER)
			return te_ins_filter_attach_out(in_flt, out_flt);

		if (in_flt->type == TE_OBJ_PID_REP_FILTER)
			return te_rep_filter_attach_out(in_flt, out_flt);

		break;
	}

	/* Only create the output filter's buffer handle if required */
	if (output->hal_buffer == NULL) {

		stptiHAL_BufferConfigParams_t params = {0};

		pr_debug("OF:%s creating HAL buffer\n", out_flt->name);

		/* Ok lets allocate a buffer */
		te_out_filter_get_buffer_params(output, &params);

		/* Reset pointers for the new buffer */
		output->wr_offs = 0;
		output->rd_offs = 0;
		output->overflow = false;

		result = te_hal_obj_alloc(&output->hal_buffer,
					demux->hal_session,
					OBJECT_TYPE_BUFFER, &params);
		if (result != 0) {
			pr_err("OF:%s couldn't allocate buffer\n",
					out_flt->name);
			goto error;
		}

		/*Store buffer physical address*/
		result = stptiHAL_call(Buffer.HAL_BufferGetPhysicalAddr,
				output->hal_buffer->hdl, (U32
					*)&paddr,&aligned_size);
		if (ST_NO_ERROR != result) {
			pr_err("OF:%s HAL_BufferGetPhysicalAddr error : %d\n",
					out_flt->name, result);
			result = te_hal_err_to_errno(result);
			goto error;
		}


		output->buf_phyaddr = paddr;
		output->buf_aligned_size = aligned_size;

		/*Initialize the push stat time stamp*/
		getrawmonotonic(&ts);
		output->push_stats.last_stamp = timespec_to_ktime(ts);

		/* set initial free size , max to 512Kb. This is to ensure that
		 * first injection doesn't inject too much data before kworker
		 * gets scehduled.
		 * */
		output->free_space = min_t(uint32_t,
				aligned_size, DEFAULT_FREE_BUF_SZ);

		if ((out_flt->type == TE_OBJ_PES_FILTER || out_flt->type ==
			TE_OBJ_TS_FILTER) && output->push_intf.connect &&
			(output->push_intf.mode == STM_IOMODE_NON_BLOCKING_IO)) {
			 /* For non blocking sinks, once buffer threshold is
			 * reached, TE will schedule buffer work on every
			 * packet received. This can flood the system with too
			 * many push work calls. Therefore, we set the buffer
			 * threshold for PES and TS filter equal to buffer size.
			 */
			stptiHAL_call(Buffer.HAL_BufferSetThreshold,
					output->hal_buffer->hdl,
					params.BufferSize);
		} else if (out_flt->type == TE_OBJ_PES_FILTER) 
		{
			stptiHAL_call(Buffer.HAL_BufferSetThreshold,
				      output->hal_buffer->hdl,
				      3 * params.BufferSize / 4); /* 75% threshold */

		} else if (out_flt->type == TE_OBJ_PCR_FILTER) {
			stptiHAL_call(Buffer.HAL_BufferSetThreshold,
					      output->hal_buffer->hdl, 1);

		}
		stptiHAL_call(Buffer.HAL_BufferSetOverflowControl,
					      output->hal_buffer->hdl,
					      output->overflow_behaviour);

		/* The HAL returns the BufferStart in params.BufferStart_p */
		output->buf_start = params.BufferStart_p;
		output->buf_size = params.BufferSize;

		pr_debug("OF:%s HalHdl:0x%x PhyAddr:0x%x VirtAddr:0x%x Size:0x%x\n",
			out_flt->name, output->hal_buffer->hdl.word,
			(unsigned)paddr, (unsigned)output->buf_start,
			output->buf_size);


	} else {
		/* Reuse existing buffer, but increment refcount */
		pr_debug("OF:%s reusing HAL buffer:0x%x\n", out_flt->name,
				output->hal_buffer->hdl.word);
		te_hal_obj_inc(output->hal_buffer);
	}

	pr_debug("OF:%s PTI buffer handle 0x%x\n", out_flt->name,
			output->hal_buffer->hdl.word);

	/* Allocate a slot and link it to the buffer */
	result = te_hal_obj_alloc(&hal_slot, demux->hal_session,
					OBJECT_TYPE_SLOT, &slot_params);

	if (result != 0) {
		pr_err("OF:%s couldn't allocate slot\n", out_flt->name);
		goto error;
	}

	/* Store it in the list */
	list_add(&hal_slot->entry, &input->slots);

	if (TE_OBJ_TS_FILTER == out_flt->type) {
		BOOL dlna = false;
		pr_debug("OF:%s connecting TS filter\n", out_flt->name);
		if (te_ts_filter_is_security_formatted(out_flt))
			/* Output: If no local scramble (LS) set,
			 * records input stream. If LS set, records
			 * scrambled stream.  Scrambled data, remains
			 * scrambled.
			 */
			Error = stptiHAL_call(
					Slot.HAL_SlotSetSecurePathOutputNode,
					hal_slot->hdl,
					stptiHAL_SECUREPATH_OUTPUT_NODE_SCRAMBLED);
		else
			Error = stptiHAL_call(
					Slot.HAL_SlotSetSecurePathOutputNode,
					hal_slot->hdl,
					stptiHAL_SECUREPATH_OUTPUT_NODE_CLEAR);

		if (Error != ST_NO_ERROR) {
			pr_err("OF:%s SlotSetSecurePathOutputNode error 0x%x\n",
					out_flt->name, Error);
			result = te_hal_err_to_errno(Error);
			goto error;
		}

		if (te_ts_filter_is_dlna_formatted(out_flt))
			dlna = true;

		Error = stptiHAL_call(Slot.HAL_SlotFeatureEnable,
				hal_slot->hdl,
				stptiHAL_SLOT_OUTPUT_DNLA_TS_TAG,
				dlna);

		if (ST_NO_ERROR != Error) {
			result = te_hal_err_to_errno(Error);
			goto error;
		}
		if (dlna)
			output->extra_bytes_per_out_pkt = TTS_BYTE_LENGTH;

	}

	if (TE_OBJ_PES_FILTER == out_flt->type) {
		uint8_t stream_id = te_pes_filter_get_stream_id(out_flt);

		pr_debug("OF:%s connecting PES filter, stream_id %d\n",
				out_flt->name, stream_id);
		if (stream_id) {
			if (!output->hal_filter) {
				stptiHAL_FilterConfigParams_t params = {
					stptiHAL_PES_STREAMID_FILTER
				};

				result = te_hal_obj_alloc(&hal_filter,
						demux->hal_session,
						OBJECT_TYPE_FILTER,
						&params);
				if (result != 0) {
					pr_err("OF:%s couldn't allocate HAL filter\n",
							out_flt->name);
					goto error;
				}
				output->hal_filter = hal_filter;
			} else {
				te_hal_obj_inc(output->hal_filter);
				hal_filter = output->hal_filter;
			}

			/* Associate the filter to the slot */
			Error = stptiOBJMAN_AssociateObjects(hal_filter->hdl,
								hal_slot->hdl);
			if (ST_NO_ERROR != Error) {
				pr_err("couldn't associate hal filter to slot\n");
				result = te_hal_err_to_errno(Error);
				goto error;
			}
			pr_debug("OF:%s Associated HalFilter:0x%x to SlotHdl:0x%x\n",
					output->obj.name, hal_filter->hdl.word,
					 hal_slot->hdl.word);

			/* Call filterset and enable to activate the
			 * filter */
			Error = stptiHAL_call(Filter.HAL_FilterUpdate,
					hal_filter->hdl,
					stptiHAL_PES_STREAMID_FILTER,
					false, false, &stream_id,
					NULL, NULL);

			if (ST_NO_ERROR != Error) {
				pr_err("OF:%s Error calling HAL_FilterUpdate Error 0x%x\n",
						out_flt->name, Error);
				/* deallocate the objects - they will
				 * automatically break the links */
				result = te_hal_err_to_errno(Error);
				goto error;
			}

			Error = stptiHAL_call(Filter.HAL_FilterEnable, hal_filter->hdl, true);
			if (Error != ST_NO_ERROR) {
				pr_err("OF:%s HAL_FilterEnable Failed(0x%x)\n",
						out_flt->name,
						(unsigned int)Error);
				result = te_hal_err_to_errno(Error);
				goto error;
			}
		}

	}

/* PS To Check*/
	if (input->path_id) {
		Error = stptiHAL_call(Slot.HAL_SlotSetSecurePathID,
				hal_slot->hdl, input->path_id);
		if (ST_NO_ERROR != Error) {
			result = te_hal_err_to_errno(Error);
			goto error;
		}
	}

	pr_debug("OF:%s Associate SlotHdl:0x%x to BufHdl:0x%x\n",
		output->obj.name, hal_slot->hdl.word,
		output->hal_buffer->hdl.word);

	Error = stptiOBJMAN_AssociateObjects(hal_slot->hdl,
					     output->hal_buffer->hdl);

	if (ST_NO_ERROR != Error) {
		pr_err("stptiOBJMAN_AssociateObjects error 0x%x\n",
				Error);
		result = te_hal_err_to_errno(Error);
		goto error;
	}

	Error = stptiHAL_call(
			Slot.HAL_SlotSetPID,
			hal_slot->hdl,
			input->pid,
			false);

	if (Error != ST_NO_ERROR) {
		pr_err("HAL_SlotSetPID error 0x%x\n", Error);
		result = te_hal_err_to_errno(Error);
		goto error;
	}

	/* Register buffer workqueue function */
	if (output->external_connection != NULL) {
		result = te_bwq_register(demux->bwq, out_flt,
				output->hal_buffer,
				output->buffer_work_func);
		if (result != 0) {
			pr_err("OF:%s unable to register buffer work func\n",
					out_flt->name);
			goto error;
		}
	}

	pr_debug("#ATTACH << IF:%s <-> OF:%s\n", in_flt->name, out_flt->name);

	return result;

error:

	if (output->hal_buffer) {
		if (te_hal_obj_dec(output->hal_buffer))
			output->hal_buffer = NULL;
	}

	if (hal_slot)
		te_hal_obj_dec(hal_slot);

	if (output->hal_filter) {
		if (te_hal_obj_dec(output->hal_filter))
			output->hal_filter = NULL;
	}

	return result;
}

/*!
 * \brief Destroys the HAL objects created when the given pid and output
 * filters were attached
 *
 * \param out_flt TE Output filter to detach pid_filter_p from
 * \param in_flt TE PID filter object to detach
 *
 * \retval 0       Success
 * \retval -EINVAL Detachment of the specified filters is not supported
 */
int te_out_filter_disconnect_input(struct te_obj *out_flt,
		struct te_obj *in_flt)
{
	ST_ErrorCode_t Error = ST_NO_ERROR;
	stptiHAL_SlotConfigParams_t params;
	int j, num_objects;
	FullHandle_t index;
	struct te_hal_obj *slot = NULL, *tmp = NULL;
	U16 hal_slot_mode = 0;
	int err = 0;

	struct te_out_filter *output = te_out_filter_from_obj(out_flt);
	struct te_in_filter *input = te_in_filter_from_obj(in_flt);

	/* validate functions arguments */
	if (in_flt == NULL || out_flt == NULL) {
		pr_err("input or output filter is NULL\n");
		err = -EINVAL;
		goto done;
	} else if (in_flt->type != TE_OBJ_PID_FILTER
		&& in_flt->type != TE_OBJ_PID_INS_FILTER
		&& in_flt->type != TE_OBJ_PID_REP_FILTER) {
		pr_err("IF:%s input is not a PID filter\n", in_flt->name);
		err = -EINVAL;
		goto done;
	}

	if (in_flt->type == TE_OBJ_PID_INS_FILTER)
		return te_ins_filter_detach_out(in_flt, out_flt);

	if (in_flt->type == TE_OBJ_PID_REP_FILTER)
		return te_rep_filter_detach_out(in_flt, out_flt);

	memset(&params, 0, sizeof(stptiHAL_SlotConfigParams_t));

	/* Check to see if there are any HAL objects that need disassociating.
	 * Where there is a 1:1 mapping between hal objects
	 * (slots with buffers filters & signals etc)
	 * the HAL objects can be deallocated after disassociating.
	 * However if more than one association exists then we need to check
	 * it is safe to deallocate the HAL objects */

	switch (out_flt->type) {

		/* Objects that do not use their own slot all other filters do
		 * */
	case TE_OBJ_TS_INDEX_FILTER:
		index = output->hal_index->hdl;

		/* Find the RAW slot we are indexing on */
		list_for_each_entry(slot, &input->slots, entry) {

			Error = stptiHAL_call(Slot.HAL_SlotGetMode,
				slot->hdl,
				&hal_slot_mode);

			if (ST_NO_ERROR == Error
				&& hal_slot_mode == stptiHAL_SLOT_TYPE_RAW) {
				stptiOBJMAN_DisassociateObjects(slot->hdl,
					output->hal_index->hdl);
				te_hal_obj_dec(slot);
				break;
			}
		}

		if (te_hal_obj_dec(output->hal_index)) {
			output->hal_index = NULL;
			output->obj.state = TE_OBJ_STOPPED;
		}

		break;

		/* Objects where >1:1 associations are allowed */
	case TE_OBJ_TS_FILTER:
	case TE_OBJ_SECTION_FILTER:
	case TE_OBJ_PES_FILTER:
	case TE_OBJ_PCR_FILTER:
		/* Find out the slot type from the output filter object */
		err = te_out_filter_to_stpti_slot_type(out_flt,
							out_flt->type,
							&params.SlotMode);
		if (err != 0) {
			pr_err("unable to determine slot type\n");
			goto done;
		}

		if (out_flt->type == TE_OBJ_SECTION_FILTER) {
			te_section_filter_disconnect(out_flt, in_flt);
		} else {
			FullHandle_t *assoc_handles;
			assoc_handles = kmalloc(sizeof(FullHandle_t) *
					MAX_INT_CONNECTIONS, GFP_KERNEL);
			if (!assoc_handles) {
				pr_err("Failed to allocassoc handles array\n");
				err = -ENOMEM;
				goto done;
			}

			/* Get the list of slots attached to this buffer.
			 * Search the list until you find one that matches. */
			num_objects = stptiOBJMAN_ReturnAssociatedObjects(
					output->hal_buffer->hdl, assoc_handles,
					MAX_INT_CONNECTIONS, OBJECT_TYPE_SLOT);

			if (num_objects == 0) {
				pr_err("stptiOBJMAN_ReturnAssociatedObjects failed\n");
				kfree(assoc_handles);
				goto done;
			}

			list_for_each_entry_safe(slot, tmp, &input->slots, entry) {
				for (j = 0; j < num_objects; j++) {
					if (slot->hdl.word == assoc_handles[j].word) {
						te_hal_obj_dec(slot);
						break;
					}
				}
			}
			if (TE_OBJ_PID_FILTER == in_flt->type)
				te_pid_filter_update(in_flt);

			/* Delete HAL filter if it exists (e.g. PES stream id filter) */
			if (output->hal_filter) {
				if (te_hal_obj_dec(output->hal_filter)) {
					output->hal_filter = NULL;
				}
			}

			if (te_hal_obj_dec(output->hal_buffer)) {
				output->hal_buffer = NULL;
				output->buf_manual = false;
				output->obj.state = TE_OBJ_STOPPED;
			} else {
				/* Buffer still exists, check + flush */
				if (output->flushing_behaviour
					== STM_TE_FILTER_CONTROL_FLUSH_ON_DETACH) {
					output->flush(output);
				}
			}
			output->extra_bytes_per_out_pkt = 0;
			kfree(assoc_handles);
		}
		break;
	default:
		pr_err("unsupported output filter type %d\n", out_flt->type);
		err = -EINVAL;
		goto done;
	}

done:
	return err;
}

static int te_out_filter_pull_testfordata(stm_object_h filter_h, uint32_t *size)
{
	int err = 0;
	struct te_out_filter *output;
	int32_t sz;

	*size = 0;

	err = te_out_filter_from_hdl(filter_h, &output);
	if (err) {
		pr_err("Bad filter handle %p\n", filter_h);
		return err;
	}

	/* output->next_read_size can return a negative errno if interrupted by
	 * a signal*/
	sz = output->next_read_size(output);
	if (sz >= 0)
		*size = sz;
	else
		err = sz;

	return err;
}

static int te_out_filter_pull_data(stm_object_h filter_h,
		struct stm_data_block *data_block,
		uint32_t block_count,
		uint32_t *filled_blocks)
{
	int err = 0, ret = 0;
	struct te_out_filter *output;
	struct stm_data_block *cur_blk = data_block;
	uint8_t *cur_des = cur_blk->data_addr;
	int32_t total_read = 0, cur_read = 0;

	int32_t available_size = 0;
	int32_t req_size = 0;
	int32_t min_read_size;
	int i;
	struct te_demux *demux ;
	*filled_blocks = 0;

	/* Check handle */
	err = te_out_filter_from_hdl(filter_h, &output);
	if (err) {
		pr_err("Bad filter handle %p\n", filter_h);
		return err;
	}

	demux = te_demux_from_obj(output->obj.parent);
	if (demux == NULL)
		return -ENODEV;

	/* Determine total requested size */
	for (i = 0; i < block_count; i++)
		req_size += data_block[i].len;

	ret = mutex_lock_interruptible(&output->obj.lock);
	if (ret < 0)
		return ret;

	/* Check attachment status */
	if (output->external_connection) {
		te_hal_obj_inc(output->hal_buffer);
		mutex_unlock(&output->obj.lock);
	} else {
		pr_warn("Inavlid pull request on detached filter %s\n",
				output->obj.name);
		ret = -EPERM;
		mutex_unlock(&output->obj.lock);
		goto exit_no_ext_con;
	}

	if (STM_TE_FILTER_CONTROL_FLUSH_ON_OVERFLOW ==
			output->flushing_behaviour) {
		/* Flush buffer in case of overflow
		 * Return -EOVERFLOW
		 */

		if (true == output->overflow) {
			err = output->flush(output);
			if (err)
				pr_err("OF:%s flush on overflow failed(%d)\n",
						output->obj.name, err);
			return -EOVERFLOW;
		}
	}

	/* If the output filter is in quantisation unit reading mode we will
	 * return at most one quantisation unit, so regardless of req_size, the
	 * minimum read size is 1 byte.
	 * In byte-stream mode the minimum read size is the size of the buffer.
	 * This means that blocking pull requests will block until the whole
	 * buffer is filled. */
	if (output->read_quantisation_units)
		min_read_size = 1;
	else
		min_read_size = req_size;

	/* If output->next_read_size returns an error, it has been interrupted
	 * and we should return now */
	available_size = output->next_read_size(output);
	if (available_size < 0) {
		ret = available_size;
		goto out;
	}

	/* If we cannot satisfy the pull_request now force pull_work to happen
	 * now, so we have an up-to-date view of the output filter's buffer */
	if (available_size < min_read_size && output->hal_buffer) {
		output->buffer_work_func(&output->obj, output->hal_buffer);
		available_size = output->next_read_size(output);
		if (available_size < 0) {
			ret = available_size;
			goto out;
		}
	}


	/* If this output filter has enough data to satisty the pull request or
	 * we are in non-blocking mode populate the buffer
	 *
	 * Otherwise this task must wait until enough data becomes available
	 * (or the output filter is disconnected)
	 */
	pr_debug("Output %p >> ReadMode:%d total req %d bytes, total available %d\n",
			output, output->pull_intf.mode,
			req_size, available_size);

	if (!(output->pull_intf.mode & STM_IOMODE_NON_BLOCKING_IO)) {
		while (min_read_size > 0 &&
				output->external_connection != NULL) {
			int wait_err;

			cur_read = te_out_filter_copy_to_usr_buf(
					output, &cur_blk, &cur_des,
					block_count, filled_blocks, req_size);
			if (cur_read < 0) {
				ret = cur_read;
				goto out;
			}

			total_read += cur_read;
			min_read_size -= cur_read;

			ret = update_pacing_info_for_pull(output, cur_read);
			if (ret < 0)
				goto out;

			/*check if previous read fulfills the pull request
			 * before going to sleep
			 * */
			if (min_read_size <= 0)
				break;

			wait_err = wait_event_interruptible(
					output->reader_waitq,
					output->external_connection == NULL
					|| (output->next_read_size(output) > 0));
			if (wait_err || output->external_connection == NULL) {
				pr_warn("Pull request on %s interrupted\n",
						output->obj.name);
				ret = total_read;
				goto out;
			}
			/* Update requested size with last read*/
			req_size -= cur_read;
		}
	} else {

		cur_read = te_out_filter_copy_to_usr_buf(
				output, &cur_blk, &cur_des,
				block_count, filled_blocks, req_size);
		if (cur_read < 0) {
			ret = cur_read;
			goto out;
		}

		total_read += cur_read;
		min_read_size -= cur_read;

		ret = update_pacing_info_for_pull(output, cur_read);
		if (ret < 0)
			goto out;
	}


	 ret = total_read;
out:
	pr_debug("Output %p << Err:%d total req %d Pages %d Bytes, total read %d pages, %d bytes\n",
			output, ret, block_count, req_size,
			*filled_blocks, total_read);

	if (ret < 0 && total_read > 0)
		ret = total_read;

	if (te_hal_obj_dec(output->hal_buffer)) {
		output->hal_buffer = NULL;
		output->buf_manual = false;
		output->obj.state = TE_OBJ_STOPPED;
	}

exit_no_ext_con:

	return ret;
}

stm_data_interface_pull_src_t stm_te_pull_byte_interface = {
	te_out_filter_pull_data,
	te_out_filter_pull_testfordata,
	.user_copy_supported = true,
};

/*!
 * \brief Gets stm_te control data for an stm_te output filter object
 *
 * \param filter  output filter object to interrogate
 * \param control Control to get
 * \param value Set with the current value of the control
 *
 * \retval 0       Success
 * \retval -ENOSYS Invalid control coptrol for this output filter
 */
int te_out_filter_get_control(struct te_obj *filter, uint32_t control,
				void *buf, uint32_t size)
{
	int err = 0;
	struct te_out_filter *output = te_out_filter_from_obj(filter);

	switch (control) {
	case STM_TE_OUTPUT_FILTER_CONTROL_BUFFER_SIZE:
		/* Return actual allocated buffer size */
		err = GET_CONTROL(output->buf_aligned_size, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_OVERFLOW_BEHAVIOUR:
		err = GET_CONTROL(output->overflow_behaviour, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_ERROR_RECOVERY:
		err = GET_CONTROL(output->error_recovery, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		err = GET_CONTROL(output->flushing_behaviour, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS:
		err = GET_CONTROL(output->read_quantisation_units, buf, size);
		break;
	case __deprecated_STM_TE_FILTER_CONTROL_STATUS:
	case STM_TE_OUTPUT_FILTER_CONTROL_STATUS:
		err = te_out_filter_get_stat(filter,
					(stm_te_output_filter_stats_t *) buf);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_PACING:
		err = GET_CONTROL(output->pacing, buf, size);
		break;
	default:
		err = -ENOSYS;
	}

	return err;
}

static int te_out_filter_set_overflow_control(struct te_out_filter *filter)
{
	ST_ErrorCode_t hal_err;
	int err = 0;

	/* Grab the object's lock to prevent other threads updating output
		buffer overflow control */
	err = mutex_lock_interruptible(&filter->obj.lock);
	if (err < 0)
		return err;

	/* Set the overflow control for HAL buffer */
	if (filter->hal_buffer) {
		te_hal_obj_inc(filter->hal_buffer);
		hal_err = stptiHAL_call(Buffer.HAL_BufferSetOverflowControl,
				filter->hal_buffer->hdl,
				filter->overflow_behaviour);
		if (ST_NO_ERROR != hal_err) {
			pr_err("HAL_BufferSetOverflowControl return 0x%x\n",
				hal_err);
			err = te_hal_err_to_errno(hal_err);
		}
		te_hal_obj_dec(filter->hal_buffer);
	} else {
		err = -ENOSYS;
		pr_err("There is no allocated hal_buffer 0x%x\n", err);
	}
	mutex_unlock(&filter->obj.lock);

	return err;
}

/*!
 * \brief Sets stm_te control data for an stm_te output filter object
 *
 * \param filter  output filter object to update
 * \param control Control to set
 * \param value New value of the control
 *
 * \retval 0       Success
 * \retval -ENOSYS Invalid control control for this output filter
 */
int te_out_filter_set_control(struct te_obj *filter, uint32_t control,
				const void *buf, uint32_t size)
{
	int err = 0;
	unsigned int val;
	struct  te_out_filter *output = NULL;

	if (NULL == buf)
		return -EINVAL;

	output = te_out_filter_from_obj(filter);

	switch (control) {
	case STM_TE_OUTPUT_FILTER_CONTROL_BUFFER_SIZE:
		if (filter->state == TE_OBJ_STARTED) {
			pr_err("Unable to change buffer size, output filter is already attached\n");
			err = -EINVAL;
		} else if (*((int *)buf) < DVB_PAYLOAD_SIZE) {
			pr_err("Buffer size should be atleast equal to DVB payload size i.e. 184 bytes\n");
			err = -EINVAL;
		} else
			err = SET_CONTROL(output->buf_size, buf, size);

		if (!err)
			pr_info("Filter:%s New BufSize:%d bytes\n",
					filter->name, output->buf_size);
		else
			pr_err("Filter:%s Failed(0x%x) to set buffer size:%d\n",
					filter->name, err, *((uint32_t *)buf));


		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_READ_IN_QUANTISATION_UNITS:
		err = SET_CONTROL(val, buf, size);
		if (!err)
			output->read_quantisation_units = (val != 0);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_OVERFLOW_BEHAVIOUR:
		err = SET_CONTROL(output->overflow_behaviour, buf, size);
		if (err == 0)
			err = te_out_filter_set_overflow_control(output);
		break;

	case STM_TE_OUTPUT_FILTER_CONTROL_ERROR_RECOVERY:
		/* Setting these values is currently unsupported */
		err = -ENOSYS;
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		err = SET_CONTROL(output->flushing_behaviour, buf, size);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSH:
		err = output->flush(output);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_REQUEST_FLUSH:
		err = output->request_flush(output);
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_PAUSE:
		if (*(int *)buf == TE_PAUSE && filter->state !=
						TE_OBJ_STOPPED) {
			filter->state = TE_OBJ_PAUSED;
		} else if (*(int *)buf == TE_RESUME && filter->state !=
						TE_OBJ_STOPPED) {
			filter->state = TE_OBJ_STARTED;
		} else {
			pr_err("Invalid set control request - buf val %d"
				" filter_state %d\n", *(int *)buf,
						filter->state);
			err = -EINVAL;
		}
		break;
	case STM_TE_OUTPUT_FILTER_CONTROL_PACING:
		err = SET_CONTROL(val, buf, size);
		if (!err)
			output->pacing = (val != 0);
		break;
	default:
		err = -ENOSYS;
	}

	return err;
}

static int __filter_flush(struct te_out_filter *filter)
{
	ST_ErrorCode_t hal_err;
	struct te_demux *dmx = te_demux_from_obj(filter->obj.parent);
	int err = 0;

	/* Flush the HAL buffer */
	pr_debug("#FLUSHOF OF:%s >> Actual Flush\n", filter->obj.name);
	if (filter->hal_buffer) {
		te_hal_obj_inc(filter->hal_buffer);

		hal_err = stptiHAL_call(Buffer.HAL_BufferFlush,
				filter->hal_buffer->hdl);
		if (ST_NO_ERROR != hal_err) {
			pr_err("BufferFlush return 0x%x\n", hal_err);
			err = te_hal_err_to_errno(hal_err);
		}

		hal_err = stptiHAL_call(Buffer.HAL_BufferGetWriteOffset,
				filter->hal_buffer->hdl, &filter->wr_offs);
		if (ST_NO_ERROR != hal_err) {
			pr_err("BufferGetWriteOffset return 0x%x\n", hal_err);
			err = te_hal_err_to_errno(hal_err);
		}


		if (!err) {
			filter->rd_offs = filter->wr_offs;
			stptiHAL_call(Buffer.HAL_BufferSetReadOffset,
					filter->hal_buffer->hdl,
					filter->rd_offs);
		}

		get_free_space_info(filter, filter->hal_buffer);
		/* Reset overflow */
		filter->overflow = false;

		te_hal_obj_dec(filter->hal_buffer);
	}

	if (filter->pacing == true && atomic_read(&filter->dp_set)) {
		filter->data_in_buf = 0;
		te_demux_dec_dp_cnt(dmx, filter);
		atomic_dec(&filter->dp_set);
	}
	if (filter->buf_monitoring == true)
		te_demux_notify_src(dmx, &filter->obj, filter->free_space);

	pr_debug("#FLUSHOF OF:%s << Actual Flush\n", filter->obj.name);

	return err;
}

static int te_out_filter_flush(struct te_out_filter *filter)
{
	int err = 0;

	pr_debug("#FLUSHOF OF:%s >> Blocking Flush\n", filter->obj.name);

	/* Grab the object's lock to prevent other threads updating output
	 * rd/wr offset */
	err = mutex_lock_interruptible(&filter->obj.lock);
	if (err < 0)
		return err;

	err = __filter_flush(filter);

	mutex_unlock(&filter->obj.lock);
	pr_debug("#FLUSHOF OF:%s << Blocking Flush\n", filter->obj.name);

	return err;
}


static int te_out_filter_request_flush(struct te_out_filter *filter)
{
	int err = 0;

	pr_debug("#FLUSHOF OF:%s >> NonBlocking Flush\n", filter->obj.name);
	/* Grab the object's lock to prevent other threads updating output
	 *          * rd/wr offset */
	err = mutex_trylock(&filter->obj.lock);
	if (!err)
		return -EAGAIN;

	err = __filter_flush(filter);

	mutex_unlock(&filter->obj.lock);
	pr_debug("#FLUSHOF OF:%s << NonBlocking Flush\n", filter->obj.name);

	return err;
}


/*!
 * \brief stm_te output filter object constructor
 *
 * Initialises a new output filter.r default values
 *
 * \param filter pointer to the output filter to initialise
 * \param demux  pointer to the TE obj of the parent demux for this output
 *               filter
 * \param name   String name for this output filter
 * \param type   Type of this output filter
 *
 * \retval 0       Success
 * \retval -ENOMEM No memory for new object
 */
int te_out_filter_init(struct te_out_filter *filter, struct te_obj *demux_obj,
		char *name, enum te_obj_type_e type)
{
	int err = 0;
	struct te_demux *demux;
	u_long flags = 0;

	demux = te_demux_from_obj(demux_obj);
	if (!demux) {
		pr_err("Bad demux handle\n");
		return -EINVAL;
	}

	err = te_obj_init(&filter->obj, type, name, demux_obj,
			&te_global.out_filter_class);
	if (err)
		goto error;

	/* Add output filter registry attributes */
	err = te_sysfs_entries_add(&filter->obj);
	if (err) {
		pr_err("Failed to add sysfs entries for filter %p %s\n", filter,
			name);
		err = -ENOMEM;
		goto error;
	}

	err = stm_registry_add_attribute(&filter->obj,
			TE_SYSFS_NAME_HAL_HDL "buffer",
			TE_SYSFS_TAG_HAL_HDL,
			&filter->hal_buffer,
			sizeof(struct te_hal_obj *));
	if (err) {
		pr_err("Failed to add HAL buffer handle to obj %s (%d)\n",
				name, err);
		goto error;
	}
	err = stm_registry_add_attribute(&filter->obj,
			TE_SYSFS_NAME_HAL_HDL "filter",
			TE_SYSFS_TAG_HAL_HDL,
			&filter->hal_filter,
			sizeof(struct te_hal_obj *));
	if (err) {
		pr_err("Failed to add HAL filter handle to obj %s (%d)\n",
				name, err);
		goto error;
	}

	err = stm_registry_add_attribute(&filter->obj,
			TE_SYSFS_NAME_HAL_HDL "index",
			TE_SYSFS_TAG_HAL_HDL,
			&filter->hal_index,
			sizeof(struct te_hal_obj *));
	if (err) {
		pr_err("Failed to add HAL index handle to obj %s (%d)\n",
				name, err);
		goto error;
	}

	/*
	 * Initialise output filter fields with non-zero defaults to default
	 * values
	 */
	filter->flushing_behaviour  = TE_DEFAULT_OUTPUT_FILTER_FLUSHING;
	filter->overflow_behaviour = TE_DEFAULT_OUTPUT_FILTER_OVERFLOW;
	filter->buf_size = 0;

	/* Set output filter pull src interface */
	filter->pull_src_intf = &stm_te_pull_byte_interface;

	/* Set default read_bytes and next_read_size functions
	 * Sub-classes of output filter may override these */
	filter->read_bytes = &te_out_filter_read_bytes;
	filter->next_read_size = &te_out_filter_next_read_size;

	filter->copy_function = &te_out_filter_memcpy;

	/* Set default buffer work function */
	filter->buffer_work_func = &te_out_filter_work;

	INIT_LIST_HEAD(&filter->queued_data);
	INIT_LIST_HEAD(&filter->in_filters);
	filter->total_queued_data = 0;
	filter->max_queued_data = te_cfg.max_queued_output_data;
	filter->pacing = false;
	filter->buf_monitoring = false;
	mutex_init(&filter->queue_lock);

	init_waitqueue_head(&filter->reader_waitq);

	write_lock_irqsave(&demux->out_flt_rw_lock, flags);
	list_add_tail(&filter->obj.lh, &demux->out_filters);
	write_unlock_irqrestore(&demux->out_flt_rw_lock, flags);

	/* Set default object functions */
	filter->flush = &te_out_filter_flush;
	filter->request_flush = &te_out_filter_request_flush;
	filter->extra_bytes_per_out_pkt = 0;

	return 0;

error:
	te_obj_deinit(&filter->obj);
	return err;
}

int te_out_filter_deinit(struct te_out_filter *filter)
{
	int err;
	struct te_demux *dmx = te_demux_from_obj(filter->obj.parent);
	u_long flags = 0;

	err = te_sysfs_entries_remove(&filter->obj);
	if (err)
		pr_warn("Failed to remove sysfs entries\n");

	err = te_obj_deinit(&filter->obj);
	if (err)
		return err;

	/* Delete any queued data */
	te_out_filter_queue_empty(filter);
	mutex_destroy(&filter->queue_lock);
	write_lock_irqsave(&dmx->out_flt_rw_lock, flags);
	list_del(&filter->obj.lh);
	write_unlock_irqrestore(&dmx->out_flt_rw_lock, flags);

	return 0;
}

/*!
 * \brief object to output filter cast
 */
struct te_out_filter *te_out_filter_from_obj(struct te_obj *filter)
{
	return container_of(filter, struct te_out_filter, obj);
}

/*!
 * \brief Adds a single data unit to the tail of the queued data for this
 * output filter
 *
 * \param output output filter to discard data from
 * \param data   Data buffer to queue (will be copied by this function)
 * \param size   Size of data
 * \param bool   Validity flag stored with data. Used for recording section crc
 *               validity
 *
 * \retval 0       Success
 * \retval -ENOMEM Allocation failed, or too much data already stored for this
 *                 object
 * \retval -EINTR  Interrupted by signal
 */
int te_out_filter_queue_data(struct te_out_filter *output, uint8_t *data,
		uint32_t size, bool valid)
{
	int err;
	struct te_queued_data *q_data;

	err = mutex_lock_interruptible(&output->queue_lock);
	if (err < 0)
		return err;

	if (output->total_queued_data + size >
			output->max_queued_data) {
		pr_warn_ratelimited("Obj:%s data queue overrun\n",
				output->obj.name);
		err = -ENOMEM;
		goto out;
	}

	q_data = kmalloc(sizeof(*q_data) + size, GFP_KERNEL);
	if (!q_data) {
		err = -ENOMEM;
		goto out;
	}

	q_data->data = (uint8_t *)&q_data[1];
	q_data->size = size;
	q_data->valid = valid;
	memcpy(q_data->data, data, size);

	list_add_tail(&q_data->lh, &output->queued_data);
	output->total_queued_data += size;
	output->lifetime_queued_data += size;

out:
	mutex_unlock(&output->queue_lock);
	return err;
}

/*!
 * \brief Reads data from the head of the queued data for an output filter
 *
 * \remark When the output filter is in quantisation unit read mode this
 * function will either read one complete data unit or as much of that data
 * unit as will fit in the supplied buffer (whatever is smallest)
 *
 * \remark When the output filter is in bytestream read mode this function will
 * read as much data from the queue aswill fit in the supplied buffer
 *
 * \param output      output filter to discard data from
 * \param buffer      Buffer to read the data into
 * \param buffer_size Size of buffer
 * \param bytes_read  Returned size of data read
 * \param valid       Whether the data returned was marked as valid (normally
 *                    refers to section crc check). In bytestream mode this
 *                    indicates that ALL the data read came from valid
 *                    packets/sections.
 *
 * \retval 0      Success
 * \retval -EINTR Interrupted by signal
 */
int te_out_filter_queue_read(struct te_out_filter *output, uint8_t *buffer,
		uint32_t buffer_size, uint32_t *bytes_read, bool *valid)
{
	int err;
	struct te_queued_data *q, *q2;
	enum stm_data_mem_type mem_type;
	bool user_copy_if;

	*valid = true;
	*bytes_read = 0;

	err = mutex_lock_interruptible(&output->queue_lock);
	if (err < 0)
		return err;

	mem_type = output->pull_intf.mem_type;
	user_copy_if = output->pull_src_intf->user_copy_supported;

	if (output->read_quantisation_units) {
		if (list_empty(&output->queued_data))
			goto out;

		q = list_first_entry(&output->queued_data,
				struct te_queued_data, lh);

		*bytes_read = min(buffer_size, q->size);
		*valid = q->valid;
		if (mem_type == USER && user_copy_if == true) {
			err = te_out_filter_user_memcpy((void **)&buffer,
					q->data, *bytes_read);
			if (err != ST_NO_ERROR) {
				*bytes_read = 0;
				err = te_hal_err_to_errno(err);
				goto out;
			}
		} else {
			memcpy(buffer, q->data, *bytes_read);
		}

		q->size -= *bytes_read;
		q->data += *bytes_read;
		output->total_queued_data -= *bytes_read;
		if (q->size <= 0) {
			list_del(&q->lh);
			kfree(q);
		}
	} else {
		list_for_each_entry_safe(q, q2, &output->queued_data, lh) {
			uint32_t bytes_this_iter;

			/* Read data from this queue entry into read buffer */
			bytes_this_iter = min(buffer_size, q->size);
			*valid &= q->valid;
			if (mem_type == USER && user_copy_if == true) {
				err = te_out_filter_user_memcpy(
						(void **)&buffer, q->data,
						bytes_this_iter);
				if (err != ST_NO_ERROR) {
					bytes_this_iter = 0;
					err = te_hal_err_to_errno(err);
					goto out;
				}
			} else {
				te_out_filter_memcpy((void **)&buffer,
						q->data,
						bytes_this_iter);
			}

			/* Update queue entry. Discard this queue entry if all
			 * data has been read from it */
			q->size -= bytes_this_iter;
			q->data += bytes_this_iter;
			output->total_queued_data -= bytes_this_iter;
			if (q->size <= 0) {
				list_del(&q->lh);
				kfree(q);
			}

			/* Update read buffer pointer */
			*bytes_read += bytes_this_iter;
			buffer_size -= bytes_this_iter;
			if (buffer_size <= 0)
				break;
		}
	}
out:
	mutex_unlock(&output->queue_lock);
	return err;
}

/*!
 * \brief Discards any queued data for this output filter
 *
 * \param output  output filter to discard data from
 *
 * \retval 0      Success
 * \retval -EINTR Interrupted by signal
 */
int te_out_filter_queue_empty(struct te_out_filter *output)
{
	struct te_queued_data *q, *q2;
	int err;

	err = mutex_lock_interruptible(&output->queue_lock);
	if (err)
		return err;

	list_for_each_entry_safe(q, q2, &output->queued_data,
			lh) {
		list_del(&q->lh);
		kfree(q);
	}
	output->total_queued_data = 0;
	mutex_unlock(&output->queue_lock);

	return err;
}

/*!
 * \brief Wait until the specified output filter's buffer has at least 25%
 * space
 * \param obj TE obj for output filter to wait upon
 *
 * \retval 0       Success
 * \retval -EINVAL Output filter is not valid
 * \retval -EIO    HAL error
 * \retval -EINTR  Interrupted by signal
 * \retval -EWOULDBLOCK Output filter is not started or has not connections
 */
int te_out_filter_wait(struct te_obj *obj)
{
	int err = 0;
	struct te_out_filter *filter = te_out_filter_from_obj(obj);
	ST_ErrorCode_t hal_err;
	struct te_hal_obj *hal_buffer = NULL;
	uint32_t buffer_size, bytes_in_buffer, units_in_buffer, free_space;
	struct te_demux *dmx;

	buffer_size = bytes_in_buffer = units_in_buffer = free_space = 0;

	/* Check if the pacing filter is valid whilst holding the global lock.
	 * If it is, retrieve a reference to the HAL buffer handle */
	err = mutex_lock_interruptible(&obj->lock);
	if (err < 0)
		return err;

	err = te_valid_hdl(te_obj_to_hdl(obj));
	if (err) {
		pr_warn("Pacing filter handle %p is invalid\n", obj);
	} else if (!filter->hal_buffer) {
		pr_warn("Pacing filter %s is not started\n", obj->name);
		err = -EINVAL;
	} else if (!filter->external_connection) {
		pr_warn("Pacing filter %s has no downstream connections\n",
				obj->name);
		err = -EINVAL;
	} else {
		te_hal_obj_inc(filter->hal_buffer);
		hal_buffer = filter->hal_buffer;
	}
	mutex_unlock(&obj->lock);

	if (err)
		return err;

	dmx = te_demux_from_obj(obj->parent);
	if (!dmx)
		return -EINVAL;

	/* If we got here without error, we have a valid HAL buffer to wait on */
	while (!err) {
		hal_err = stptiHAL_call(Buffer.HAL_BufferStatus,
				hal_buffer->hdl,
				&buffer_size, &bytes_in_buffer,
				&units_in_buffer, &free_space, NULL, NULL, NULL);
		if (ST_NO_ERROR != hal_err) {
			pr_err("HAL_BufferStatus return 0x%x\n", hal_err);
			err = te_hal_err_to_errno(hal_err);
			break;
		}

		/* If buffer has at one page size left full break, else wait */
		if ((buffer_size - bytes_in_buffer) >= PAGE_SIZE)
			break;

		/* Wait for buffer to become empty */
		set_current_state(TASK_INTERRUPTIBLE);
		if (signal_pending(current) ||
				dmx->disconnect_pending ||
				schedule_timeout(msecs_to_jiffies(30))) {
			err = -EINTR;
			pr_info("TE inject pacing interrupted\n");
		}
	}
	if (hal_buffer)
		if (te_hal_obj_dec(hal_buffer)) {
			/* If this was the last reference to the HAL buffer, we
			 * need to check if the output filter still exists and
			 * if so we can set the HAL buffer pointer to NULL */
			err = mutex_lock_interruptible(&obj->lock);
			if (err < 0)
				return err;
			if (0 == te_valid_hdl(te_obj_to_hdl(obj)) &&
					filter->hal_buffer == hal_buffer) {
				filter->hal_buffer = NULL;
				filter->buf_manual = false;
				filter->obj.state = TE_OBJ_STOPPED;
			}
			mutex_unlock(&obj->lock);
		}

	return err;
}


int te_out_filter_get_sizes(struct te_obj *obj, uint8_t dmx_pkt_size,
				unsigned int *bytes_in_buffer,
				unsigned int *free_space)
{
	struct te_out_filter *filter = te_out_filter_from_obj(obj);
	struct te_demux *dmx = te_demux_from_obj(obj->parent);
	int err = 0;
	uint32_t total_size = 0;

	err = te_bwq_get_work_status(dmx->bwq, filter->hal_buffer, free_space);
	if (err < 0) {
		pr_debug("Error while reading hal buffer status for out filter 0x%p hal buf %p\n",
					filter, filter->hal_buffer);
		return err;
	}

	*bytes_in_buffer = err;
	total_size = filter->buf_aligned_size;

	/* Adjust free_space for any extra_bytes generated in output
	 * buffer per output pkt. This adjustment should be done only
	 * when output pkt size is bigger than input pkt size*/
	if ((filter->extra_bytes_per_out_pkt) &&
			IN_PKT_HAS_NO_EXTRA_BYTES(dmx_pkt_size))
		*free_space -= ((*free_space / dmx_pkt_size)
				* (filter->extra_bytes_per_out_pkt));

	return 0;
}

int te_out_filter_send_event(struct te_out_filter *output,
		stm_te_filter_event_t event)
{
	int ret = 0;
	stm_event_t evt;

	evt.event_id = event;
	evt.object = &output->obj;

	ret = stm_event_signal(&evt);
	if (ret)
		pr_err("OutputFilter:0x%p failed(%d) to signal event(0x%x)\n",
				output, ret, event);
	else
		pr_debug("OutputFilter:0x%p event(0x%x) signalled\n",
				output, event);
	return ret;
}


int te_out_filter_notify_pull_sink(struct te_out_filter *output,
				stm_memsink_event_t event)
{
	int ret = 0;

	if (!output->pull_intf.notify || !output->external_connection) {
		pr_debug("Output filter 0x%p has no resource to notify source (%p, %p)\n",
			output, output->pull_intf.notify, output->external_connection);
		goto skip_event;
	}

	ret = output->pull_intf.notify(output->external_connection, event);
	if (ret)
		pr_err_ratelimited("Notify sink %p failed (%d)\n",
				output->external_connection, ret);

skip_event:
	return ret;
}


int te_out_filter_check_pacing(struct te_out_filter *filter)
{
	if (!filter->external_connection || filter->pacing != true)
		return 0;
	else
		return 1;
}


int te_out_filter_copy_to_usr_buf(struct te_out_filter *of,
		struct stm_data_block **blk, uint8_t **cur_des,
		uint32_t blk_cnt, uint32_t *filled, uint32_t max_read)
{
	int err = 0;
	int available = 0, read_size = 0;
	int total_read = 0, space_in_blk = 0;
	int cur_len = 0;
	int bytes_read = 0, read_in_blk = 0;
	struct stm_data_block *cur_blk = *blk;

	available = of->next_read_size(of);
	if (available <= 0) {
		err = available;
		goto out;
	}

	read_size = min_t(uint32_t, available, max_read);

	/* If the HAL buffer is allocated outside TE then we just need to
	 *  * update the read pointer*/
	if (of->buf_manual) {
		bytes_read = of->rd_offs + read_size;
		if (bytes_read >= of->buf_size)
			bytes_read = (of->rd_offs + read_size) - of->buf_size;
		of->rd_offs = bytes_read;
		pr_debug("OF:%s External TP buffer buf_hdl:0x%x BytesRead:%d New RdOffset:%d\n",
				of->obj.name, of->hal_buffer->hdl.word,
				read_size, of->rd_offs);
		stptiHAL_call(Buffer.HAL_BufferSetReadOffset,
				of->hal_buffer->hdl, of->rd_offs);
		return read_size;
	}

	space_in_blk = cur_blk->len -
		(*cur_des - (uint8_t *)(cur_blk->data_addr));
	cur_len = space_in_blk;

	pr_debug("OF:%s >> Reading %d bytes (%d requested, %d available)\n",
			of->obj.name, read_size, max_read, available);

	while (!err  && read_size > 0) {
		bytes_read = 0;
		err = of->read_bytes(of, *cur_des,
				min_t(uint32_t, space_in_blk, read_size),
				&bytes_read);

		if (err && 0 == bytes_read)
			goto out;

		total_read += bytes_read;
		read_in_blk += bytes_read;
		if (read_in_blk == cur_len) {
			(*filled)++;
			if ((*filled) == blk_cnt)
				break;
			cur_blk = cur_blk->next;
			*cur_des = cur_blk->data_addr;
			space_in_blk = cur_blk->len;
			read_in_blk = 0;
			cur_len = space_in_blk;
		} else {
			*cur_des += bytes_read;
			space_in_blk = cur_blk->len - read_in_blk;
		}
		read_size -= bytes_read;
	}

	err = total_read;
out:
	pr_debug("OF:%s << Err:%d Req %d pages %d bytes, Read %d pages %d bytes\n",
			of->obj.name, err, blk_cnt, max_read, *filled,
			total_read);

	if (err < 0 && total_read > 0)
		err = total_read;

	*blk = cur_blk;
	return err;
}

static int update_pacing_info_for_pull(struct te_out_filter *of,
		uint32_t last_read)
{
	struct te_demux *dmx = te_demux_from_obj(of->obj.parent);

	if (dmx == NULL)
		return -ENODEV;

	if (of->pacing == true) {
		obj_lock(&of->obj);
		get_free_space_info(of, of->hal_buffer);
		if (atomic_read(&of->dp_set)) {
			of->data_in_buf -= last_read;
			if (of->data_in_buf <= 0) {
				te_demux_dec_dp_cnt(dmx, of);
				atomic_dec(&of->dp_set);
			}
		}
		obj_unlock(&of->obj);
		pr_debug("OF:%s is pacing DatInBuf:%d\n",
				of->obj.name, of->data_in_buf);
	}
	if (of->buf_monitoring == true)
		te_demux_notify_src(dmx, &of->obj, of->free_space);

	return 0;
}

int te_out_filter_set_prop(struct te_obj *obj, uint32_t prop, uint32_t value)
{
	int err = 0;
	struct te_out_filter *output = NULL;

	output = te_out_filter_from_obj(obj);

	switch (prop) {
	case TE_OUT_FILTER_PROP_USE_TP_TIMER:
		pr_debug("OF:%s time property is not implemented at output filter level\n",
				obj->name);
		err = -ENOSYS;
		break;

	default:
		pr_err("OF:%s invalid property: %d\n", obj->name, prop);
		err = -EINVAL;
	}

	return err;
}

int te_out_filter_get_prop(struct te_obj *obj, uint32_t prop, uint32_t *value_p)
{
	int err = 0;
	struct te_out_filter *output = NULL;

	output = te_out_filter_from_obj(obj);

	switch (prop) {
	case TE_OUT_FILTER_PROP_USE_TP_TIMER:
		pr_debug("OF:%s time property is not implemented at output filter level\n",
				obj->name);
		err = -ENOSYS;
		break;

	default:
		pr_err("OF:%s invalid property: %d\n", obj->name, prop);
		err = -EINVAL;
	}

	return err;
}
