/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_psdemux.h

Declares TE PS demux type and operators
******************************************************************************/
#ifndef TE_PSDEMUX_H
#define TE_PSDEMUX_H

#include <linux/list.h>
#include <linux/semaphore.h>
#include <linux/wait.h>
#include "te_object.h"
#include <stm_data_interface.h>

#include "te_ps_utils.h"
#include "ps_parser.h"

/* To decrement / increment data process pending variable */
enum te_psdmx_dp_mode {
	PSDMX_INC_DP_COUNT,
	PSDMX_DEC_DP_COUNT,
};

struct te_psdemux {
	uint8_t	sys_hdr_stream_id;
	uint32_t	program_mux_rate;

	/* Child filters */
	struct list_head stream_filters;
	struct list_head out_filters;

	rwlock_t out_flt_rw_lock;

	struct workqueue_struct *work_que;
	struct te_ps_buffer_work_queue *bwq;

	/* Upstream connected object */
	stm_object_h upstream_obj;
	stm_data_interface_push_notify_t push_notify;
	stm_data_interface_push_sink_t sink_interface;

	/* Upstream disconnection pending */
	bool disconnect_pending;

	/* Connections to other object and interfaces */
	stm_object_h external_connection;

	/* message queue for sending message to work queue */
	struct list_head ps_msg_list;
	/* mutex for linked list protection */
	struct mutex ps_msg_mutex;
	ps_os_semaphore_t sema;

	ps_parser_data_t *parser_p;
	/* The API object variables */
	struct te_obj obj;

	wait_queue_head_t pacing_waitq;

	atomic_t data_process_pending;

};

struct te_psdemux *te_psdemux_from_obj(struct te_obj *obj);
int te_psdemux_new(char *name, struct te_obj **new_psdemux);
int te_psdemux_start(struct te_obj *obj);
int te_psdemux_stop(struct te_obj *obj);

int te_psdemux_update_data_process_cnt(struct te_psdemux *psdemux, uint8_t val);
#endif
