/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_pes_filter.h

Defines PES filter specific operations
******************************************************************************/

#ifndef __TE_PS_PES_FILTER_H
#define __TE_PS_PES_FILTER_H

#include "te_object.h"
#include "te_ps_output_filter.h"

int te_ps_pes_filter_new(struct te_obj *psdmx, struct te_obj **new_filter);

#endif
