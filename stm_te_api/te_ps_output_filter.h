/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_output_filter.h

Declares te output filter-specific functions and types
******************************************************************************/

#ifndef __TE_PS_OUT_FILTER_H
#define __TE_PS_OUT_FILTER_H

#include <linux/wait.h>
#include "te_object.h"
#include <stm_data_interface.h>
#include <stm_se.h>
#include "te_ps_buffer_work.h"

struct te_ps_out_filter {
	struct te_obj obj;

	struct te_buffer_obj *buffer_obj;
	/* used to store user defined buffer size value */
	int   buf_size;
	/* Function to get the next read size for this filter */
	int32_t (*next_read_size)(struct te_ps_out_filter *of);

	/* Function to read data from this filter */
	int (*read_bytes)(struct te_ps_out_filter *of, uint8_t *buf,
			uint32_t size, uint32_t *bytes_read);

	/* Buffer workqueue function */
	TE_PS_BWQ_WORK buffer_work_func;

	unsigned int overflow_behaviour;
	unsigned int flushing_behaviour;
	unsigned int error_recovery;
	bool pacing;
	bool read_quantisation_units;
	atomic_t dp_set;

	stm_object_h external_connection;
	wait_queue_head_t reader_waitq;

	stm_data_interface_pull_sink_t pull_intf;
	stm_data_interface_push_sink_t push_intf;
	stm_se_clock_data_point_interface_push_sink_t scr_push_intf;

	stm_data_interface_pull_src_t *pull_src_intf;

	int (*flush)(struct te_ps_out_filter *);
};
int te_ps_out_filter_get_sizes(struct te_obj *obj, unsigned int *total_size,
				unsigned int *bytes_in_buffer,
				unsigned int *free_space);
int32_t te_ps_out_filter_next_read_size(struct te_ps_out_filter *output);
int te_ps_out_filter_read_bytes(struct te_ps_out_filter *output,
		uint8_t *buf,
		uint32_t buf_size, uint32_t *bytes_read);

struct te_ps_out_filter *te_ps_out_filter_from_obj(struct te_obj *filter);
int te_ps_out_filter_from_hdl(stm_object_h hdl, struct te_ps_out_filter **obj);

int te_ps_out_filter_init(struct te_ps_out_filter *filter, struct te_obj *demux,
		char *name, enum te_obj_type_e type);
int te_ps_out_filter_deinit(struct te_ps_out_filter *filter);

int te_ps_out_filter_attach(struct te_obj *filter, stm_object_h target);
int te_ps_out_filter_detach(struct te_obj *filter, stm_object_h target);

int te_ps_out_filter_get_control(struct te_obj *filter, uint32_t control,
				void *buf, uint32_t size);

int te_ps_out_filter_set_control(struct te_obj *filter, uint32_t control,
				const void *buf, uint32_t size);

int te_ps_out_filter_connect_input(struct te_obj *out_flt,
		struct te_obj *in_flt);
int te_ps_out_filter_disconnect_input(struct te_obj *out_flt,
		struct te_obj *in_flt);

void te_ps_out_filter_work(struct te_obj *obj,
		struct te_buffer_obj *buffer_obj);

/* internal buffer read/write functions */
int te_buffer_write_bytes(struct te_ps_out_filter *output,
		unsigned char *output_data,
		uint32_t output_data_size,
		uint32_t *bytes_written,
		int *notify);

int te_buffer_read_bytes(struct te_ps_out_filter *output,
		uint32_t *read_offset_p,
		void *destbuffer1_p,
		uint32_t destsize1,
		int (*copy_function)(void **, const void *, size_t),
		uint32_t *bytes_copied);

int te_buffer_status(struct te_ps_out_filter *output,
		uint32_t *bytes_in_buffer_p,
		uint32_t *free_space_p,
		bool *overflowed_flag_p);

int te_buffer_signal(struct te_ps_out_filter *output);

int te_buffer_set_threshold_overflow(struct te_ps_out_filter *output);

int te_buffer_set_overflow_control(struct te_ps_out_filter *output,
				bool discard_input_on_overflow);

int te_buffer_set_threshold(struct te_ps_out_filter *output,
		uint32_t upperthreshold);

int te_buffer_set_read_offset(struct te_ps_out_filter *output,
				uint32_t read_offset);
#endif
