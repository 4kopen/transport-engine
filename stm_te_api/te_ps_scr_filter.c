/******************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_scr_filter.c

Defines PS SCR filter specific operations
******************************************************************************/
#include <stm_te_dbg.h>
#include "te_object.h"
#include "te_ps_output_filter.h"
#include "te_ps_scr_filter.h"
#include "te_time.h"
#include "te_ps_buffer_work.h"
#include "te_psdemux.h"

struct te_ps_scr_filter {
	te_ps_scr_time_point_t time;
	struct te_ps_out_filter output;
	bool new_seq;
};

static struct te_ps_scr_filter *ps_scr_filter_from_obj(struct te_obj *filter)
{
	if (filter->type == TE_OBJ_PS_SCR_FILTER)
		return container_of(te_ps_out_filter_from_obj(filter),
				struct te_ps_scr_filter,
				output);
	else
		return NULL;
}

static void te_ps_convert_raw_data_to_point(uint8_t *raw_scr_data,
		te_ps_scr_time_point_t *tp)
{
	struct timespec ts;
	ktime_t ktime;
	uint64_t scr_time = 0;
	uint16_t scr_ext_time = 0;

	scr_time = (((raw_scr_data[0] & 0x38) << 27) |
			((raw_scr_data[0] & 0x03) << 28) |
			((raw_scr_data[1] & 0xFF) << 20) |
			((raw_scr_data[2] & 0xF8) << 12) |
			((raw_scr_data[2] & 0x03) << 13) |
			((raw_scr_data[3] & 0xFF) << 5) |
			((raw_scr_data[4] & 0xF8) >> 3));

	scr_ext_time = (((raw_scr_data[4] & 0x03) << 7) |
			((raw_scr_data[5] & 0xFE) >> 1));

	tp->scr_time = (scr_time * 300) + scr_ext_time;

	getrawmonotonic(&ts);
	ktime = timespec_to_ktime(ts);
	tp->sys_time = ktime_to_us(ktime);

	pr_debug("\nscr_time = %llx sys_time = %llx\n", tp->scr_time,
			tp->sys_time);
}

static void te_ps_scr_filter_push_work(struct te_ps_scr_filter *scr,
		struct te_buffer_obj *buf)
{
	te_ps_scr_data_t raw_scr_data;
	stm_te_pcr_t convert_data;
	uint32_t bytes_read;
	te_ps_scr_time_point_t time;
	int result;

	result = te_ps_out_filter_read_bytes(&scr->output,
					raw_scr_data.raw_data,
					sizeof(raw_scr_data.raw_data),
					&bytes_read);

	if (!result && (bytes_read == sizeof(raw_scr_data.raw_data))) {
		te_ps_convert_raw_data_to_point(raw_scr_data.raw_data, &time);
		convert_data.system_time = time.sys_time;
		convert_data.pcr = time.scr_time;

		result = scr->output.scr_push_intf.set_clock_data_point_data(
						scr->output.external_connection,
						TIME_FORMAT_PTS, scr->new_seq,
						convert_data.pcr,
						convert_data.system_time);
		if (result)
			pr_err("set_clock_data_point_data error %d\n", result);
		else
			scr->new_seq = false;
	} else {
		/* This is probably due to read triggered by timeout, ignore */
		pr_debug("error reading SCR data. result %d bytes_read %d\n",
			result, bytes_read);
	}
}

static void te_ps_scr_filter_work(struct te_obj *obj, struct te_buffer_obj *buf)
{
	struct te_ps_out_filter *output = te_ps_out_filter_from_obj(obj);

	if (obj->state == TE_OBJ_PAUSED)
		return;

	if (output->scr_push_intf.connect)
		te_ps_scr_filter_push_work(ps_scr_filter_from_obj(obj), buf);
	else
		te_ps_out_filter_work(obj, buf);
}

static int te_ps_scr_filter_attach_to_clk(struct te_ps_scr_filter *scr,
		stm_object_h target)
{
	int err;
	struct te_psdemux *psdemux =
		te_psdemux_from_obj(scr->output.obj.parent);

	pr_debug("Attaching scr filter %p to clock %p\n",
				&scr->output.obj, target);

	err = scr->output.scr_push_intf.connect(&scr->output.obj, target);
	if (err) {
		pr_err("Failed to connect SCR filter %p -> %p (%d)\n",
				&scr->output.obj, target, err);
		goto error;
	}

	scr->new_seq = true;

	if (scr->output.buffer_obj)
		err = te_ps_bwq_register(psdemux->bwq, &scr->output.obj,
				scr->output.buffer_obj,
				scr->output.buffer_work_func);
	if (err) {
		pr_err("Failed to register buffer work\n");
		goto error;
	}

	err = stm_registry_add_connection(&scr->output.obj,
			STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH, target);
	if (err) {
		pr_err("Failed to register connection %s (%d)\n",
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH, err);
		goto error;
	}
	return 0;

error:
	if (scr->output.buffer_obj)
		if (te_ps_bwq_unregister(psdemux->bwq, scr->output.buffer_obj))
			scr->output.buffer_obj = NULL;
	scr->output.scr_push_intf.disconnect(&scr->output.obj, target);
	return err;
}

static int te_ps_scr_filter_detach_from_clk(struct te_ps_scr_filter *scr,
		stm_object_h target)
{
	int err;
	int result = 0;
	struct te_psdemux *psdemux =
		te_psdemux_from_obj(scr->output.obj.parent);

	pr_debug("Detaching scr filter %p from clock %p\n",
					&scr->output.obj, target);

	err = stm_registry_remove_connection(&scr->output.obj,
			STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH);
	if (err) {
		pr_err("Failed to unregister connection %s (%d)\n",
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH, err);
		result = err;
	}

	if (scr->output.buffer_obj)
		if (te_ps_bwq_unregister(psdemux->bwq, scr->output.buffer_obj))
			scr->output.buffer_obj = NULL;

	err = scr->output.scr_push_intf.disconnect(&scr->output.obj, target);
	if (err) {
		pr_err("Failed to disconnect %p->%p (%d)\n",
				&scr->output.obj, target, err);
		result = err;
	} else {
		memset(&scr->output.scr_push_intf, 0,
				sizeof(scr->output.scr_push_intf));
	}

	return result;

}

static int te_ps_scr_filter_attach(struct te_obj *filter, stm_object_h target)
{
	int err;
	stm_object_h target_type;
	char type_tag[STM_REGISTRY_MAX_TAG_SIZE];
	uint32_t iface_size;
	struct te_ps_scr_filter *scr = NULL;

	if (!filter)
		return -EINVAL;

	scr = ps_scr_filter_from_obj(filter);
	if (!scr)
		return -EINVAL;

	/* Check if this output filter is already attached to an external
	* object */
	if (scr->output.external_connection) {
		pr_err("Output filter %p already has an attachment\n",
						filter);
		return -EBUSY;
	}

	err = stm_registry_get_object_type(target, &target_type);
	if (0 != err) {
		pr_err("unable to get type of target object %p\n", target);
		return err;
	}

	if (0 == stm_registry_get_attribute(target_type,
			STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH,
			type_tag,
			sizeof(stm_se_clock_data_point_interface_push_sink_t),
			&scr->output.scr_push_intf,
			&iface_size))
		err = te_ps_scr_filter_attach_to_clk(scr, target);
	else
		/* Fallback to base output filter attach */
		err = te_ps_out_filter_attach(filter, target);

	if (!err)
		scr->output.external_connection = target;

	return err;
}

static int te_ps_scr_filter_detach(struct te_obj *filter, stm_object_h target)
{
	int err;
	stm_object_h connected_obj = NULL;
	struct te_ps_scr_filter *scr = NULL;

	if (!filter)
		return -EINVAL;

	scr = ps_scr_filter_from_obj(filter);
	if (!scr)
		return -EINVAL;

	if (scr->output.external_connection != target) {
		pr_err("Output filter %p not attached to %p\n",
				&scr->output.obj, target);
		return -EINVAL;
	}

	/* Use "push_clk" detach handler if available */
	if (scr->output.scr_push_intf.disconnect) {
		err = stm_registry_get_connection(te_obj_to_hdl(filter),
				STM_SE_CLOCK_DATA_POINT_INTERFACE_PUSH,
				&connected_obj);
		if (0 == err && connected_obj == target)
			err = te_ps_scr_filter_detach_from_clk(scr, target);
	} else {
		/* Fallback to base output filter detach */
		err = te_ps_out_filter_detach(filter, target);
	}

	if (!err)
		scr->output.external_connection = NULL;

	return err;
}

static int te_ps_scr_filter_delete(struct te_obj *filter)
{
	struct te_ps_scr_filter *scr = ps_scr_filter_from_obj(filter);
	int err;

	err = te_ps_out_filter_deinit(&scr->output);
	if (err)
		return err;

	kfree(scr);
	return 0;
}

static int te_ps_scr_filter_read_bytes(struct te_ps_out_filter *output,
		uint8_t *buf, uint32_t buf_size, uint32_t *bytes_read)
{
	int err;
	te_ps_scr_data_t raw_scr_data;
	stm_te_pcr_t *convert_data = (stm_te_pcr_t *)buf;
	struct te_ps_scr_filter *scr = NULL;
	te_ps_scr_time_point_t time;

	scr = ps_scr_filter_from_obj(&output->obj);
	if (!scr)
		return -EINVAL;

	/* Currently SCR filter always reads in quantisation units: 1 SCR at a
	* time */
	if (buf_size < sizeof(stm_te_pcr_t)) {
		pr_err("Supplied buffer not large enough for SCR data %d\n",
				buf_size);
		return -ENOMEM;
	}
	err = te_ps_out_filter_read_bytes(output, raw_scr_data.raw_data,
			sizeof(raw_scr_data.raw_data), bytes_read);
	if (err) {
		pr_err("Error reading raw SCR data (%d)\n", err);
		return err;
	}

	te_ps_convert_raw_data_to_point(raw_scr_data.raw_data, &time);
	convert_data->system_time = time.sys_time;
	convert_data->pcr = time.scr_time;

	*bytes_read = sizeof(*convert_data);
	return err;
}

static int32_t te_ps_scr_filter_next_read_size(struct te_ps_out_filter *output)
{
	int32_t size = 0;
	int err;
	struct te_buffer_obj *buffer_obj;

	err = mutex_lock_interruptible(&output->obj.lock);
	if (err < 0)
		return err;

	buffer_obj = output->buffer_obj;

	if (buffer_obj->wr_offs != buffer_obj->rd_offs)
		size = sizeof(stm_te_pcr_t);

	mutex_unlock(&output->obj.lock);
	return size;
}

static struct te_obj_ops te_ps_scr_filter_ops = {
	.attach = te_ps_scr_filter_attach,
	.detach = te_ps_scr_filter_detach,
	.set_control = te_ps_out_filter_set_control,
	.get_control = te_ps_out_filter_get_control,
	.delete = te_ps_scr_filter_delete,
};

int te_ps_scr_filter_new(struct te_obj *demux_o, struct te_obj **new_filter)
{
	int res = 0;
	char name[STM_REGISTRY_MAX_TAG_SIZE];
	struct te_psdemux *psdemux;

	struct te_ps_scr_filter *filter = kzalloc(sizeof(*filter), GFP_KERNEL);

	if (!filter) {
		pr_err("couldn't allocate SCR filter object\n");
		return -ENOMEM;
	}

	psdemux = te_psdemux_from_obj(demux_o);
	snprintf(name, STM_REGISTRY_MAX_TAG_SIZE, "SCRFilter.0x%p",
					&filter->output.obj);

	res = te_ps_out_filter_init(&filter->output, demux_o, name,
			TE_OBJ_PS_SCR_FILTER);

	if (res != 0)
		goto error;

	filter->output.obj.ops = &te_ps_scr_filter_ops;
	filter->output.read_bytes = &te_ps_scr_filter_read_bytes;
	filter->output.next_read_size = &te_ps_scr_filter_next_read_size;

	/* SCR filter has a different buffer work function */
	filter->output.buffer_work_func = &te_ps_scr_filter_work;

	/* Only create the output filter's buffer handle if required */
	if (filter->output.buffer_obj == NULL) {

		/* We're the first, alloc the buffer */
		res = te_buffer_allocator(&filter->output.obj,
						&filter->output.buffer_obj);
		if (res != 0) {
			pr_err("couldn't allocate buffer\n");
			res = -ENOMEM;
			goto error;
		}
		pr_debug("%s: Allocating buffer size %d buffer index 0x%x\n",
				filter->output.obj.name,
				filter->output.buffer_obj->buf_size,
				(unsigned)filter->output.buffer_obj->buf_index);
	}

	*new_filter = &filter->output.obj;
	return 0;

error:
	*new_filter = NULL;
	kfree(filter);

	return res;
}

