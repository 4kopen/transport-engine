/******************************************************************************
Copyright (C) 2011, 2012 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Transport Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_demux.h

Declares TE demux type and operators
******************************************************************************/
#ifndef __TE_DEMUX_H
#define __TE_DEMUX_H

#include <linux/list.h>
#include <linux/workqueue.h>
#include "te_object.h"
#include "te_hal_obj.h"
#include "te_buffer_work.h"
#include "te_input_filter.h"
#include "te_output_filter.h"

#define MAX_TSIN (2)
#define TTS_BYTE_LENGTH        4

#define STC_SOURCE_UNDEF 0xFF
#define STC_SOURCE_TP 0x2
#define STC_SOURCE_STFE 0x4

enum te_tsprotocol_set_type_e {
	TE_AUTO_DETECT_PROTOCOL,
	TE_USER_DEFINED_PROTOCOL
};

struct te_demux {
	struct te_obj obj;

	/* Demux input parameters */
	uint32_t tsin_num[MAX_TSIN];
	uint32_t tsin_src;
	uint32_t tag;
	uint32_t input_type;
	uint32_t discard_dupe_ts;
	/* Child filters */
	struct list_head in_filters;
	struct list_head out_filters;

	rwlock_t out_flt_rw_lock;
	struct mutex of_sec_lock;

	/* Linked objects */
	struct list_head autotargets;
	struct list_head linked_pids;

	/* HAL resources */
	struct te_hal_obj *hal_vdevice;
	struct te_hal_obj *hal_session;
	struct te_hal_obj *hal_signal;
	struct te_hal_obj *hal_injector;
	struct te_hal_obj *hal_buf;

	/* Injection analysis buffer */
	uint8_t *analysis;
	size_t   analysis_length;

	/* Upstream connected object */
	stm_object_h upstream_obj;
	stm_data_interface_push_notify_t push_notify;
	stm_data_interface_push_sink_t sink_interface;

	/* Upstream disconnection pending */
	bool disconnect_pending;


	/* Statistics */
	stm_te_demux_stats_t stat_upon_reset;
	struct device sysfs_dev;

	/*inject time*/
	ktime_t last_push;
	struct te_push_stats_s push_stats;

	struct workqueue_struct *work_que;

	struct te_buffer_work_queue *bwq;

	/* remote pid filtering functions */
	int (*stm_fe_bc_pid_set)(stm_object_h demod_object,
			stm_object_h demux_object, uint32_t pid);
	int (*stm_fe_bc_pid_clear)(stm_object_h demod_object,
			stm_object_h demux_object, uint32_t pid);

	/* strelay index */
	unsigned int injector_relayfs_index;

	/* Upstream disconnection pending */
	bool use_timer_tag;
	uint8_t timer_read_mode;

	enum te_tsprotocol_set_type_e  tsprotocol_set_type;
	stptiHAL_TransportProtocol_t tsprotocol;

	uint32_t min_allowed_size;
	uint32_t min_pes_sz;
	wait_queue_head_t pacing_waitq;
	struct mutex pacing_lock;
	struct list_head pacing_filters;
	atomic_t data_process_pending;
	uint8_t pkt_size;

	bool use_large_sf_pool;
	/* temp buffer to hold physical address of pushed memory*/
	uint32_t phy_addr[256];

	void *sglist_buf;
	uint32_t sglist_sz;
};

struct te_autotarget {
	struct list_head entry;
	stm_te_pid_t pid;
	stm_te_object_h target;
};

struct te_linkedpid {
	struct list_head entry;
	stm_te_pid_t p_pid;
	stm_te_pid_t s_pid;
	stm_te_secondary_pid_mode_t mode;
	struct te_obj *p_filter;
	struct te_obj *s_filter;
};

struct te_demux *te_demux_from_obj(struct te_obj *obj);

int te_demux_new(char *name, struct te_obj **new_demux);
int te_demux_start(struct te_obj *obj);
int te_demux_stop(struct te_obj *obj);

int te_demux_get_device_time(struct te_obj *demux, uint64_t *dev_time,
		uint64_t *sys_time);

int te_demux_register_pid_autotarget(stm_te_object_h demux, stm_te_pid_t pid,
					stm_te_object_h target);

int te_demux_unregister_pid_autotarget(stm_te_object_h demux, stm_te_pid_t pid);

int te_demux_pid_announce(struct te_in_filter *input);
int te_demux_pid_announce_change(struct te_in_filter *input,
		stm_te_pid_t old_pid, stm_te_pid_t new_pid);

int te_demux_reset_stat(struct te_obj *obj);

int te_demux_link_secondary_pid(stm_te_object_h dmx_h, stm_te_pid_t pri,
				stm_te_pid_t sec,
				stm_te_secondary_pid_mode_t mode);

int te_demux_unlink_secondary_pid(stm_te_object_h dmx_h, stm_te_pid_t pid);
int te_demux_inc_dp_cnt(struct te_demux *dmx, struct te_out_filter *out);
int te_demux_dec_dp_cnt(struct te_demux *dmx, struct te_out_filter *out);
int te_demux_notify_src(struct te_demux *dmx, struct te_obj *of,
		uint32_t free_sz);
#endif
