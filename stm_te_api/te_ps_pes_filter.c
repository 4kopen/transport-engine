/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_pes_filter.c

Defines PES filter specific operations
******************************************************************************/
#include <stm_te_dbg.h>
#include "te_object.h"
#include "te_ps_output_filter.h"
#include "te_ps_pes_filter.h"
#include "te_internal_cfg.h"

struct te_ps_pes_filter {
	struct te_ps_out_filter output;
};

static struct te_ps_pes_filter *te_ps_pes_filter_from_obj(struct te_obj *filter)
{
	if (filter->type == TE_OBJ_PS_PES_FILTER)
		return container_of(filter, struct te_ps_pes_filter,
				output.obj);
	else
		return NULL;
}

int te_ps_pes_filter_get_control(struct te_obj *filter, uint32_t control,
				void *buf, uint32_t size)
{
	int err = 0;

	switch (control) {
	default:
		err = te_ps_out_filter_get_control(filter, control, buf, size);
	}
	return err;
}

int te_ps_pes_filter_set_control(struct te_obj *filter, uint32_t control,
				const void *buf, uint32_t size)
{
	int err = 0;

	switch (control) {
	default:
		err = te_ps_out_filter_set_control(filter, control, buf, size);
	}
	return err;
}

static int te_ps_pes_filter_delete(struct te_obj *filter)
{
	struct te_ps_pes_filter *pes = te_ps_pes_filter_from_obj(filter);
	int err;

	err = te_ps_out_filter_deinit(&pes->output);
	if (err)
		return err;

	kfree(pes);
	return 0;
}

static struct te_obj_ops te_ps_pes_filter_ops = {
	.attach = te_ps_out_filter_attach,
	.detach = te_ps_out_filter_detach,
	.set_control = te_ps_pes_filter_set_control,
	.get_control = te_ps_pes_filter_get_control,
	.delete = te_ps_pes_filter_delete,
};

int te_ps_pes_filter_new(struct te_obj *demux, struct te_obj **new_filter)
{
	int res = 0;
	char name[STM_REGISTRY_MAX_TAG_SIZE];

	struct te_ps_pes_filter *filter = kzalloc(sizeof(*filter), GFP_KERNEL);

	if (!filter) {
		pr_err("couldn't allocate PES PS filter object\n");
		return -ENOMEM;
	}

	snprintf(name, STM_REGISTRY_MAX_TAG_SIZE, "PSPESFilter.0x%p",
				&filter->output.obj);

	res = te_ps_out_filter_init(&filter->output, demux, name,
			TE_OBJ_PS_PES_FILTER);
	if (res != 0)
		goto error;

	/* Initialise TS Index filter data */
	filter->output.obj.ops = &te_ps_pes_filter_ops;

	*new_filter = &filter->output.obj;
	return 0;

error:
	*new_filter = NULL;
	kfree(filter);

	return res;
}
