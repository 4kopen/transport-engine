/******************************************************************************
Copyright (C) 2014, 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Transport Engine Library.

Transport Engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Transport Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The Transport Engine Library may alternatively be licensed under a proprietary
license from ST.

Source file name : te_ps_stream_filter.c

Defines stream filter specific operations
******************************************************************************/
#include <stm_te_dbg.h>
#include "te_object.h"
#include "te_psdemux.h"
#include <te_ps_output_filter.h>
#include <te_ps_stream_filter.h>
#include <te_global.h>
#include <te_sysfs.h>

struct te_stream_filter *te_stream_filter_from_obj(struct te_obj *filter)
{
	return container_of(filter, struct te_stream_filter, obj);
}

/*!
 * \brief Initialises a TE stream filter object
 *
 * Initialises all the internal data structures in the stream filter object to
 * their default values
 *
 * \param filter stream filter object to initialise
 * \param demux Parent demux for stream filter
 * \param name	Name of stream filter
 * \param stream id Initial stream id to be captured by the stream filter object
 *
 * \note This function allocates and initialises only stm_te-level resources
 * \retval 0	Success
 * \retval -EINVAL Bad parameter
 */
static int te_stream_filter_init(struct te_stream_filter *filter,
				struct te_obj *demux_obj,
				char *name, enum te_obj_type_e type)
{
	int err = 0;
	struct te_psdemux *demux = NULL;

	demux = te_psdemux_from_obj(demux_obj);
	if (!demux) {
		pr_err("Bad psdemux handle\n");
		return -EINVAL;
	}

	/* Initialise embedded TE obj */
	err = te_obj_init(&filter->obj, type, name, demux_obj,
			&te_global.in_filter_class);
	if (err)
		goto err1;

	/* Add input filter attributes to registry */
	err = te_sysfs_entries_add(&filter->obj);
	if (err)
		goto err0;

	err = stm_registry_add_attribute(&filter->obj,
			"stream_id",
			STM_REGISTRY_ADDRESS, &filter->stream_id,
			sizeof(filter->stream_id));
	if (err) {
		pr_err("Failed to add attr stream id to %s (%d)\n", name, err);
		goto err0;
	}

	err = stm_registry_add_attribute(&filter->obj,
			"sub_stream_id",
			STM_REGISTRY_ADDRESS, &filter->sub_stream_id,
			sizeof(filter->sub_stream_id));
	if (err) {
		pr_err("Failed to add attr sub stream id to %s (%d)\n", name, err);
		goto err0;
	}

	err = stm_registry_add_attribute(&filter->obj,
			"flushing",
			STM_REGISTRY_UINT32, &filter->flushing_behaviour,
			sizeof(filter->flushing_behaviour));
	if (err) {
		pr_err("Failed to add attr flushing to %s (%d)\n", name, err);
		goto err0;
	}
	list_add_tail(&filter->obj.lh, &demux->stream_filters);

	return 0;
err0:
	te_obj_deinit(&filter->obj);
err1:
	return err;
}

/*!
 * \brief Uninitializes a TE stream filter object
 *
 * \param filter Pointer to the stream filter object to destroy
 *
 * \retval 0 Success
 */
static int te_stream_filter_deinit(struct te_stream_filter *filter)
{
	int err = 0;

	te_sysfs_entries_remove(&filter->obj);

	err = te_obj_deinit(&filter->obj);
	if (err)
		return err;

	list_del(&filter->obj.lh);
	return 0;
}

/*!
 * \brief Deletes a TE STREAM filter object
 *
 * \param obj TE STREAM filter object to delete
 *
 * \retval 0 Success
 */
static int te_stream_filter_delete(struct te_obj *obj)
{
	int err;
	struct te_stream_filter *filter = te_stream_filter_from_obj(obj);

	err = te_stream_filter_deinit(filter);
	if (err)
		return err;

	kfree(filter);
	return err;
}

static int te_stream_filter_get_control(struct te_obj *obj, uint32_t control,
		void *buf, uint32_t size)
{
	int err = 0;

	struct te_stream_filter *filter = te_stream_filter_from_obj(obj);

	switch (control) {
	case STM_TE_PS_STREAM_FILTER_CONTROL_STREAM_ID:
		err = GET_CONTROL(filter->stream_id, buf, size);
		break;

	case STM_TE_PS_STREAM_FILTER_CONTROL_SUB_STREAM_ID:
		err = GET_CONTROL(filter->sub_stream_id, buf, size);
		break;

	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		/* TODO: This control is mis-named, since it also applies to
		* input filters */
		err = GET_CONTROL(filter->flushing_behaviour, buf, size);
		break;
	default:
		err = -ENOSYS;
		break;
	}
	return err;
}

static int te_stream_filter_set_control(struct te_obj *obj, uint32_t control,
		const void *buf, uint32_t size)
{
	int err = 0;

	struct te_stream_filter *filter = te_stream_filter_from_obj(obj);

	switch (control) {
	case STM_TE_PS_STREAM_FILTER_CONTROL_STREAM_ID:
		err = SET_CONTROL(filter->stream_id, buf, size);
		break;

	case STM_TE_PS_STREAM_FILTER_CONTROL_SUB_STREAM_ID:
		err = SET_CONTROL(filter->sub_stream_id, buf, size);
		break;

	case STM_TE_OUTPUT_FILTER_CONTROL_FLUSHING_BEHAVIOUR:
		/* TODO: This control is mis-named, since it also applies to
		* input filters */
		err = SET_CONTROL(filter->flushing_behaviour, buf, size);
		break;

	default:
		err = -ENOSYS;
		break;
	}
	return err;
}

/*!
 * \brief Attaches a input filter to an output filter
 *
 * \param in_filter Pointer to input filter to attach
 * \param out_filter TE obj for output filter to attach to
 *
 * \retval 0	Success
 * \retval -EEXIST Filters are already connected
 */
static int te_ps_in_filter_attach_out_filter(struct te_stream_filter *in_filter,
		struct te_obj *out_filter)
{
	int err = 0;
	int i;
	char link_name[STM_REGISTRY_MAX_TAG_SIZE];

	snprintf(link_name, STM_REGISTRY_MAX_TAG_SIZE, "in(%p)->output(%p)",
			in_filter, out_filter);

	/* Check if objects are already attached */
	for (i = 0; i < PS_MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] == out_filter) {
			pr_err("Filters %p and %p are already attached\n",
					&in_filter->obj, out_filter);
			return -EEXIST;
		}
	}

	/* Check for free internal connections */
	for (i = 0; i < PS_MAX_INT_CONNECTIONS; i++) {
		if (NULL == in_filter->output_filters[i])
			break;
	}
	if (PS_MAX_INT_CONNECTIONS == i) {
		pr_err("input filter %p has too many connections\n",
				&in_filter->obj);
		goto error;
	}

	/* Inform registry of connections */
	err = stm_registry_add_connection(&in_filter->obj, link_name,
			out_filter);
	if (err) {
		pr_err("Failed to register connection (%d)\n", err);
		goto error;
	}

	/* Create objects resulting from this attachment */
	err = te_ps_out_filter_connect_input(out_filter, &in_filter->obj);
	if (err)
		goto error;

	/* Update filter states and connection */
	in_filter->obj.state = TE_OBJ_STARTED;
	out_filter->state = TE_OBJ_STARTED;
	in_filter->output_filters[i] = out_filter;

	pr_debug("Attached input filter %p -> out filter %p\n", &in_filter->obj,
			out_filter);

	return 0;

error:
	if (stm_registry_remove_connection(&in_filter->obj, link_name))
		pr_warn("Cleanup:Error removing link %s\n", link_name);
	for (i = 0; i < PS_MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] == out_filter)
			in_filter->output_filters[i] = NULL;
	}

	return err;
}

/*!
 * \brief Disconnects a input filter from an output filter
 *
 * \param in_filter Input filter object to disconnect
 * \param out_filter TE Output filter object to disconnect from
 *
 * \retval 0	Success
 * \retval -EINVAL No connection
 */
static int te_ps_in_filter_detach_out_filter(struct te_stream_filter *in_filter,
		struct te_obj *out_filter)
{
	int err = 0;
	int i;
	bool connected = false;
	char link_name[STM_REGISTRY_MAX_TAG_SIZE];

	snprintf(link_name, STM_REGISTRY_MAX_TAG_SIZE, "in(%p)->output(%p)",
			in_filter, out_filter);

	/* All errors in this function are treated as non-fatal: we
	* print a warning but continue */

	/* Remove internal connections */
	for (i = 0; i < PS_MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] == out_filter) {
			in_filter->output_filters[i] = NULL;
			connected = true;
		}
	}

	if (!connected) {
		pr_err("output (0x%p) not connected to input (0x%p)\n",
				out_filter, in_filter);
		return -EINVAL;
	}

	/* Unregister connection */
	err = stm_registry_remove_connection(&in_filter->obj, link_name);
	if (err) {
		pr_warn("Failed to remove connection %s (%d)\n", link_name,
				err);
	}
	/* If the input filter has no other connections, set it's state to
	* stopped */
	for (i = 0; i < PS_MAX_INT_CONNECTIONS; i++) {
		if (in_filter->output_filters[i] != NULL)
			break;
	}
	if (PS_MAX_INT_CONNECTIONS == i)
		in_filter->obj.state = TE_OBJ_STOPPED;

	/* Destroy objects */
	err = te_ps_out_filter_disconnect_input(out_filter, &in_filter->obj);
	if (err)
		pr_warn("Failed to disconnect HAL objects (err %d) 0x%p 0x%p\n",
			err, &in_filter->obj, out_filter);
	else
		pr_debug("Detached input filter %p -> out filter %p\n",
				&in_filter->obj,
				out_filter);

	return err;
}

/*!
 * \brief Connects a PS input filter to another STKPI object
 *
 * \param[in] obj	Pointer to TE obj for the input filter to connect
 * \param[in] target	Object handle of the object being connected to
 *
 * \retval 0		Success
 * \retval -EPERM Do not know how to connect the specified objects
 **/
static int te_ps_in_filter_attach(struct te_stream_filter *filter,
		stm_object_h target)
{
	int err = 0;
	struct te_ps_out_filter *out_filter;

	/* Try stream -> output filter connection */
	if (0 == te_ps_out_filter_from_hdl(target, &out_filter)) {
		err = te_ps_in_filter_attach_out_filter(filter,
				&out_filter->obj);
		goto out;
	}

out:
	return err;
}
/*!
 * \brief Disconnects a input filter from another object
 *
 * \param[in] obj	Pointer to TE obj for the input filter to disconnect
 * \param[in] target	Object handle of the object being disconnected from
 *
 * Objects must have previously been connected
 */
static int te_ps_in_filter_detach(struct te_stream_filter *filter,
		stm_object_h target)
{
	int err = 0;
	struct te_ps_out_filter *out_filter;

	/* Try stream -> output filter disconnection */
	if (0 == te_ps_out_filter_from_hdl(target, &out_filter)) {
		err = te_ps_in_filter_detach_out_filter(filter,
				&out_filter->obj);
		goto out;
	}

out:
	return err;
}
static int te_stream_filter_attach(struct te_obj *obj, stm_object_h target)
{
	stm_object_h hdl;
	struct te_stream_filter *inf = NULL;
	int err;

	err = te_hdl_from_obj(obj, &hdl);
	if (err == 0) {
		inf = te_stream_filter_from_obj(obj);
		err = te_ps_in_filter_attach(inf, target);
	}
	return err;
}

static int te_stream_filter_detach(struct te_obj *obj, stm_object_h target)
{
	stm_object_h hdl;
	struct te_stream_filter *in = NULL;
	int err;

	err = te_hdl_from_obj(obj, &hdl);
	if (err == 0) {
		in = te_stream_filter_from_obj(obj);
		err = te_ps_in_filter_detach(in, target);
	}

	return err;
}

static struct te_obj_ops te_stream_filter_ops = {
	.delete = &te_stream_filter_delete,
	.set_control = &te_stream_filter_set_control,
	.get_control = &te_stream_filter_get_control,
	.attach = &te_stream_filter_attach,
	.detach = &te_stream_filter_detach,
};

/*!
 * \brief Creates a new TE STREAM filter object
 *
 * \param demux		Parent demux for new stream filter object
 * \param stream	Initial stream to be capture by the STREAM filter object
 * \param new_filter Set to point to the new stream filter TE object on success
 *
 * \retval 0	Success
 * \retval -EINVAL A bad parameter was supplied
 * \retval -ENOMEM Insufficient resources to allocate the new filter
 */
int te_stream_filter_new(struct te_obj *psdemux, uint8_t stream_id,
		struct te_obj **new_filter)

{
	int err = 0;
	struct te_stream_filter *filter;
	char name[STM_REGISTRY_MAX_TAG_SIZE];

	filter = kzalloc(sizeof(struct te_stream_filter), GFP_KERNEL);
	if (!filter) {
		pr_err("Failed to allocate STREAM filter\n");
		return -ENOMEM;
	}

	snprintf(name, STM_REGISTRY_MAX_TAG_SIZE, "Stream.Filter.0x%p",
			&filter->obj);

	err = te_stream_filter_init(filter, psdemux, name,
			TE_OBJ_PS_STREAM_FILTER);
	if (err)
		goto error;

	filter->obj.ops = &te_stream_filter_ops;
	filter->stream_id = stream_id;
	if (filter->stream_id == PES_TYPE_PRIVATE_STREAM_1)
		filter->sub_stream_id = PES_DEFAULT_SUB_STREAM_ID;
	*new_filter = &filter->obj;

	return 0;
error:
	kfree(filter);
	return err;
}
