# Objects we will link into busybox / subdirs we need to visit

CONFIG_KERNEL_BUILD?=$(KERNELDIR)
CONFIG_KERNEL_PATH?=$(KERNELDIR)

export CONFIG_KERNEL_BUILD CONFIG_KERNEL_PATH


all: modules

distclean: clean
	-find . -name "*.o" | xargs rm -f
	-find . -name ".*.o.cmd" | xargs rm -f
	-find . -name ".*.ko.cmd" | xargs rm -f
	-find . -name "*.ko" | xargs rm -f
	-find . -name "*.mod.c" | xargs rm -f
	rm -rf doxygen
	rm -rf linux/.tmp_versions/*

KBUILD_PATH := $(shell pwd)/linux
ifeq ($(CONFIG_STM_VIRTUAL_PLATFORM),) # FIXME VSoC arch for compilation must be HCE
ifneq ($(STM_TARGET_CPU),)
MYARCH := $(STM_TARGET_CPU)
else
ifneq ($(ARCH),)
MYARCH := $(ARCH)
else
MYARCH := sh
endif
endif
else
MYARCH := hce
endif

modules:
	$(MAKE) -C $(CONFIG_KERNEL_PATH) O=$(CONFIG_KERNEL_BUILD) M="$(KBUILD_PATH)" CROSS_COMPILE=$(CROSS_COMPILE) ARCH=$(MYARCH) modules

modules_install:
	$(MAKE) -C $(CONFIG_KERNEL_PATH) O=$(CONFIG_KERNEL_BUILD) M="$(KBUILD_PATH)" CROSS_COMPILE=$(CROSS_COMPILE) ARCH=$(MYARCH) INSTALL_MOD_DIR=transport_engine modules_install

# Headers management
# Install kpi headers to staging area with same sudirs
KPI_HEADERS_INSTALL_DIR=$(KERNEL_STAGING_DIR)

KPI_HEADERS := \
	include/stm_te_if_tsmux.h \
	include/stm_te_if_ce.h \
	include/stm_te_dbg.h \
	include/stm_te_if_tsinhw.h \
	include/stm_te.h \
	include/stm_te_version.h \

INSTALLED_KPI_HEADERS := $(foreach file,$(KPI_HEADERS),$(KPI_HEADERS_INSTALL_DIR)/$(file))

$(KPI_HEADERS_INSTALL_DIR)/% : %
	install -d $(dir $@)
	install --mode=644 $< $@

module_install_headers: $(INSTALLED_KPI_HEADERS)

module_clean_headers: $(INSTALLED_KPI_HEADERS)
	$(RM) $(INSTALLED_KPI_HEADERS)


docs doxygen : .PHONY
	doxygen

.PHONY :

